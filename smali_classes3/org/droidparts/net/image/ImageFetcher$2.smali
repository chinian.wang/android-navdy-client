.class Lorg/droidparts/net/image/ImageFetcher$2;
.super Ljava/lang/Object;
.source "ImageFetcher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/droidparts/net/image/ImageFetcher;->fetchAndDecode(Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;)Landroid/util/Pair;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/droidparts/net/image/ImageFetcher;

.field final synthetic val$imgView:Landroid/widget/ImageView;

.field final synthetic val$kBReceived:I

.field final synthetic val$kBTotal:I

.field final synthetic val$spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;


# direct methods
.method constructor <init>(Lorg/droidparts/net/image/ImageFetcher;Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;Landroid/widget/ImageView;II)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lorg/droidparts/net/image/ImageFetcher$2;->this$0:Lorg/droidparts/net/image/ImageFetcher;

    iput-object p2, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    iput-object p3, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$imgView:Landroid/widget/ImageView;

    iput p4, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$kBTotal:I

    iput p5, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$kBReceived:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 225
    iget-object v0, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    iget-object v0, v0, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->listener:Lorg/droidparts/net/image/ImageFetchListener;

    iget-object v1, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$imgView:Landroid/widget/ImageView;

    iget-object v2, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$spec:Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;

    iget-object v2, v2, Lorg/droidparts/net/image/ImageFetcher$ImageViewSpec;->imgUrl:Ljava/lang/String;

    iget v3, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$kBTotal:I

    iget v4, p0, Lorg/droidparts/net/image/ImageFetcher$2;->val$kBReceived:I

    invoke-interface {v0, v1, v2, v3, v4}, Lorg/droidparts/net/image/ImageFetchListener;->onFetchProgressChanged(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 227
    return-void
.end method
