.class public Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;
.super Lorg/droidparts/net/http/worker/HTTPWorker;
.source "HttpURLConnectionWorker.java"


# static fields
.field private static final BOUNDARY:Ljava/lang/String; = "*****"

.field private static final CRLF:Ljava/lang/String; = "\r\n"

.field private static final TWO_HYPHENS:Ljava/lang/String; = "--"


# instance fields
.field private proxy:Ljava/net/Proxy;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 57
    invoke-direct {p0}, Lorg/droidparts/net/http/worker/HTTPWorker;-><init>()V

    .line 58
    iput-object p2, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->userAgent:Ljava/lang/String;

    .line 59
    return-void
.end method

.method private static connectAndGetResponseCodeOrThrow(Ljava/net/HttpURLConnection;)I
    .locals 5
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 176
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->connect()V

    .line 177
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    .line 178
    .local v2, "respCode":I
    invoke-static {v2}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->isErrorResponseCode(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {p0, v3}, Lorg/droidparts/net/http/worker/HTTPInputStream;->getInstance(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/worker/HTTPInputStream;

    move-result-object v1

    .line 181
    .local v1, "is":Lorg/droidparts/net/http/worker/HTTPInputStream;
    new-instance v3, Lorg/droidparts/net/http/HTTPException;

    invoke-virtual {v1}, Lorg/droidparts/net/http/worker/HTTPInputStream;->readAndClose()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Lorg/droidparts/net/http/HTTPException;-><init>(ILjava/lang/String;)V

    throw v3
    :try_end_0
    .catch Lorg/droidparts/net/http/HTTPException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 184
    .end local v1    # "is":Lorg/droidparts/net/http/worker/HTTPInputStream;
    .end local v2    # "respCode":I
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Lorg/droidparts/net/http/HTTPException;
    throw v0

    .line 179
    .end local v0    # "e":Lorg/droidparts/net/http/HTTPException;
    .restart local v2    # "respCode":I
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 186
    .end local v2    # "respCode":I
    :catch_1
    move-exception v0

    .line 187
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->throwIfNetworkOnMainThreadException(Ljava/lang/Exception;)V

    .line 188
    new-instance v3, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v3, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 183
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "respCode":I
    :cond_1
    return v2
.end method

.method public static getResponse(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/HTTPResponse;
    .locals 3
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .param p1, "body"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v1, Lorg/droidparts/net/http/HTTPResponse;

    invoke-direct {v1}, Lorg/droidparts/net/http/HTTPResponse;-><init>()V

    .line 162
    .local v1, "response":Lorg/droidparts/net/http/HTTPResponse;
    invoke-static {p0}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->connectAndGetResponseCodeOrThrow(Ljava/net/HttpURLConnection;)I

    move-result v2

    iput v2, v1, Lorg/droidparts/net/http/HTTPResponse;->code:I

    .line 163
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v2

    iput-object v2, v1, Lorg/droidparts/net/http/HTTPResponse;->headers:Ljava/util/Map;

    .line 164
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lorg/droidparts/net/http/worker/HTTPInputStream;->getInstance(Ljava/net/HttpURLConnection;Z)Lorg/droidparts/net/http/worker/HTTPInputStream;

    move-result-object v0

    .line 165
    .local v0, "is":Lorg/droidparts/net/http/worker/HTTPInputStream;
    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {v0}, Lorg/droidparts/net/http/worker/HTTPInputStream;->readAndClose()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lorg/droidparts/net/http/HTTPResponse;->body:Ljava/lang/String;

    .line 170
    :goto_0
    return-object v1

    .line 168
    :cond_0
    iput-object v0, v1, Lorg/droidparts/net/http/HTTPResponse;->inputStream:Lorg/droidparts/net/http/worker/HTTPInputStream;

    goto :goto_0
.end method

.method public static postMultipart(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 10
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "contentType"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 122
    invoke-virtual {p0, v8}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 123
    const-string v5, "Cache-Control"

    const-string v6, "no-cache"

    invoke-virtual {p0, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v5, "Connection"

    const-string v6, "keep-alive"

    invoke-virtual {p0, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v5, "Content-Type"

    const-string v6, "multipart/form-data;boundary=*****"

    invoke-virtual {p0, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v3, 0x0

    .line 129
    .local v3, "request":Ljava/io/DataOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/DataOutputStream;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 131
    .end local v3    # "request":Ljava/io/DataOutputStream;
    .local v4, "request":Ljava/io/DataOutputStream;
    :try_start_1
    const-string v5, "--*****\r\n"

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 132
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content-Disposition: form-data; name=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\";filename=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 134
    if-eqz p2, :cond_0

    .line 135
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content-Type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 137
    :cond_0
    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 139
    const/4 v1, 0x0

    .line 141
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_2
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 142
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    :try_start_3
    invoke-static {v2}, Lorg/droidparts/util/IOUtils;->readToByteArray(Ljava/io/InputStream;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 144
    const/4 v5, 0x1

    :try_start_4
    new-array v5, v5, [Ljava/io/Closeable;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v5}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    .line 147
    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 148
    const-string v5, "--*****--\r\n"

    invoke-virtual {v4, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 155
    new-array v5, v8, [Ljava/io/Closeable;

    aput-object v4, v5, v9

    invoke-static {v5}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    .line 157
    return-void

    .line 144
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :catchall_0
    move-exception v5

    :goto_0
    const/4 v6, 0x1

    :try_start_5
    new-array v6, v6, [Ljava/io/Closeable;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v6}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    throw v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 151
    .end local v1    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 152
    .end local v4    # "request":Ljava/io/DataOutputStream;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "request":Ljava/io/DataOutputStream;
    :goto_1
    :try_start_6
    invoke-static {v0}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->throwIfNetworkOnMainThreadException(Ljava/lang/Exception;)V

    .line 153
    new-instance v5, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v5, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 155
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v5

    :goto_2
    new-array v6, v8, [Ljava/io/Closeable;

    aput-object v3, v6, v9

    invoke-static {v6}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    throw v5

    .end local v3    # "request":Ljava/io/DataOutputStream;
    .restart local v4    # "request":Ljava/io/DataOutputStream;
    :catchall_2
    move-exception v5

    move-object v3, v4

    .end local v4    # "request":Ljava/io/DataOutputStream;
    .restart local v3    # "request":Ljava/io/DataOutputStream;
    goto :goto_2

    .line 151
    :catch_1
    move-exception v0

    goto :goto_1

    .line 144
    .end local v3    # "request":Ljava/io/DataOutputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "request":Ljava/io/DataOutputStream;
    :catchall_3
    move-exception v5

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0
.end method

.method public static postOrPut(Ljava/net/HttpURLConnection;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "conn"    # Ljava/net/HttpURLConnection;
    .param p1, "contentType"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 106
    const-string v2, "Accept-Charset"

    const-string v3, "utf-8"

    invoke-virtual {p0, v2, v3}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, "Content-Type"

    invoke-virtual {p0, v2, p1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/4 v1, 0x0

    .line 110
    .local v1, "os":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 111
    const-string v2, "utf-8"

    invoke-virtual {p2, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    new-array v2, v5, [Ljava/io/Closeable;

    aput-object v1, v2, v4

    invoke-static {v2}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    .line 118
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-static {v0}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->throwIfNetworkOnMainThreadException(Ljava/lang/Exception;)V

    .line 114
    new-instance v2, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v2, v0}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    new-array v3, v5, [Ljava/io/Closeable;

    aput-object v1, v3, v4

    invoke-static {v3}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    throw v2
.end method


# virtual methods
.method public getConnection(Ljava/lang/String;Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 7
    .param p1, "urlStr"    # Ljava/lang/String;
    .param p2, "requestMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/droidparts/net/http/HTTPException;
        }
    .end annotation

    .prologue
    .line 73
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 74
    .local v4, "url":Ljava/net/URL;
    invoke-virtual {p0, v4}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->openConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 75
    .local v0, "conn":Ljava/net/HttpURLConnection;
    iget-object v5, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->headers:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 76
    .local v3, "key":Ljava/lang/String;
    iget-object v5, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->headers:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v0, v3, v5}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    .end local v0    # "conn":Ljava/net/HttpURLConnection;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 90
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->throwIfNetworkOnMainThreadException(Ljava/lang/Exception;)V

    .line 91
    new-instance v5, Lorg/droidparts/net/http/HTTPException;

    invoke-direct {v5, v1}, Lorg/droidparts/net/http/HTTPException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 78
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "conn":Ljava/net/HttpURLConnection;
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v4    # "url":Ljava/net/URL;
    :cond_0
    :try_start_1
    iget-object v5, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->userAgent:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 79
    const-string v5, "User-Agent"

    iget-object v6, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->userAgent:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_1
    const-string v5, "Accept-Encoding"

    const-string v6, "gzip,deflate"

    invoke-virtual {v0, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, p2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 83
    iget-boolean v5, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->followRedirects:Z

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 84
    const-string v5, "PUT"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "POST"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 86
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 88
    :cond_3
    return-object v0
.end method

.method protected openConnection(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->proxy:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->proxy:Ljava/net/Proxy;

    invoke-virtual {p1, v0}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 100
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    goto :goto_0
.end method

.method public setCookieJar(Lorg/droidparts/net/http/CookieJar;)V
    .locals 0
    .param p1, "cookieJar"    # Lorg/droidparts/net/http/CookieJar;

    .prologue
    .line 63
    invoke-static {p1}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    .line 64
    return-void
.end method

.method public setProxy(Ljava/net/Proxy;)V
    .locals 0
    .param p1, "proxy"    # Ljava/net/Proxy;

    .prologue
    .line 67
    iput-object p1, p0, Lorg/droidparts/net/http/worker/HttpURLConnectionWorker;->proxy:Ljava/net/Proxy;

    .line 68
    return-void
.end method
