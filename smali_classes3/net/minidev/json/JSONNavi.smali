.class public Lnet/minidev/json/JSONNavi;
.super Ljava/lang/Object;
.source "JSONNavi.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final ERROR_COMPRESS:Lnet/minidev/json/JSONStyle;


# instance fields
.field private current:Ljava/lang/Object;

.field private factory:Lnet/minidev/json/parser/ContainerFactory;

.field private failure:Z

.field private failureMessage:Ljava/lang/String;

.field private missingKey:Ljava/lang/Object;

.field private path:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private readonly:Z

.field private root:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 627
    new-instance v0, Lnet/minidev/json/JSONStyle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONStyle;-><init>(I)V

    sput-object v0, Lnet/minidev/json/JSONNavi;->ERROR_COMPRESS:Lnet/minidev/json/JSONStyle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 81
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    sget-object v0, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;-><init>(Lnet/minidev/json/parser/ContainerFactory;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    .line 39
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    .line 42
    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    .line 45
    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    .line 85
    invoke-static {p1}, Lnet/minidev/json/JSONValue;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    .line 86
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    .line 88
    return-void
.end method

.method public constructor <init>(Lnet/minidev/json/parser/ContainerFactory;)V
    .locals 2
    .param p1, "factory"    # Lnet/minidev/json/parser/ContainerFactory;

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    .line 39
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    .line 42
    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    .line 45
    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    .line 91
    iput-object p1, p0, Lnet/minidev/json/JSONNavi;->factory:Lnet/minidev/json/parser/ContainerFactory;

    .line 92
    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    .line 93
    return-void
.end method

.method private a(Ljava/lang/Object;)Ljava/util/List;
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 538
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    check-cast p1, Ljava/util/List;

    .end local p1    # "obj":Ljava/lang/Object;
    return-object p1
.end method

.method private failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;
    .locals 3
    .param p1, "err"    # Ljava/lang/String;
    .param p2, "jPathPostfix"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 653
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x1

    iput-boolean v1, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 655
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "Error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    const-string v1, " at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->getJPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 659
    if-eqz p2, :cond_0

    .line 660
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 661
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x5d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 664
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->failureMessage:Ljava/lang/String;

    .line 665
    return-object p0

    .line 663
    :cond_1
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private isArray(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 520
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    if-nez p1, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 522
    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Ljava/util/List;

    goto :goto_0
.end method

.method private isObject(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 529
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    if-nez p1, :cond_0

    .line 530
    const/4 v0, 0x0

    .line 531
    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Ljava/util/Map;

    goto :goto_0
.end method

.method public static newInstance()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<",
            "Lnet/minidev/json/JSONAwareEx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lnet/minidev/json/JSONNavi;

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONNavi;-><init>(Lnet/minidev/json/parser/ContainerFactory;)V

    return-object v0
.end method

.method public static newInstanceArray()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<",
            "Lnet/minidev/json/JSONArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 75
    new-instance v0, Lnet/minidev/json/JSONNavi;

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONNavi;-><init>(Lnet/minidev/json/parser/ContainerFactory;)V

    .line 76
    .local v0, "o":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<Lnet/minidev/json/JSONArray;>;"
    invoke-virtual {v0}, Lnet/minidev/json/JSONNavi;->array()Lnet/minidev/json/JSONNavi;

    .line 77
    return-object v0
.end method

.method public static newInstanceObject()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<",
            "Lnet/minidev/json/JSONObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lnet/minidev/json/JSONNavi;

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_SIMPLE:Lnet/minidev/json/parser/ContainerFactory;

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONNavi;-><init>(Lnet/minidev/json/parser/ContainerFactory;)V

    .line 67
    .local v0, "o":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<Lnet/minidev/json/JSONObject;>;"
    invoke-virtual {v0}, Lnet/minidev/json/JSONNavi;->object()Lnet/minidev/json/JSONNavi;

    .line 68
    return-object v0
.end method

.method public static newInstanceOrdered()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<",
            "Ljava/util/Collection",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Lnet/minidev/json/JSONNavi;

    sget-object v1, Lnet/minidev/json/parser/ContainerFactory;->FACTORY_ORDERED:Lnet/minidev/json/parser/ContainerFactory;

    invoke-direct {v0, v1}, Lnet/minidev/json/JSONNavi;-><init>(Lnet/minidev/json/parser/ContainerFactory;)V

    return-object v0
.end method

.method private o(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 545
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    check-cast p1, Ljava/util/Map;

    .end local p1    # "obj":Ljava/lang/Object;
    return-object p1
.end method

.method private store()V
    .locals 6

    .prologue
    .line 491
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    .line 492
    .local v2, "parent":Ljava/lang/Object;
    invoke-direct {p0, v2}, Lnet/minidev/json/JSONNavi;->isObject(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 493
    invoke-direct {p0, v2}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v4

    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    invoke-direct {p0, v2}, Lnet/minidev/json/JSONNavi;->isArray(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 495
    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 496
    .local v0, "index":I
    invoke-direct {p0, v2}, Lnet/minidev/json/JSONNavi;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 497
    .local v1, "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v0, :cond_2

    .line 498
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 499
    :cond_2
    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-interface {v1, v0, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public varargs add([Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;
    .locals 6
    .param p1, "values"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 251
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->array()Lnet/minidev/json/JSONNavi;

    .line 252
    iget-boolean v5, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v5, :cond_1

    .line 257
    :cond_0
    return-object p0

    .line 254
    :cond_1
    iget-object v5, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v5}, Lnet/minidev/json/JSONNavi;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 255
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 256
    .local v4, "o":Ljava/lang/Object;
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public array()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 430
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-object p0

    .line 432
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    if-eqz v0, :cond_2

    .line 433
    const-string v0, "Can not create Array child in readonly"

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 434
    :cond_2
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 435
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isObject()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 438
    const-string v0, "can not use Object feature on Array."

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 439
    :cond_3
    const-string v0, "Can not use current possition as Object"

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 443
    :goto_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    if-nez v0, :cond_5

    .line 444
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    goto :goto_0

    .line 441
    :cond_4
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->factory:Lnet/minidev/json/parser/ContainerFactory;

    invoke-interface {v0}, Lnet/minidev/json/parser/ContainerFactory;->createArrayContainer()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    goto :goto_1

    .line 446
    :cond_5
    invoke-direct {p0}, Lnet/minidev/json/JSONNavi;->store()V

    goto :goto_0
.end method

.method public asBoolean()Z
    .locals 1

    .prologue
    .line 383
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 385
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public asBooleanObj()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v0, 0x0

    .line 393
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 397
    :cond_0
    :goto_0
    return-object v0

    .line 395
    :cond_1
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 396
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public asDouble()D
    .locals 2

    .prologue
    .line 277
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 279
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    goto :goto_0
.end method

.method public asDoubleObj()Ljava/lang/Double;
    .locals 2

    .prologue
    .line 287
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 288
    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    .line 289
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    goto :goto_0

    .line 292
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 294
    :cond_2
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method public asFloat()D
    .locals 2

    .prologue
    .line 302
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    float-to-double v0, v0

    .line 304
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    goto :goto_0
.end method

.method public asFloatObj()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 312
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 313
    const/4 v0, 0x0

    .line 319
    :goto_0
    return-object v0

    .line 314
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    goto :goto_0

    .line 317
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_2
    const/high16 v0, 0x7fc00000    # NaNf

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public asInt()I
    .locals 1

    .prologue
    .line 326
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 328
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public asIntegerObj()Ljava/lang/Integer;
    .locals 6

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 336
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v2, :cond_1

    .line 349
    :cond_0
    :goto_0
    return-object v1

    .line 338
    :cond_1
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Number;

    if-eqz v2, :cond_0

    .line 339
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 340
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    goto :goto_0

    .line 341
    :cond_2
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 342
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    .line 343
    .local v0, "l":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 344
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method public asLong()J
    .locals 2

    .prologue
    .line 356
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    .line 358
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public asLongObj()Ljava/lang/Long;
    .locals 2

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v0, 0x0

    .line 366
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-object v0

    .line 368
    :cond_1
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Number;

    if-eqz v1, :cond_0

    .line 369
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 370
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    goto :goto_0

    .line 371
    :cond_2
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 372
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public asString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 266
    const/4 v0, 0x0

    .line 269
    :goto_0
    return-object v0

    .line 267
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 269
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public at(I)Lnet/minidev/json/JSONNavi;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 557
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v2, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v2, :cond_0

    .line 581
    .end local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :goto_0
    return-object p0

    .line 559
    .restart local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :cond_0
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v2, v2, Ljava/util/List;

    if-nez v2, :cond_1

    .line 560
    const-string v2, "current node is not an Array"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 561
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 562
    .local v0, "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-gez p1, :cond_2

    .line 563
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr p1, v2

    .line 564
    if-gez p1, :cond_2

    .line 565
    const/4 p1, 0x0

    .line 567
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lt p1, v2, :cond_4

    .line 568
    iget-boolean v2, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    if-eqz v2, :cond_3

    .line 569
    const-string v2, "Out of bound exception for index"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 571
    :cond_3
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 572
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 573
    const/4 v2, 0x0

    iput-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 574
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    goto :goto_0

    .line 577
    :cond_4
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 578
    .local v1, "next":Ljava/lang/Object;
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 579
    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 580
    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    goto :goto_0
.end method

.method public at(Ljava/lang/String;)Lnet/minidev/json/JSONNavi;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v1, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v1, :cond_0

    .line 152
    .end local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :goto_0
    return-object p0

    .line 135
    .restart local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :cond_0
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isObject()Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->object()Lnet/minidev/json/JSONNavi;

    .line 137
    :cond_1
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/Map;

    if-nez v1, :cond_2

    .line 138
    const-string v1, "current node is not an Object"

    invoke-direct {p0, v1, p1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 139
    :cond_2
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v1}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 140
    iget-boolean v1, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    if-eqz v1, :cond_3

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "current Object have no key named "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 142
    :cond_3
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v1, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 144
    const/4 v1, 0x0

    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 145
    iput-object p1, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    goto :goto_0

    .line 148
    :cond_4
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v1}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 149
    .local v0, "next":Ljava/lang/Object;
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    iget-object v2, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v1, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 151
    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    goto :goto_0
.end method

.method public atNext()Lnet/minidev/json/JSONNavi;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 590
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v1, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v1, :cond_0

    .line 595
    .end local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    .local v0, "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    :goto_0
    return-object p0

    .line 592
    .end local v0    # "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .restart local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :cond_0
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v1, v1, Ljava/util/List;

    if-nez v1, :cond_1

    .line 593
    const-string v1, "current node is not an Array"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 594
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 595
    .restart local v0    # "lst":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v1}, Lnet/minidev/json/JSONNavi;->at(I)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 166
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 172
    .end local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :goto_0
    return-object p0

    .line 168
    .restart local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :cond_0
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->array()Lnet/minidev/json/JSONNavi;

    .line 170
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/List;

    if-nez v0, :cond_2

    .line 171
    const-string v0, "current node is not an List"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 156
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 162
    .end local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :goto_0
    return-object p0

    .line 158
    .restart local p0    # "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    :cond_0
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isObject()Z

    move-result v0

    if-nez v0, :cond_1

    .line 159
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->object()Lnet/minidev/json/JSONNavi;

    .line 160
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Map;

    if-nez v0, :cond_2

    .line 161
    const-string v0, "current node is not an Object"

    invoke-direct {p0, v0, p1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    move-result-object p0

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public getCurrentObject()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 113
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    return-object v0
.end method

.method public getJPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 672
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 673
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 674
    .local v1, "o":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 675
    const/16 v3, 0x2f

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 677
    :cond_0
    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x5d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 679
    .end local v1    # "o":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getKeys()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRoot()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 484
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 124
    const/4 v0, 0x0

    .line 129
    :goto_0
    return v0

    .line 125
    :cond_0
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 127
    :cond_1
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isObject()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0

    .line 129
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasFailure()Z
    .locals 1

    .prologue
    .line 109
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    return v0
.end method

.method public isArray()Z
    .locals 1

    .prologue
    .line 506
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->isArray(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isObject()Z
    .locals 1

    .prologue
    .line 513
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->isObject(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public object()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 405
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-object p0

    .line 407
    :cond_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->readonly:Z

    if-eqz v0, :cond_2

    .line 408
    const-string v0, "Can not create Object child in readonly"

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 409
    :cond_2
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    if-eqz v0, :cond_4

    .line 410
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isObject()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->isArray()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 413
    const-string v0, "can not use Object feature on Array."

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 414
    :cond_3
    const-string v0, "Can not use current possition as Object"

    invoke-direct {p0, v0, v1}, Lnet/minidev/json/JSONNavi;->failure(Ljava/lang/String;Ljava/lang/Object;)Lnet/minidev/json/JSONNavi;

    .line 418
    :goto_1
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    if-nez v0, :cond_5

    .line 419
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    goto :goto_0

    .line 416
    :cond_4
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->factory:Lnet/minidev/json/parser/ContainerFactory;

    invoke-interface {v0}, Lnet/minidev/json/parser/ContainerFactory;->createObjectContainer()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    goto :goto_1

    .line 421
    :cond_5
    invoke-direct {p0}, Lnet/minidev/json/JSONNavi;->store()V

    goto :goto_0
.end method

.method public root()Lnet/minidev/json/JSONNavi;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 101
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    .line 103
    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->missingKey:Ljava/lang/Object;

    .line 104
    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->failureMessage:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public set(Ljava/lang/Boolean;)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "bool"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 465
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 469
    :goto_0
    return-object p0

    .line 467
    :cond_0
    iput-object p1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 468
    invoke-direct {p0}, Lnet/minidev/json/JSONNavi;->store()V

    goto :goto_0
.end method

.method public set(Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "num"    # Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Number;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 454
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 458
    :goto_0
    return-object p0

    .line 456
    :cond_0
    iput-object p1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 457
    invoke-direct {p0}, Lnet/minidev/json/JSONNavi;->store()V

    goto :goto_0
.end method

.method public set(Ljava/lang/String;)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 476
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 480
    :goto_0
    return-object p0

    .line 478
    :cond_0
    iput-object p1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 479
    invoke-direct {p0}, Lnet/minidev/json/JSONNavi;->store()V

    goto :goto_0
.end method

.method public set(Ljava/lang/String;D)Lnet/minidev/json/JSONNavi;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "D)",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 227
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lnet/minidev/json/JSONNavi;->set(Ljava/lang/String;Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;F)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "F)",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 240
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lnet/minidev/json/JSONNavi;->set(Ljava/lang/String;Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;I)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 214
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lnet/minidev/json/JSONNavi;->set(Ljava/lang/String;Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;J)Lnet/minidev/json/JSONNavi;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lnet/minidev/json/JSONNavi;->set(Ljava/lang/String;Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;

    move-result-object v0

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/Number;)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Number;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 184
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->object()Lnet/minidev/json/JSONNavi;

    .line 185
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 188
    :goto_0
    return-object p0

    .line 187
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)Lnet/minidev/json/JSONNavi;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lnet/minidev/json/JSONNavi",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 176
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    invoke-virtual {p0}, Lnet/minidev/json/JSONNavi;->object()Lnet/minidev/json/JSONNavi;

    .line 177
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 180
    :goto_0
    return-object p0

    .line 179
    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lnet/minidev/json/JSONNavi;->o(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 633
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->failureMessage:Ljava/lang/String;

    sget-object v1, Lnet/minidev/json/JSONNavi;->ERROR_COMPRESS:Lnet/minidev/json/JSONStyle;

    invoke-static {v0, v1}, Lnet/minidev/json/JSONValue;->toJSONString(Ljava/lang/Object;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    .line 635
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    invoke-static {v0}, Lnet/minidev/json/JSONValue;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(Lnet/minidev/json/JSONStyle;)Ljava/lang/String;
    .locals 1
    .param p1, "compression"    # Lnet/minidev/json/JSONStyle;

    .prologue
    .line 644
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-boolean v0, p0, Lnet/minidev/json/JSONNavi;->failure:Z

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->failureMessage:Ljava/lang/String;

    invoke-static {v0, p1}, Lnet/minidev/json/JSONValue;->toJSONString(Ljava/lang/Object;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    .line 646
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->root:Ljava/lang/Object;

    invoke-static {v0, p1}, Lnet/minidev/json/JSONValue;->toJSONString(Ljava/lang/Object;Lnet/minidev/json/JSONStyle;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public up()Lnet/minidev/json/JSONNavi;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 620
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 621
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 622
    iget-object v0, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 624
    :cond_0
    return-object p0
.end method

.method public up(I)Lnet/minidev/json/JSONNavi;
    .locals 2
    .param p1, "level"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lnet/minidev/json/JSONNavi",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 605
    .local p0, "this":Lnet/minidev/json/JSONNavi;, "Lnet/minidev/json/JSONNavi<TT;>;"
    move v0, p1

    .end local p1    # "level":I
    .local v0, "level":I
    :goto_0
    add-int/lit8 p1, v0, -0x1

    .end local v0    # "level":I
    .restart local p1    # "level":I
    if-lez v0, :cond_0

    .line 606
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 607
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->stack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lnet/minidev/json/JSONNavi;->current:Ljava/lang/Object;

    .line 608
    iget-object v1, p0, Lnet/minidev/json/JSONNavi;->path:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move v0, p1

    .end local p1    # "level":I
    .restart local v0    # "level":I
    goto :goto_0

    .line 612
    .end local v0    # "level":I
    .restart local p1    # "level":I
    :cond_0
    return-object p0
.end method
