.class public Lcom/google/maps/internal/ApiConfig;
.super Ljava/lang/Object;
.source "ApiConfig.java"


# instance fields
.field public fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

.field public hostName:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public supportsClientId:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    sget-object v0, Lcom/google/gson/FieldNamingPolicy;->LOWER_CASE_WITH_UNDERSCORES:Lcom/google/gson/FieldNamingPolicy;

    iput-object v0, p0, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    .line 11
    const-string v0, "https://maps.googleapis.com"

    iput-object v0, p0, Lcom/google/maps/internal/ApiConfig;->hostName:Ljava/lang/String;

    .line 12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/maps/internal/ApiConfig;->supportsClientId:Z

    .line 15
    iput-object p1, p0, Lcom/google/maps/internal/ApiConfig;->path:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public fieldNamingPolicy(Lcom/google/gson/FieldNamingPolicy;)Lcom/google/maps/internal/ApiConfig;
    .locals 0
    .param p1, "fieldNamingPolicy"    # Lcom/google/gson/FieldNamingPolicy;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/maps/internal/ApiConfig;->fieldNamingPolicy:Lcom/google/gson/FieldNamingPolicy;

    .line 20
    return-object p0
.end method

.method public hostName(Ljava/lang/String;)Lcom/google/maps/internal/ApiConfig;
    .locals 0
    .param p1, "hostName"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/maps/internal/ApiConfig;->hostName:Ljava/lang/String;

    .line 25
    return-object p0
.end method

.method public supportsClientId(Z)Lcom/google/maps/internal/ApiConfig;
    .locals 0
    .param p1, "supportsClientId"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/maps/internal/ApiConfig;->supportsClientId:Z

    .line 30
    return-object p0
.end method
