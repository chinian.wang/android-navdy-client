.class public Lcom/google/maps/DirectionsApiRequest;
.super Lcom/google/maps/PendingResultBase;
.source "DirectionsApiRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/maps/PendingResultBase",
        "<[",
        "Lcom/google/maps/model/DirectionsRoute;",
        "Lcom/google/maps/DirectionsApiRequest;",
        "Lcom/google/maps/DirectionsApi$Response;",
        ">;"
    }
.end annotation


# instance fields
.field private optimizeWaypoints:Z

.field private waypoints:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/maps/GeoApiContext;)V
    .locals 2
    .param p1, "context"    # Lcom/google/maps/GeoApiContext;

    .prologue
    .line 39
    sget-object v0, Lcom/google/maps/DirectionsApi;->API_CONFIG:Lcom/google/maps/internal/ApiConfig;

    const-class v1, Lcom/google/maps/DirectionsApi$Response;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/maps/PendingResultBase;-><init>(Lcom/google/maps/GeoApiContext;Lcom/google/maps/internal/ApiConfig;Ljava/lang/Class;)V

    .line 40
    return-void
.end method


# virtual methods
.method public alternatives(Z)Lcom/google/maps/DirectionsApiRequest;
    .locals 2
    .param p1, "alternateRoutes"    # Z

    .prologue
    .line 182
    if-eqz p1, :cond_0

    .line 183
    const-string v0, "alternatives"

    const-string v1, "true"

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    .line 185
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "alternatives"

    const-string v1, "false"

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    goto :goto_0
.end method

.method public arrivalTime(Lorg/joda/time/ReadableInstant;)Lcom/google/maps/DirectionsApiRequest;
    .locals 6
    .param p1, "time"    # Lorg/joda/time/ReadableInstant;

    .prologue
    .line 131
    const-string v0, "arrival_time"

    invoke-interface {p1}, Lorg/joda/time/ReadableInstant;->getMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public varargs avoid([Lcom/google/maps/DirectionsApi$RouteRestriction;)Lcom/google/maps/DirectionsApiRequest;
    .locals 2
    .param p1, "restrictions"    # [Lcom/google/maps/DirectionsApi$RouteRestriction;

    .prologue
    .line 108
    const-string v0, "avoid"

    const/16 v1, 0x7c

    invoke-static {v1, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public departureTime(Lorg/joda/time/ReadableInstant;)Lcom/google/maps/DirectionsApiRequest;
    .locals 6
    .param p1, "time"    # Lorg/joda/time/ReadableInstant;

    .prologue
    .line 140
    const-string v0, "departure_time"

    invoke-interface {p1}, Lorg/joda/time/ReadableInstant;->getMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public destination(Lcom/google/maps/model/LatLng;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "destination"    # Lcom/google/maps/model/LatLng;

    .prologue
    .line 87
    invoke-virtual {p1}, Lcom/google/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/DirectionsApiRequest;->destination(Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object v0

    return-object v0
.end method

.method public destination(Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "destination"    # Ljava/lang/String;

    .prologue
    .line 73
    const-string v0, "destination"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public mode(Lcom/google/maps/model/TravelMode;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "mode"    # Lcom/google/maps/model/TravelMode;

    .prologue
    .line 98
    const-string v0, "mode"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Lcom/google/maps/internal/StringJoin$UrlValue;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public optimizeWaypoints(Z)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "optimize"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/google/maps/DirectionsApiRequest;->optimizeWaypoints:Z

    .line 169
    iget-object v0, p0, Lcom/google/maps/DirectionsApiRequest;->waypoints:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/maps/DirectionsApiRequest;->waypoints:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/maps/DirectionsApiRequest;->waypoints([Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object p0

    .line 172
    .end local p0    # "this":Lcom/google/maps/DirectionsApiRequest;
    :cond_0
    return-object p0
.end method

.method public origin(Lcom/google/maps/model/LatLng;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "origin"    # Lcom/google/maps/model/LatLng;

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/google/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/maps/DirectionsApiRequest;->origin(Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;

    move-result-object v0

    return-object v0
.end method

.method public origin(Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "origin"    # Ljava/lang/String;

    .prologue
    .line 63
    const-string v0, "origin"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public region(Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "region"    # Ljava/lang/String;

    .prologue
    .line 122
    const-string v0, "region"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public varargs transitMode([Lcom/google/maps/model/TransitMode;)Lcom/google/maps/DirectionsApiRequest;
    .locals 2
    .param p1, "transitModes"    # [Lcom/google/maps/model/TransitMode;

    .prologue
    .line 194
    const-string v0, "transit_mode"

    const/16 v1, 0x7c

    invoke-static {v1, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Lcom/google/maps/internal/StringJoin$UrlValue;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public transitRoutingPreference(Lcom/google/maps/model/TransitRoutingPreference;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "pref"    # Lcom/google/maps/model/TransitRoutingPreference;

    .prologue
    .line 203
    const-string v0, "transit_routing_preference"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Lcom/google/maps/internal/StringJoin$UrlValue;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method public units(Lcom/google/maps/model/Unit;)Lcom/google/maps/DirectionsApiRequest;
    .locals 1
    .param p1, "units"    # Lcom/google/maps/model/Unit;

    .prologue
    .line 115
    const-string v0, "units"

    invoke-virtual {p0, v0, p1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Lcom/google/maps/internal/StringJoin$UrlValue;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    return-object v0
.end method

.method protected validateRequest()V
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/maps/DirectionsApiRequest;->params()Ljava/util/Map;

    move-result-object v0

    const-string v1, "origin"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request must contain \'origin\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/DirectionsApiRequest;->params()Ljava/util/Map;

    move-result-object v0

    const-string v1, "destination"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request must contain \'destination\'"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    sget-object v0, Lcom/google/maps/model/TravelMode;->TRANSIT:Lcom/google/maps/model/TravelMode;

    invoke-virtual {v0}, Lcom/google/maps/model/TravelMode;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/maps/DirectionsApiRequest;->params()Ljava/util/Map;

    move-result-object v1

    const-string v2, "mode"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/maps/DirectionsApiRequest;->params()Ljava/util/Map;

    move-result-object v0

    const-string v1, "arrival_time"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/maps/DirectionsApiRequest;->params()Ljava/util/Map;

    move-result-object v0

    const-string v1, "departure_time"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transit request must not contain both a departureTime and an arrivalTime"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_2
    return-void
.end method

.method public varargs waypoints([Ljava/lang/String;)Lcom/google/maps/DirectionsApiRequest;
    .locals 4
    .param p1, "waypoints"    # [Ljava/lang/String;

    .prologue
    .line 154
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p0

    .line 159
    :goto_0
    return-object v0

    .line 156
    :cond_1
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 157
    const-string v0, "waypoints"

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {p0, v0, v1}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    goto :goto_0

    .line 159
    :cond_2
    const-string v1, "waypoints"

    iget-boolean v0, p0, Lcom/google/maps/DirectionsApiRequest;->optimizeWaypoints:Z

    if-eqz v0, :cond_3

    const-string v0, "optimize:true|"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/16 v0, 0x7c

    invoke-static {v0, p1}, Lcom/google/maps/internal/StringJoin;->join(C[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/google/maps/DirectionsApiRequest;->param(Ljava/lang/String;Ljava/lang/String;)Lcom/google/maps/PendingResultBase;

    move-result-object v0

    check-cast v0, Lcom/google/maps/DirectionsApiRequest;

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method
