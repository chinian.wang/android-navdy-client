.class public final Lcom/nimbusds/jose/jca/JWEJCAContext;
.super Lcom/nimbusds/jose/jca/JCAContext;
.source "JWEJCAContext.java"


# instance fields
.field private ceProvider:Ljava/security/Provider;

.field private keProvider:Ljava/security/Provider;

.field private macProvider:Ljava/security/Provider;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 42
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/nimbusds/jose/jca/JWEJCAContext;-><init>(Ljava/security/Provider;Ljava/security/Provider;Ljava/security/Provider;Ljava/security/Provider;Ljava/security/SecureRandom;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/security/Provider;Ljava/security/Provider;Ljava/security/Provider;Ljava/security/Provider;Ljava/security/SecureRandom;)V
    .locals 0
    .param p1, "generalProvider"    # Ljava/security/Provider;
    .param p2, "keProvider"    # Ljava/security/Provider;
    .param p3, "ceProvider"    # Ljava/security/Provider;
    .param p4, "macProvider"    # Ljava/security/Provider;
    .param p5, "randomGen"    # Ljava/security/SecureRandom;

    .prologue
    .line 78
    invoke-direct {p0, p1, p5}, Lcom/nimbusds/jose/jca/JCAContext;-><init>(Ljava/security/Provider;Ljava/security/SecureRandom;)V

    .line 79
    iput-object p2, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->keProvider:Ljava/security/Provider;

    .line 80
    iput-object p3, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->ceProvider:Ljava/security/Provider;

    .line 81
    iput-object p4, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->macProvider:Ljava/security/Provider;

    .line 82
    return-void
.end method


# virtual methods
.method public getContentEncryptionProvider()Ljava/security/Provider;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->ceProvider:Ljava/security/Provider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->ceProvider:Ljava/security/Provider;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v0

    goto :goto_0
.end method

.method public getKeyEncryptionProvider()Ljava/security/Provider;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->keProvider:Ljava/security/Provider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->keProvider:Ljava/security/Provider;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v0

    goto :goto_0
.end method

.method public getMACProvider()Ljava/security/Provider;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->macProvider:Ljava/security/Provider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->macProvider:Ljava/security/Provider;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/nimbusds/jose/jca/JWEJCAContext;->getProvider()Ljava/security/Provider;

    move-result-object v0

    goto :goto_0
.end method

.method public setContentEncryptionProvider(Ljava/security/Provider;)V
    .locals 0
    .param p1, "ceProvider"    # Ljava/security/Provider;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->ceProvider:Ljava/security/Provider;

    .line 123
    return-void
.end method

.method public setKeyEncryptionProvider(Ljava/security/Provider;)V
    .locals 0
    .param p1, "keProvider"    # Ljava/security/Provider;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->keProvider:Ljava/security/Provider;

    .line 97
    return-void
.end method

.method public setMACProvider(Ljava/security/Provider;)V
    .locals 0
    .param p1, "macProvider"    # Ljava/security/Provider;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/nimbusds/jose/jca/JWEJCAContext;->macProvider:Ljava/security/Provider;

    .line 151
    return-void
.end method
