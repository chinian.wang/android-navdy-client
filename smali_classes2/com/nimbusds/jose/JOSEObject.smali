.class public abstract Lcom/nimbusds/jose/JOSEObject;
.super Ljava/lang/Object;
.source "JOSEObject.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final MIME_TYPE_COMPACT:Ljava/lang/String; = "application/jose; charset=UTF-8"

.field public static final MIME_TYPE_JS:Ljava/lang/String; = "application/jose+json; charset=UTF-8"

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

.field private payload:Lcom/nimbusds/jose/Payload;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/nimbusds/jose/JOSEObject;->payload:Lcom/nimbusds/jose/Payload;

    .line 61
    iput-object v0, p0, Lcom/nimbusds/jose/JOSEObject;->parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

    .line 62
    return-void
.end method

.method protected constructor <init>(Lcom/nimbusds/jose/Payload;)V
    .locals 0
    .param p1, "payload"    # Lcom/nimbusds/jose/Payload;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/nimbusds/jose/JOSEObject;->payload:Lcom/nimbusds/jose/Payload;

    .line 74
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/nimbusds/jose/JOSEObject;
    .locals 8
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 262
    invoke-static {p0}, Lcom/nimbusds/jose/JOSEObject;->split(Ljava/lang/String;)[Lcom/nimbusds/jose/util/Base64URL;

    move-result-object v3

    .line 267
    .local v3, "parts":[Lcom/nimbusds/jose/util/Base64URL;
    const/4 v4, 0x0

    :try_start_0
    aget-object v4, v3, v4

    invoke-virtual {v4}, Lcom/nimbusds/jose/util/Base64URL;->decodeToString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/nimbusds/jose/util/JSONObjectUtils;->parse(Ljava/lang/String;)Lnet/minidev/json/JSONObject;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 274
    .local v2, "jsonObject":Lnet/minidev/json/JSONObject;
    invoke-static {v2}, Lcom/nimbusds/jose/Header;->parseAlgorithm(Lnet/minidev/json/JSONObject;)Lcom/nimbusds/jose/Algorithm;

    move-result-object v0

    .line 276
    .local v0, "alg":Lcom/nimbusds/jose/Algorithm;
    sget-object v4, Lcom/nimbusds/jose/Algorithm;->NONE:Lcom/nimbusds/jose/Algorithm;

    invoke-virtual {v0, v4}, Lcom/nimbusds/jose/Algorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 277
    invoke-static {p0}, Lcom/nimbusds/jose/PlainObject;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/PlainObject;

    move-result-object v4

    .line 281
    :goto_0
    return-object v4

    .line 269
    .end local v0    # "alg":Lcom/nimbusds/jose/Algorithm;
    .end local v2    # "jsonObject":Lnet/minidev/json/JSONObject;
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/text/ParseException;
    new-instance v4, Ljava/text/ParseException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid unsecured/JWS/JWE header: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v4

    .line 278
    .end local v1    # "e":Ljava/text/ParseException;
    .restart local v0    # "alg":Lcom/nimbusds/jose/Algorithm;
    .restart local v2    # "jsonObject":Lnet/minidev/json/JSONObject;
    :cond_0
    instance-of v4, v0, Lcom/nimbusds/jose/JWSAlgorithm;

    if-eqz v4, :cond_1

    .line 279
    invoke-static {p0}, Lcom/nimbusds/jose/JWSObject;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/JWSObject;

    move-result-object v4

    goto :goto_0

    .line 280
    :cond_1
    instance-of v4, v0, Lcom/nimbusds/jose/JWEAlgorithm;

    if-eqz v4, :cond_2

    .line 281
    invoke-static {p0}, Lcom/nimbusds/jose/JWEObject;->parse(Ljava/lang/String;)Lcom/nimbusds/jose/JWEObject;

    move-result-object v4

    goto :goto_0

    .line 283
    :cond_2
    new-instance v4, Ljava/lang/AssertionError;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected algorithm type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v4
.end method

.method public static split(Ljava/lang/String;)[Lcom/nimbusds/jose/util/Base64URL;
    .locals 12
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 201
    const-string v5, "."

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 203
    .local v0, "dot1":I
    if-ne v0, v8, :cond_0

    .line 204
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Invalid serialized unsecured/JWS/JWE object: Missing part delimiters"

    invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 207
    :cond_0
    const-string v5, "."

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 209
    .local v1, "dot2":I
    if-ne v1, v8, :cond_1

    .line 210
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Invalid serialized unsecured/JWS/JWE object: Missing second delimiter"

    invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 214
    :cond_1
    const-string v5, "."

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 216
    .local v2, "dot3":I
    if-ne v2, v8, :cond_2

    .line 219
    new-array v4, v11, [Lcom/nimbusds/jose/util/Base64URL;

    .line 220
    .local v4, "parts":[Lcom/nimbusds/jose/util/Base64URL;
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {p0, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v7

    .line 221
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v9

    .line 222
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v10

    .line 244
    :goto_0
    return-object v4

    .line 227
    .end local v4    # "parts":[Lcom/nimbusds/jose/util/Base64URL;
    :cond_2
    const-string v5, "."

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    .line 229
    .local v3, "dot4":I
    if-ne v3, v8, :cond_3

    .line 230
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Invalid serialized JWE object: Missing fourth delimiter"

    invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 233
    :cond_3
    if-eq v3, v8, :cond_4

    const-string v5, "."

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    if-eq v5, v8, :cond_4

    .line 234
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Invalid serialized unsecured/JWS/JWE object: Too many part delimiters"

    invoke-direct {v5, v6, v7}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 238
    :cond_4
    const/4 v5, 0x5

    new-array v4, v5, [Lcom/nimbusds/jose/util/Base64URL;

    .line 239
    .restart local v4    # "parts":[Lcom/nimbusds/jose/util/Base64URL;
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    invoke-virtual {p0, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v7

    .line 240
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v9

    .line 241
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v10

    .line 242
    new-instance v5, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {p0, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v5, v4, v11

    .line 243
    const/4 v5, 0x4

    new-instance v6, Lcom/nimbusds/jose/util/Base64URL;

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/nimbusds/jose/util/Base64URL;-><init>(Ljava/lang/String;)V

    aput-object v6, v4, v5

    goto :goto_0
.end method


# virtual methods
.method public abstract getHeader()Lcom/nimbusds/jose/Header;
.end method

.method public getParsedParts()[Lcom/nimbusds/jose/util/Base64URL;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/nimbusds/jose/JOSEObject;->parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

    return-object v0
.end method

.method public getParsedString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 149
    iget-object v2, p0, Lcom/nimbusds/jose/JOSEObject;->parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

    if-nez v2, :cond_0

    .line 150
    const/4 v2, 0x0

    .line 166
    :goto_0
    return-object v2

    .line 153
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 155
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/nimbusds/jose/JOSEObject;->parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_1
    if-lt v2, v4, :cond_1

    .line 166
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 155
    :cond_1
    aget-object v0, v3, v2

    .line 157
    .local v0, "part":Lcom/nimbusds/jose/util/Base64URL;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 158
    const/16 v5, 0x2e

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 161
    :cond_2
    if-eqz v0, :cond_3

    .line 162
    invoke-virtual {v0}, Lcom/nimbusds/jose/util/Base64URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getPayload()Lcom/nimbusds/jose/Payload;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/nimbusds/jose/JOSEObject;->payload:Lcom/nimbusds/jose/Payload;

    return-object v0
.end method

.method public abstract serialize()Ljava/lang/String;
.end method

.method protected varargs setParsedParts([Lcom/nimbusds/jose/util/Base64URL;)V
    .locals 0
    .param p1, "parts"    # [Lcom/nimbusds/jose/util/Base64URL;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/nimbusds/jose/JOSEObject;->parsedParts:[Lcom/nimbusds/jose/util/Base64URL;

    .line 121
    return-void
.end method

.method protected setPayload(Lcom/nimbusds/jose/Payload;)V
    .locals 0
    .param p1, "payload"    # Lcom/nimbusds/jose/Payload;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/nimbusds/jose/JOSEObject;->payload:Lcom/nimbusds/jose/Payload;

    .line 94
    return-void
.end method
