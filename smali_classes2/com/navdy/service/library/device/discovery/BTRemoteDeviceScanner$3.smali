.class Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;
.super Landroid/content/BroadcastReceiver;
.source "BTRemoteDeviceScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 105
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "action":Ljava/lang/String;
    const-string v5, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "android.bluetooth.device.action.NAME_CHANGED"

    .line 109
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 110
    :cond_0
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    .line 111
    .local v2, "device":Landroid/bluetooth/BluetoothDevice;
    const-string v5, "android.bluetooth.device.extra.NAME"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 112
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "address":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Scanned: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - name = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 114
    if-eqz v4, :cond_1

    invoke-static {v4}, Lcom/navdy/service/library/device/discovery/BTDeviceBroadcaster;->isDisplay(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 115
    iget-object v5, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v5}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$200(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 116
    iget-object v5, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-static {v5}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->access$200(Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v3, Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v5, Lcom/navdy/service/library/device/NavdyDeviceId$Type;->BT:Lcom/navdy/service/library/device/NavdyDeviceId$Type;

    invoke-direct {v3, v5, v1, v4}, Lcom/navdy/service/library/device/NavdyDeviceId;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId$Type;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .local v3, "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    iget-object v5, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    new-instance v6, Lcom/navdy/service/library/device/connection/BTConnectionInfo;

    new-instance v7, Lcom/navdy/service/library/device/connection/ServiceAddress;

    sget-object v8, Lcom/navdy/service/library/device/connection/ConnectionService;->NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

    .line 123
    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v1, v8}, Lcom/navdy/service/library/device/connection/ServiceAddress;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v8, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v6, v3, v7, v8}, Lcom/navdy/service/library/device/connection/BTConnectionInfo;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;Lcom/navdy/service/library/device/connection/ServiceAddress;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    .line 121
    invoke-virtual {v5, v6}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->dispatchOnDiscovered(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 131
    .end local v1    # "address":Ljava/lang/String;
    .end local v2    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v3    # "deviceId":Lcom/navdy/service/library/device/NavdyDeviceId;
    .end local v4    # "name":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    const-string v5, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 129
    iget-object v5, p0, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner$3;->this$0:Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;

    invoke-virtual {v5}, Lcom/navdy/service/library/device/discovery/BTRemoteDeviceScanner;->dispatchOnScanStopped()V

    goto :goto_0
.end method
