.class Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;
.super Ljava/util/TimerTask;
.source "MDNSRemoteDeviceScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->startScan()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 66
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scan timer fired: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v0, v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mNsdManager:Landroid/net/nsd/NsdManager;

    iget-object v1, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v1, v1, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v3, v3, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mDiscoveryListener:Landroid/net/nsd/NsdManager$DiscoveryListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/nsd/NsdManager;->discoverServices(Ljava/lang/String;ILandroid/net/nsd/NsdManager$DiscoveryListener;)V

    .line 69
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scan started: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSRemoteDeviceScanner;->mServiceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 71
    return-void
.end method
