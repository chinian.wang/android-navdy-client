.class Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;
.super Ljava/lang/Object;
.source "MDNSDeviceBroadcaster.java"

# interfaces
.implements Landroid/net/nsd/NsdManager$RegistrationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->getRegistrationListener()Landroid/net/nsd/NsdManager$RegistrationListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRegistrationFailed(Landroid/net/nsd/NsdServiceInfo;I)V
    .locals 3
    .param p1, "nsdServiceInfo"    # Landroid/net/nsd/NsdServiceInfo;
    .param p2, "errorCode"    # I

    .prologue
    .line 70
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SERVICE REGISTRATION FAILED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public onServiceRegistered(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "nsdServiceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SERVICE REGISTERED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onServiceUnregistered(Landroid/net/nsd/NsdServiceInfo;)V
    .locals 3
    .param p1, "nsdServiceInfo"    # Landroid/net/nsd/NsdServiceInfo;

    .prologue
    .line 75
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SERVICE UNREGISTERED"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public onUnregistrationFailed(Landroid/net/nsd/NsdServiceInfo;I)V
    .locals 3
    .param p1, "nsdServiceInfo"    # Landroid/net/nsd/NsdServiceInfo;
    .param p2, "errorCode"    # I

    .prologue
    .line 80
    sget-object v0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SERVICE UNREGISTRATION FAILED: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster$1;->this$0:Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;

    iget-object v2, v2, Lcom/navdy/service/library/device/discovery/MDNSDeviceBroadcaster;->mServiceInfo:Landroid/net/nsd/NsdServiceInfo;

    invoke-virtual {v2}, Landroid/net/nsd/NsdServiceInfo;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 81
    return-void
.end method
