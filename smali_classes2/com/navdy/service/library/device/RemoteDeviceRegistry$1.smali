.class Lcom/navdy/service/library/device/RemoteDeviceRegistry$1;
.super Ljava/lang/Object;
.source "RemoteDeviceRegistry.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListEventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/device/RemoteDeviceRegistry;->sendDeviceListUpdate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/RemoteDeviceRegistry;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/RemoteDeviceRegistry;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDeviceRegistry$1;->this$0:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/service/library/device/RemoteDeviceRegistry;Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;)V
    .locals 2
    .param p1, "source"    # Lcom/navdy/service/library/device/RemoteDeviceRegistry;
    .param p2, "listener"    # Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    .prologue
    .line 126
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/navdy/service/library/device/RemoteDeviceRegistry$1;->this$0:Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    iget-object v1, v1, Lcom/navdy/service/library/device/RemoteDeviceRegistry;->mKnownConnectionInfo:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 127
    .local v0, "connectionInfo":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    invoke-interface {p2, v0}, Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;->onDeviceListChanged(Ljava/util/Set;)V

    .line 128
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 123
    check-cast p1, Lcom/navdy/service/library/device/RemoteDeviceRegistry;

    check-cast p2, Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/service/library/device/RemoteDeviceRegistry$1;->dispatchEvent(Lcom/navdy/service/library/device/RemoteDeviceRegistry;Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;)V

    return-void
.end method
