.class public final Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConnectionStateChange.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionStateChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionStateChange;",
        ">;"
    }
.end annotation


# instance fields
.field public remoteDeviceId:Ljava/lang/String;

.field public state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionStateChange;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 69
    if-nez p1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->remoteDeviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->remoteDeviceId:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionStateChange;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/connection/ConnectionStateChange;
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->checkRequiredFields()V

    .line 87
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/connection/ConnectionStateChange;-><init>(Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;Lcom/navdy/service/library/events/connection/ConnectionStateChange$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->build()Lcom/navdy/service/library/events/connection/ConnectionStateChange;

    move-result-object v0

    return-object v0
.end method

.method public remoteDeviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    .locals 0
    .param p1, "remoteDeviceId"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->remoteDeviceId:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public state(Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;)Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;
    .locals 0
    .param p1, "state"    # Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$Builder;->state:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 81
    return-object p0
.end method
