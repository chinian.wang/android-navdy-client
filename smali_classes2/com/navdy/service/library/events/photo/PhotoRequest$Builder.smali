.class public final Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public identifier:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public photoChecksum:Ljava/lang/String;

.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 89
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoRequest;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 93
    if-nez p1, :cond_0

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoRequest;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->identifier:Ljava/lang/String;

    .line 95
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoChecksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoChecksum:Ljava/lang/String;

    .line 96
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 97
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoRequest;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->name:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/photo/PhotoRequest;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->checkRequiredFields()V

    .line 136
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoRequest;-><init>(Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;Lcom/navdy/service/library/events/photo/PhotoRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoRequest;

    move-result-object v0

    return-object v0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->identifier:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->name:Ljava/lang/String;

    .line 130
    return-object p0
.end method

.method public photoChecksum(Ljava/lang/String;)Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    .locals 0
    .param p1, "photoChecksum"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoChecksum:Ljava/lang/String;

    .line 113
    return-object p0
.end method

.method public photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;
    .locals 0
    .param p1, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 121
    return-object p0
.end method
