.class public final Lcom/navdy/service/library/events/Capabilities$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Capabilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/Capabilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/Capabilities;",
        ">;"
    }
.end annotation


# instance fields
.field public cannedResponseToSms:Ljava/lang/Boolean;

.field public compactUi:Ljava/lang/Boolean;

.field public customDialLongPress:Ljava/lang/Boolean;

.field public localMusicBrowser:Ljava/lang/Boolean;

.field public musicArtworkCache:Ljava/lang/Boolean;

.field public navCoordsLookup:Ljava/lang/Boolean;

.field public placeTypeSearch:Ljava/lang/Boolean;

.field public searchResultList:Ljava/lang/Boolean;

.field public supportsPhoneticTts:Ljava/lang/Boolean;

.field public voiceSearch:Ljava/lang/Boolean;

.field public voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 181
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/Capabilities;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/Capabilities;

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 185
    if-nez p1, :cond_0

    .line 197
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->compactUi:Ljava/lang/Boolean;

    .line 187
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->placeTypeSearch:Ljava/lang/Boolean;

    .line 188
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearch:Ljava/lang/Boolean;

    .line 189
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->localMusicBrowser:Ljava/lang/Boolean;

    .line 190
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->navCoordsLookup:Ljava/lang/Boolean;

    .line 191
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->searchResultList:Ljava/lang/Boolean;

    .line 192
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->musicArtworkCache:Ljava/lang/Boolean;

    .line 193
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    .line 194
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->cannedResponseToSms:Ljava/lang/Boolean;

    .line 195
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->customDialLongPress:Ljava/lang/Boolean;

    .line 196
    iget-object v0, p1, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->supportsPhoneticTts:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/Capabilities;
    .locals 2

    .prologue
    .line 307
    new-instance v0, Lcom/navdy/service/library/events/Capabilities;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/Capabilities;-><init>(Lcom/navdy/service/library/events/Capabilities$Builder;Lcom/navdy/service/library/events/Capabilities$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/navdy/service/library/events/Capabilities$Builder;->build()Lcom/navdy/service/library/events/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public cannedResponseToSms(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "cannedResponseToSms"    # Ljava/lang/Boolean;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->cannedResponseToSms:Ljava/lang/Boolean;

    .line 285
    return-object p0
.end method

.method public compactUi(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "compactUi"    # Ljava/lang/Boolean;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->compactUi:Ljava/lang/Boolean;

    .line 204
    return-object p0
.end method

.method public customDialLongPress(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "customDialLongPress"    # Ljava/lang/Boolean;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->customDialLongPress:Ljava/lang/Boolean;

    .line 293
    return-object p0
.end method

.method public localMusicBrowser(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "localMusicBrowser"    # Ljava/lang/Boolean;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->localMusicBrowser:Ljava/lang/Boolean;

    .line 232
    return-object p0
.end method

.method public musicArtworkCache(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "musicArtworkCache"    # Ljava/lang/Boolean;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->musicArtworkCache:Ljava/lang/Boolean;

    .line 263
    return-object p0
.end method

.method public navCoordsLookup(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "navCoordsLookup"    # Ljava/lang/Boolean;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->navCoordsLookup:Ljava/lang/Boolean;

    .line 241
    return-object p0
.end method

.method public placeTypeSearch(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "placeTypeSearch"    # Ljava/lang/Boolean;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->placeTypeSearch:Ljava/lang/Boolean;

    .line 215
    return-object p0
.end method

.method public searchResultList(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "searchResultList"    # Ljava/lang/Boolean;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->searchResultList:Ljava/lang/Boolean;

    .line 255
    return-object p0
.end method

.method public supportsPhoneticTts(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "supportsPhoneticTts"    # Ljava/lang/Boolean;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->supportsPhoneticTts:Ljava/lang/Boolean;

    .line 302
    return-object p0
.end method

.method public voiceSearch(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "voiceSearch"    # Ljava/lang/Boolean;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearch:Ljava/lang/Boolean;

    .line 224
    return-object p0
.end method

.method public voiceSearchNewIOSPauseBehaviors(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/Capabilities$Builder;
    .locals 0
    .param p1, "voiceSearchNewIOSPauseBehaviors"    # Ljava/lang/Boolean;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    .line 273
    return-object p0
.end method
