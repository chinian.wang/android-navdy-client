.class public interface abstract Lcom/navdy/service/library/network/SocketAcceptor;
.super Ljava/lang/Object;
.source "SocketAcceptor.java"

# interfaces
.implements Ljava/io/Closeable;


# virtual methods
.method public abstract accept()Lcom/navdy/service/library/network/SocketAdapter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getRemoteConnectionInfo(Lcom/navdy/service/library/network/SocketAdapter;Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/service/library/device/connection/ConnectionInfo;
.end method
