.class Lcom/navdy/service/library/network/http/services/JiraClient$1;
.super Ljava/lang/Object;
.source "JiraClient.java"

# interfaces
.implements Lokhttp3/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/service/library/network/http/services/JiraClient;->submitTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

.field final synthetic val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/network/http/services/JiraClient;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/network/http/services/JiraClient;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->this$0:Lcom/navdy/service/library/network/http/services/JiraClient;

    iput-object p2, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lokhttp3/Call;Ljava/io/IOException;)V
    .locals 1
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v0, p2}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    .line 89
    :cond_0
    return-void
.end method

.method public onResponse(Lokhttp3/Call;Lokhttp3/Response;)V
    .locals 9
    .param p1, "call"    # Lokhttp3/Call;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    :try_start_0
    invoke-virtual {p2}, Lokhttp3/Response;->code()I

    move-result v2

    .line 95
    .local v2, "responseCode":I
    const/16 v5, 0xc8

    if-eq v2, v5, :cond_0

    const/16 v5, 0xc9

    if-ne v2, v5, :cond_2

    .line 96
    :cond_0
    invoke-virtual {p2}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v5

    invoke-virtual {v5}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, "jsonResponse":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 98
    .local v3, "responseJsonObject":Lorg/json/JSONObject;
    const-string v5, "key"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v5, :cond_1

    .line 100
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v5, v1}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onSuccess(Ljava/lang/Object;)V

    .line 112
    .end local v0    # "jsonResponse":Ljava/lang/String;
    .end local v1    # "key":Ljava/lang/String;
    .end local v2    # "responseCode":I
    .end local v3    # "responseJsonObject":Lorg/json/JSONObject;
    :cond_1
    :goto_0
    return-void

    .line 103
    .restart local v2    # "responseCode":I
    :cond_2
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v5, :cond_1

    .line 104
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Jira request failed with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v5, v6}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v2    # "responseCode":I
    :catch_0
    move-exception v4

    .line 108
    .local v4, "t":Ljava/lang/Throwable;
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    if-eqz v5, :cond_1

    .line 109
    iget-object v5, p0, Lcom/navdy/service/library/network/http/services/JiraClient$1;->val$callback:Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;

    invoke-interface {v5, v4}, Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;->onError(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
