.class Lcom/navdy/client/app/service/DataCollectionService$4;
.super Ljava/lang/Object;
.source "DataCollectionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/service/DataCollectionService;->handleDetectedActivities(ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/service/DataCollectionService;

.field final synthetic val$confidence:I

.field final synthetic val$isDriving:Z


# direct methods
.method constructor <init>(Lcom/navdy/client/app/service/DataCollectionService;IZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/service/DataCollectionService;

    .prologue
    .line 842
    iput-object p1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iput p2, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$confidence:I

    iput-boolean p3, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$isDriving:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 845
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v1}, Lcom/navdy/client/app/service/DataCollectionService;->access$100(Lcom/navdy/client/app/service/DataCollectionService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 846
    :try_start_0
    new-instance v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;

    iget v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$confidence:I

    invoke-direct {v0, v1}, Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;-><init>(I)V

    .line 847
    .local v0, "activity":Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;
    iget-boolean v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$isDriving:Z

    iput-boolean v1, v0, Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;->isDriving:Z

    .line 848
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-boolean v3, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$isDriving:Z

    invoke-static {v1, v3}, Lcom/navdy/client/app/service/DataCollectionService;->access$602(Lcom/navdy/client/app/service/DataCollectionService;Z)Z

    .line 849
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 851
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    rem-int/lit16 v1, v1, 0x3e8

    if-nez v1, :cond_0

    .line 852
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACTIVITY_MONITOR   : nbRecords="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v4, v4, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \tisDriving="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$isDriving:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " \tconfidence="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->val$confidence:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 856
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-object v1, v1, Lcom/navdy/client/app/service/DataCollectionService$Data;->activities:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const v3, 0xb71b0

    if-le v1, v3, :cond_1

    .line 857
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v3, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    iget-object v3, v3, Lcom/navdy/client/app/service/DataCollectionService;->data:Lcom/navdy/client/app/service/DataCollectionService$Data;

    iget-wide v4, v3, Lcom/navdy/client/app/service/DataCollectionService$Data;->tripUuid:J

    invoke-static {v1, v4, v5}, Lcom/navdy/client/app/service/DataCollectionService;->access$800(Lcom/navdy/client/app/service/DataCollectionService;J)V

    .line 859
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/service/DataCollectionService$4;->this$0:Lcom/navdy/client/app/service/DataCollectionService;

    invoke-static {v1}, Lcom/navdy/client/app/service/DataCollectionService;->access$500(Lcom/navdy/client/app/service/DataCollectionService;)V

    .line 860
    monitor-exit v2

    .line 861
    return-void

    .line 860
    .end local v0    # "activity":Lcom/navdy/client/app/service/DataCollectionService$Data$Activity;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
