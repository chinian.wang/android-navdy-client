.class Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;
.super Ljava/lang/Object;
.source "NetworkStatusManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 98
    const/4 v3, 0x0

    .line 100
    .local v3, "response":Lokhttp3/Response;
    :try_start_0
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$200(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 101
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    .line 102
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Ping request bailing out: no n/w, stop reachable"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    if-eqz v3, :cond_0

    .line 155
    :try_start_1
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 156
    .local v0, "body":Lokhttp3/ResponseBody;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 164
    .end local v0    # "body":Lokhttp3/ResponseBody;
    :cond_0
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v4

    .line 161
    .local v4, "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 106
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$404(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    .line 107
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ping request start:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " reachable:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 108
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    iget-object v5, v5, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->mHttpManager:Lcom/navdy/service/library/network/http/IHttpManager;

    invoke-interface {v5}, Lcom/navdy/service/library/network/http/IHttpManager;->getClientCopy()Lokhttp3/OkHttpClient$Builder;

    move-result-object v5

    const-wide/16 v6, 0x7530

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v1

    .line 109
    .local v1, "clientCopy":Lokhttp3/OkHttpClient;
    new-instance v5, Lokhttp3/Request$Builder;

    invoke-direct {v5}, Lokhttp3/Request$Builder;-><init>()V

    invoke-virtual {v5}, Lokhttp3/Request$Builder;->head()Lokhttp3/Request$Builder;

    move-result-object v5

    const-string v6, "http://www.google.com"

    invoke-virtual {v5, v6}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lokhttp3/Request$Builder;->get()Lokhttp3/Request$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v2

    .line 110
    .local v2, "request":Lokhttp3/Request;
    invoke-virtual {v1, v2}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v5

    invoke-interface {v5}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v3

    .line 111
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ping response succeeded :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 113
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 114
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$502(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;Z)Z

    .line 115
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "n/w is reachable now"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 116
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    .line 117
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v5

    new-instance v6, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z

    move-result v7

    invoke-direct {v6, v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;-><init>(Z)V

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 122
    :goto_1
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$402(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;I)I

    .line 123
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "run reachable check in 120000"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 124
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 125
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    const-wide/32 v8, 0x1d4c0

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    if-eqz v3, :cond_0

    .line 155
    :try_start_3
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 156
    .restart local v0    # "body":Lokhttp3/ResponseBody;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 160
    .end local v0    # "body":Lokhttp3/ResponseBody;
    :catch_1
    move-exception v4

    .line 161
    .restart local v4    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 119
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_2
    :try_start_4
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "n/w still reachable"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 120
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 126
    .end local v1    # "clientCopy":Lokhttp3/OkHttpClient;
    .end local v2    # "request":Lokhttp3/Request;
    :catch_2
    move-exception v4

    .line 127
    .restart local v4    # "t":Ljava/lang/Throwable;
    :try_start_5
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Ping response failed:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v5

    if-nez v5, :cond_3

    .line 130
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "run reachable check in 2000"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 131
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 132
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    const-wide/16 v8, 0x7d0

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 154
    if-eqz v3, :cond_0

    .line 155
    :try_start_6
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 156
    .restart local v0    # "body":Lokhttp3/ResponseBody;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0

    .line 160
    .end local v0    # "body":Lokhttp3/ResponseBody;
    :catch_3
    move-exception v4

    .line 161
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 136
    :cond_3
    :try_start_7
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 137
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 138
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reacheable test failed send event: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$502(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;Z)Z

    .line 140
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V

    .line 141
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v5

    new-instance v6, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$500(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Z

    move-result v7

    invoke-direct {v6, v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$ReachabilityEvent;-><init>(Z)V

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/framework/util/BusProvider;->post(Ljava/lang/Object;)V

    .line 149
    :goto_2
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "run reachable check in 30000"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 150
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 151
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$700(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v6}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$600(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)Ljava/lang/Runnable;

    move-result-object v6

    const-wide/16 v8, 0x7530

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 154
    if-eqz v3, :cond_0

    .line 155
    :try_start_8
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 156
    .restart local v0    # "body":Lokhttp3/ResponseBody;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_0

    .line 160
    .end local v0    # "body":Lokhttp3/ResponseBody;
    :catch_4
    move-exception v4

    .line 161
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 143
    :cond_4
    :try_start_9
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "n/w still not reachable"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 144
    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v5}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$300(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2

    .line 153
    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    .line 154
    if-eqz v3, :cond_5

    .line 155
    :try_start_a
    invoke-virtual {v3}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    .line 156
    .restart local v0    # "body":Lokhttp3/ResponseBody;
    if-eqz v0, :cond_5

    .line 157
    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_5

    .line 162
    .end local v0    # "body":Lokhttp3/ResponseBody;
    :cond_5
    :goto_3
    throw v5

    .line 147
    .restart local v4    # "t":Ljava/lang/Throwable;
    :cond_6
    :try_start_b
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reacheable test failed count: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$400(Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_2

    .line 160
    .end local v4    # "t":Ljava/lang/Throwable;
    :catch_5
    move-exception v4

    .line 161
    .restart local v4    # "t":Ljava/lang/Throwable;
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/NetworkStatusManager;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_3
.end method
