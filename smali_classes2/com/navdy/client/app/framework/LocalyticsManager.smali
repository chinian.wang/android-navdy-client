.class public Lcom/navdy/client/app/framework/LocalyticsManager;
.super Ljava/lang/Object;
.source "LocalyticsManager.java"


# static fields
.field private static volatile instance:Lcom/navdy/client/app/framework/LocalyticsManager;


# instance fields
.field private localyticsIsInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private localyticsPendingTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/LocalyticsManager;->instance:Lcom/navdy/client/app/framework/LocalyticsManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/LocalyticsManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 19
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsIsInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    .line 25
    return-void
.end method

.method private declared-synchronized addToLocalyticsPendingTasks(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Added 1 new task to the list of tasks waiting for Localytics initialization. The queue now has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    .line 55
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pending tasks."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/LocalyticsManager;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/navdy/client/app/framework/LocalyticsManager;->instance:Lcom/navdy/client/app/framework/LocalyticsManager;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/LocalyticsManager;->instance:Lcom/navdy/client/app/framework/LocalyticsManager;

    .line 31
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/LocalyticsManager;->instance:Lcom/navdy/client/app/framework/LocalyticsManager;

    return-object v0
.end method

.method public static runWhenReady(Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 59
    invoke-static {}, Lcom/navdy/client/app/framework/LocalyticsManager;->getInstance()Lcom/navdy/client/app/framework/LocalyticsManager;

    move-result-object v0

    .line 61
    .local v0, "lm":Lcom/navdy/client/app/framework/LocalyticsManager;
    iget-object v1, v0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsIsInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/LocalyticsManager;->addToLocalyticsPendingTasks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static setCustomerEmail(Ljava/lang/String;)V
    .locals 1
    .param p0, "email"    # Ljava/lang/String;

    .prologue
    .line 109
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$5;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/LocalyticsManager$5;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 115
    return-void
.end method

.method public static setCustomerFullName(Ljava/lang/String;)V
    .locals 1
    .param p0, "fullName"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$6;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/LocalyticsManager$6;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 125
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;J)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # J

    .prologue
    .line 138
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$8;

    invoke-direct {v0, p0, p1, p2}, Lcom/navdy/client/app/framework/LocalyticsManager$8;-><init>(Ljava/lang/String;J)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 144
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 129
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$7;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$7;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 135
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/util/Date;

    .prologue
    .line 147
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$9;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$9;-><init>(Ljava/lang/String;Ljava/util/Date;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 153
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;[J)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # [J

    .prologue
    .line 165
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$11;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$11;-><init>(Ljava/lang/String;[J)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 171
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # [Ljava/lang/String;

    .prologue
    .line 156
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$10;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$10;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 162
    return-void
.end method

.method public static setProfileAttribute(Ljava/lang/String;[Ljava/util/Date;)V
    .locals 1
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # [Ljava/util/Date;

    .prologue
    .line 174
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$12;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$12;-><init>(Ljava/lang/String;[Ljava/util/Date;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 180
    return-void
.end method

.method public static tagEvent(Ljava/lang/String;)V
    .locals 1
    .param p0, "eventName"    # Ljava/lang/String;

    .prologue
    .line 81
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$2;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/LocalyticsManager$2;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 87
    return-void
.end method

.method public static tagEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p0, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$3;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/LocalyticsManager$3;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 96
    return-void
.end method

.method public static tagEvent(Ljava/lang/String;Ljava/util/Map;J)V
    .locals 2
    .param p0, "eventName"    # Ljava/lang/String;
    .param p2, "customerValueIncrease"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/navdy/client/app/framework/LocalyticsManager$4;-><init>(Ljava/lang/String;Ljava/util/Map;J)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 105
    return-void
.end method

.method public static tagScreen(Ljava/lang/String;)V
    .locals 1
    .param p0, "screen"    # Ljava/lang/String;

    .prologue
    .line 71
    new-instance v0, Lcom/navdy/client/app/framework/LocalyticsManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/LocalyticsManager$1;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/LocalyticsManager;->runWhenReady(Ljava/lang/Runnable;)V

    .line 77
    return-void
.end method


# virtual methods
.method public declared-synchronized initLocalytics()V
    .locals 4

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "initializing Localytics"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 36
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/localytics/android/Localytics;->integrate(Landroid/content/Context;)V

    .line 37
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080540

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/localytics/android/Localytics;->registerPush(Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsIsInitialized:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 41
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Localytics is initialized. Now running "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    .line 43
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pending tasks."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 47
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->localyticsPendingTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 48
    .local v0, "r":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 35
    .end local v0    # "r":Ljava/lang/Runnable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 45
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/LocalyticsManager;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Localytics is initialized. There are no pending tasks."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 50
    :cond_1
    monitor-exit p0

    return-void
.end method
