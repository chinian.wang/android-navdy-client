.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->fetchMetaData(Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

.field final synthetic val$hereAddress:Lcom/here/android/mpa/search/Address;

.field final synthetic val$navLat:Ljava/lang/Double;

.field final synthetic val$navLong:Ljava/lang/Double;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    .prologue
    .line 621
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$navLat:Ljava/lang/Double;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$navLong:Ljava/lang/Double;

    iput-object p4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$hereAddress:Lcom/here/android/mpa/search/Address;

    iput-object p5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 7
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const-wide/16 v2, 0x0

    .line 624
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Error occured while trying to retrieve more info about a HERE places search result. "

    invoke-virtual {v0, v1, p1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 625
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$navLat:Ljava/lang/Double;

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$navLong:Ljava/lang/Double;

    iget-object v5, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$hereAddress:Lcom/here/android/mpa/search/Address;

    iget-object v6, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6$2;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-static/range {v0 .. v6}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;->access$1000(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 626
    return-void
.end method
