.class public final Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
.super Ljava/lang/Object;
.source "GooglePlacesSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;,
        Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;,
        Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;,
        Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;,
        Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    }
.end annotation


# static fields
.field public static final DEFAULT_RADIUS_VALUE:I = 0x649

.field private static final DESTINATION_PLACE_ID_SYNTAX:Ljava/lang/String; = "destination=place_id:"

.field private static final DESTINATION_SYNTAX:Ljava/lang/String; = "destination="

.field private static final INPUT_SYNTAX:Ljava/lang/String; = "input="

.field private static final KEYWORD_SYNTAX:Ljava/lang/String; = "keyword="

.field private static final KEY_SYNTAX:Ljava/lang/String; = "key="

.field private static final LOCATION_SYNTAX:Ljava/lang/String; = "location="

.field private static final OPENNOW_SYNTAX:Ljava/lang/String; = "opennow="

.field private static final OPENNOW_VALUE:Ljava/lang/String; = "true"

.field private static final ORIGIN_SYNTAX:Ljava/lang/String; = "origin="

.field private static final PLACE_ID_SYNTAX:Ljava/lang/String; = "placeid="

.field private static final QUERY_SYNTAX:Ljava/lang/String; = "query="

.field private static final RADIUS_SYNTAX:Ljava/lang/String; = "radius="

.field private static final RANKBY_SYNTAX:Ljava/lang/String; = "rankby="

.field private static final SERVICE_RADIUS_VALUE:I = 0x1f6e

.field private static final TYPE_SYNTAX:Ljava/lang/String; = "type="

.field private static final VERBOSE:Z

.field public static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private apiKEY:Ljava/lang/String;

.field private currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

.field private destination:Lcom/navdy/client/app/framework/models/Destination;

.field private googleSearchListener:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

.field private handler:Landroid/os/Handler;

.field private location_value:Ljava/lang/String;

.field private onSuccess:Ljava/lang/Runnable;

.field private placeId:Ljava/lang/String;

.field private results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->apiKEY:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 76
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$1;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->onSuccess:Ljava/lang/Runnable;

    .line 159
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->init(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V

    .line 160
    return-void
.end method

.method public constructor <init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;Landroid/os/Handler;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->apiKEY:Ljava/lang/String;

    .line 74
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 76
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$1;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->onSuccess:Ljava/lang/Runnable;

    .line 154
    iput-object p2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->handler:Landroid/os/Handler;

    .line 155
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->init(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->results:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->results:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->sendDestinationsToListener(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V

    return-void
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->onSuccess:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .param p3, "x3"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApi(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    return-void
.end method

.method private getSearchResults(Ljava/lang/String;)Ljava/util/List;
    .locals 20
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 401
    const/4 v6, 0x0

    .line 402
    .local v6, "inputStream":Ljava/io/InputStream;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 405
    .local v12, "resultDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :try_start_0
    new-instance v16, Ljava/net/URL;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 406
    .local v16, "urlObject":Ljava/net/URL;
    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v15

    check-cast v15, Ljava/net/HttpURLConnection;

    .line 407
    .local v15, "urlConnection":Ljava/net/HttpURLConnection;
    invoke-virtual {v15}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 408
    const-string v17, "UTF-8"

    move-object/from16 v0, v17

    invoke-static {v6, v0}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 409
    .local v2, "JSONString":Ljava/lang/String;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 411
    .local v7, "jsonObject":Lorg/json/JSONObject;
    sget-object v17, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "jsonObject received: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v17, v0

    sget-object v18, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 413
    invoke-static {v7}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->createDestinationObject(Lorg/json/JSONObject;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    move-result-object v3

    .line 414
    .local v3, "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v3, :cond_0

    .line 415
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    .end local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    :cond_0
    :goto_0
    sget-object v17, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "resultDestinations size after parsing response: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 452
    .end local v2    # "JSONString":Ljava/lang/String;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v12    # "resultDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    .end local v15    # "urlConnection":Ljava/net/HttpURLConnection;
    .end local v16    # "urlObject":Ljava/net/URL;
    :goto_1
    return-object v12

    .line 417
    .restart local v2    # "JSONString":Ljava/lang/String;
    .restart local v7    # "jsonObject":Lorg/json/JSONObject;
    .restart local v12    # "resultDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    .restart local v15    # "urlConnection":Ljava/net/HttpURLConnection;
    .restart local v16    # "urlObject":Ljava/net/URL;
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v17, v0

    sget-object v18, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 418
    const-string v17, "predictions"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/JSONArray;

    .line 419
    .local v9, "predictions":Lorg/json/JSONArray;
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v10

    .line 420
    .local v10, "predictionsLength":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v10, :cond_0

    .line 421
    invoke-virtual {v9, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 422
    .local v8, "prediction":Lorg/json/JSONObject;
    invoke-static {v8}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->createDestinationObject(Lorg/json/JSONObject;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    move-result-object v3

    .line 423
    .restart local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v3, :cond_2

    .line 424
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 427
    .end local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v5    # "i":I
    .end local v8    # "prediction":Lorg/json/JSONObject;
    .end local v9    # "predictions":Lorg/json/JSONArray;
    .end local v10    # "predictionsLength":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v17, v0

    sget-object v18, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_5

    .line 428
    const-string v17, "results"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lorg/json/JSONArray;

    .line 429
    .local v13, "results":Lorg/json/JSONArray;
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v14

    .line 430
    .local v14, "resultsLength":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_3
    if-ge v5, v14, :cond_0

    .line 431
    invoke-virtual {v13, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 432
    .local v11, "result":Lorg/json/JSONObject;
    invoke-static {v11}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->createDestinationObject(Lorg/json/JSONObject;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    move-result-object v3

    .line 433
    .restart local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v3, :cond_4

    .line 434
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 439
    .end local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v5    # "i":I
    .end local v11    # "result":Lorg/json/JSONObject;
    .end local v13    # "results":Lorg/json/JSONArray;
    .end local v14    # "resultsLength":I
    :cond_5
    const-string v17, "result"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/json/JSONObject;

    .line 440
    .restart local v11    # "result":Lorg/json/JSONObject;
    invoke-static {v11}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->createDestinationObject(Lorg/json/JSONObject;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    move-result-object v3

    .line 441
    .restart local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    if-eqz v3, :cond_0

    .line 442
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 446
    .end local v2    # "JSONString":Ljava/lang/String;
    .end local v3    # "destination":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v11    # "result":Lorg/json/JSONObject;
    .end local v15    # "urlConnection":Ljava/net/HttpURLConnection;
    .end local v16    # "urlObject":Ljava/net/URL;
    :catch_0
    move-exception v4

    .line 447
    .local v4, "e":Ljava/lang/Throwable;
    :try_start_2
    sget-object v17, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "throwable e: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 448
    const/4 v12, 0x0

    .line 450
    .end local v12    # "resultDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .end local v4    # "e":Ljava/lang/Throwable;
    .restart local v12    # "resultDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    :catchall_0
    move-exception v17

    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v17
.end method

.method private init(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    .prologue
    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080557

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->apiKEY:Ljava/lang/String;

    .line 168
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->googleSearchListener:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    .line 169
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getSmartStartCoordinates()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 171
    .local v0, "location":Lcom/navdy/service/library/events/location/Coordinate;
    if-eqz v0, :cond_0

    .line 172
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    .line 174
    :cond_0
    return-void
.end method

.method private runSearchWebApi(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
    .locals 9
    .param p1, "searchText"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "searchRadius"    # I

    .prologue
    const/16 v8, 0x649

    .line 267
    const-string v2, ""

    .line 269
    .local v2, "typeString":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 270
    invoke-virtual {p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    .line 273
    :cond_0
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Running search with following parameters: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 275
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 277
    .local v1, "encodedSearchText":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 278
    .local v3, "url":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-static {v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->apiKEY:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$Query:[I

    iget-object v6, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 377
    :cond_1
    :goto_0
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 378
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const-string v5, "type="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 384
    .local v4, "urlString":Ljava/lang/String;
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calling Google with URL: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->apiKEY:Ljava/lang/String;

    const-string v8, "key=API_KEY"

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 386
    invoke-direct {p0, v4}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->getSearchResults(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->results:Ljava/util/List;

    .line 388
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->handler:Landroid/os/Handler;

    if-eqz v5, :cond_c

    .line 389
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->handler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->onSuccess:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 393
    :goto_1
    return-void

    .line 283
    .end local v4    # "urlString":Ljava/lang/String;
    :pswitch_0
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 284
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    const-string v5, "location="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 288
    :cond_3
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    const-string v5, "radius="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 291
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 292
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 293
    const-string v5, "input="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 298
    :pswitch_1
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    const-string v5, "placeid="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 303
    :pswitch_2
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 304
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    const-string v5, "location="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_4
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    const-string v5, "rankby="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    if-eqz p2, :cond_6

    .line 312
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$3;->$SwitchMap$com$navdy$client$app$framework$search$GooglePlacesSearch$ServiceTypes:[I

    invoke-virtual {p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 318
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->PROMINENCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-static {v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string v5, "radius="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    if-lez p3, :cond_5

    .line 322
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 334
    :goto_2
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    const-string v5, "opennow="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    const-string v5, "true"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 315
    :pswitch_3
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-static {v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 324
    :cond_5
    const/16 v5, 0x1f6e

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 329
    :cond_6
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->DISTANCE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;

    invoke-static {v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;->getBaseUrl(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$RankByTypes;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 331
    const-string v5, "keyword="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 339
    :pswitch_4
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapUtils;->parseLatLng(Ljava/lang/String;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    if-nez v5, :cond_8

    .line 340
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 341
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string v5, "location="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    :cond_7
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    const-string v5, "radius="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 349
    :cond_8
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 350
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    const-string v5, "query="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 356
    :pswitch_5
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 357
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    const-string v5, "origin="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 361
    :cond_9
    const-string v5, "&"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 363
    const-string v5, "destination=place_id:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 364
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 365
    :cond_a
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v5, v5, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .line 366
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 367
    const-string v5, "destination="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v5, v5, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 369
    .local v0, "address":Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 371
    .end local v0    # "address":Ljava/lang/String;
    :cond_b
    sget-object v5, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Trying to call Google Directions API with a no placeId or address."

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 391
    .restart local v4    # "urlString":Ljava/lang/String;
    :cond_c
    iget-object v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->onSuccess:Ljava/lang/Runnable;

    invoke-interface {v5}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_1

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_2
    .end packed-switch

    .line 312
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private runSearchWebApiInTaskManager(Ljava/lang/String;)V
    .locals 2
    .param p1, "searchText"    # Ljava/lang/String;

    .prologue
    .line 231
    const/4 v0, 0x0

    const/16 v1, 0x649

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    .line 232
    return-void
.end method

.method private runSearchWebApiInTaskManager(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
    .locals 3
    .param p1, "searchText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "type"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "searchRadius"    # I

    .prologue
    .line 235
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x0

    sget-object v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->sendDestinationsToListener(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V

    .line 239
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 264
    return-void
.end method

.method private sendDestinationsToListener(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 2
    .param p2, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 396
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->googleSearchListener:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    iget-object v1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    invoke-interface {v0, p1, v1, p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;->onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V

    .line 397
    return-void
.end method


# virtual methods
.method public runDetailsSearchWebApi(Ljava/lang/String;)V
    .locals 1
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    .line 219
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 220
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;)V

    .line 221
    return-void
.end method

.method public runDirectionsSearchWebApi(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "d"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 225
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->placeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->placeId:Ljava/lang/String;

    .line 226
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DIRECTIONS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 227
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;)V

    .line 228
    return-void
.end method

.method public runNearbySearchWebApi(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 211
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "executing Nearby Search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 212
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 214
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;)V

    .line 215
    return-void
.end method

.method public runQueryAutoCompleteWebApi(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 177
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "executing Query Auto Complete"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 178
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->QUERY_AUTOCOMPLETE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 180
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public runServiceSearchWebApi(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;)V
    .locals 1
    .param p1, "type"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    .prologue
    .line 191
    const/16 v0, 0x1f6e

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runServiceSearchWebApi(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    .line 192
    return-void
.end method

.method public runServiceSearchWebApi(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;
    .param p2, "searchRadius"    # I

    .prologue
    .line 195
    if-nez p1, :cond_0

    .line 196
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Trying to call runServiceSearchWebApi with a null service type. You crazy?"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 199
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "executing Service Search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->location_value:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 202
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->NEARBY:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 207
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    goto :goto_0

    .line 204
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    goto :goto_1
.end method

.method public runTextSearchWebApi(Ljava/lang/String;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 184
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "executing Text Search"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 185
    sget-object v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->TEXTSEARCH:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    iput-object v0, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->currentQueryType:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    .line 187
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method public setListener(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    .prologue
    .line 456
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->googleSearchListener:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;

    .line 457
    return-void
.end method
