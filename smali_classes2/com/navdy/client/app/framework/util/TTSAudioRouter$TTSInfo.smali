.class Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;
.super Ljava/lang/Object;
.source "TTSAudioRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/util/TTSAudioRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TTSInfo"
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field originalId:Ljava/lang/String;

.field sendNotification:Z

.field subTextCount:I

.field public text:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "originalId"    # Ljava/lang/String;
    .param p4, "sendNotification"    # Z

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->subTextCount:I

    .line 147
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->text:Ljava/lang/String;

    .line 148
    iput-object p2, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->id:Ljava/lang/String;

    .line 149
    iput-object p3, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->originalId:Ljava/lang/String;

    .line 150
    iput-boolean p4, p0, Lcom/navdy/client/app/framework/util/TTSAudioRouter$TTSInfo;->sendNotification:Z

    .line 151
    return-void
.end method
