.class public Lcom/navdy/client/app/framework/util/MusicDbUtils;
.super Ljava/lang/Object;
.source "MusicDbUtils.java"


# static fields
.field private static final CHARACTER_MAP_SELECTION:Ljava/lang/String; = "1=1) GROUP BY (letter"

.field public static final CHARACTER_MAP_SORT:Ljava/lang/String; = "CASE\n  WHEN letter GLOB \'[A-Z]\' THEN 0\n  WHEN letter GLOB \'[0-9]\' THEN 1\n  ELSE 2\nEND ASC"

.field private static final COUNT:Ljava/lang/String; = "count(*) AS count"

.field public static final COUNT_COLUMN:Ljava/lang/String; = "count"

.field private static final FIRST_LETTER_FORMAT:Ljava/lang/String; = "SUBSTR(UPPER(%s), 1, 1) AS letter"

.field private static final GPM_PLAYLISTS_BASE_URI:Ljava/lang/String; = "content://com.google.android.music.MusicContent/playlists"

.field public static final GPM_PLAYLISTS_COLUMN_PLAYLIST_NAME:Ljava/lang/String; = "playlist_name"

.field private static final GPM_PLAYLISTS_MEMBERS_PATH:Ljava/lang/String; = "members"

.field public static final GPM_PLAYLIST_MEMBERS_COLUMN_ALBUM:Ljava/lang/String; = "album"

.field public static final GPM_PLAYLIST_MEMBERS_COLUMN_ARTIST:Ljava/lang/String; = "artist"

.field public static final GPM_PLAYLIST_MEMBERS_COLUMN_SOURCE_ID:Ljava/lang/String; = "SourceId"

.field public static final GPM_PLAYLIST_MEMBERS_COLUMN_TITLE:Ljava/lang/String; = "title"

.field public static final LETTER_COLUMN:Ljava/lang/String; = "letter"

.field public static final ORDER_FORMAT:Ljava/lang/String; = "CASE%n  WHEN UPPER(%1$s) GLOB \'[A-Z]*\' THEN 0%n  WHEN UPPER(%1$s) GLOB \'[0-9]*\' THEN 1%n  ELSE 2%nEND ASC, UPPER(%1$s);"

.field private static logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/MusicDbUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/MusicDbUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAlbumMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0, "albumId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 83
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 84
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 85
    .local v1, "membersUri":Landroid/net/Uri;
    const/4 v5, 0x4

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "title"

    aput-object v5, v2, v7

    const-string v5, "artist"

    aput-object v5, v2, v8

    const/4 v5, 0x2

    const-string v6, "album"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "_id"

    aput-object v6, v2, v5

    .line 86
    .local v2, "membersColumns":[Ljava/lang/String;
    const-string v3, "album_id = ?"

    .line 87
    .local v3, "query":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    aput-object p0, v4, v7

    .line 88
    .local v4, "args":[Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    return-object v5
.end method

.method public static getAlbumsCharacterMapCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 184
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 185
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 186
    .local v1, "albumsUri":Landroid/net/Uri;
    const-string v3, "album"

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getCharacterMapProjection(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 187
    .local v2, "albumsColumns":[Ljava/lang/String;
    const-string v3, "1=1) GROUP BY (letter"

    const/4 v4, 0x0

    const-string v5, "CASE\n  WHEN letter GLOB \'[A-Z]\' THEN 0\n  WHEN letter GLOB \'[0-9]\' THEN 1\n  ELSE 2\nEND ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getAlbumsCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 76
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 77
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 78
    .local v1, "albumsUri":Landroid/net/Uri;
    const/4 v4, 0x4

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "album"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "numsongs"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "artist"

    aput-object v5, v2, v4

    .line 79
    .local v2, "albumsColumns":[Ljava/lang/String;
    const-string v4, "album"

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getOrderString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getArtistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p0, "artistId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 99
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 100
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 101
    .local v1, "membersUri":Landroid/net/Uri;
    const/4 v5, 0x4

    new-array v2, v5, [Ljava/lang/String;

    const-string v5, "title"

    aput-object v5, v2, v7

    const-string v5, "artist"

    aput-object v5, v2, v8

    const/4 v5, 0x2

    const-string v6, "album"

    aput-object v6, v2, v5

    const/4 v5, 0x3

    const-string v6, "_id"

    aput-object v6, v2, v5

    .line 102
    .local v2, "membersColumns":[Ljava/lang/String;
    const-string v3, "artist_id = ?"

    .line 103
    .local v3, "query":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    aput-object p0, v4, v7

    .line 104
    .local v4, "args":[Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    return-object v5
.end method

.method public static getArtistSongsCount(Ljava/lang/String;)I
    .locals 9
    .param p0, "artistId"    # Ljava/lang/String;

    .prologue
    .line 108
    const/4 v7, 0x0

    .line 109
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, -0x1

    .line 111
    .local v6, "count":I
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 112
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 113
    .local v1, "membersUri":Landroid/net/Uri;
    const/4 v5, 0x1

    new-array v2, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "count(*) AS count"

    aput-object v8, v2, v5

    .line 114
    .local v2, "membersColumns":[Ljava/lang/String;
    const-string v3, "artist_id = ?"

    .line 115
    .local v3, "query":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    .line 116
    .local v4, "args":[Ljava/lang/String;
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 117
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 118
    const-string v5, "count"

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v7, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 120
    :cond_0
    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    .line 122
    return v6

    .line 120
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "membersUri":Landroid/net/Uri;
    .end local v2    # "membersColumns":[Ljava/lang/String;
    .end local v3    # "query":Ljava/lang/String;
    .end local v4    # "args":[Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-static {v7}, Lcom/navdy/service/library/util/IOUtils;->closeObject(Ljava/io/Closeable;)V

    throw v5
.end method

.method public static getArtistsAlbums(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 20
    .param p0, "artistId"    # Ljava/lang/String;

    .prologue
    .line 132
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 133
    .local v1, "contentResolver":Landroid/content/ContentResolver;
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 134
    .local v2, "membersUri":Landroid/net/Uri;
    const/4 v6, 0x1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "DISTINCT album_id"

    aput-object v9, v3, v6

    .line 135
    .local v3, "membersColumns":[Ljava/lang/String;
    const-string v4, "artist_id = ?"

    .line 136
    .local v4, "query":Ljava/lang/String;
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    .line 137
    .local v5, "args":[Ljava/lang/String;
    const/16 v19, 0x0

    .line 138
    .local v19, "tracksCursor":Landroid/database/Cursor;
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 141
    .local v14, "albumIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 144
    if-eqz v19, :cond_1

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 146
    :cond_0
    const-string v6, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 147
    .local v12, "albumId":I
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v14, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 148
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_0

    .line 151
    .end local v12    # "albumId":I
    :cond_1
    if-eqz v19, :cond_2

    .line 152
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_2
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 157
    .local v13, "albumIdIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .local v18, "queryBuilder":Ljava/lang/StringBuilder;
    const-string v6, "_id IN ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v15

    .line 160
    .local v15, "count":I
    const/16 v16, 0x0

    .line 161
    .local v16, "i":I
    new-array v5, v15, [Ljava/lang/String;

    .line 162
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 163
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "i":I
    .local v17, "i":I
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v16

    .line 164
    add-int/lit8 v6, v15, -0x1

    move/from16 v0, v17

    if-gt v0, v6, :cond_4

    .line 165
    const-string v6, "?,"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v16, v17

    .end local v17    # "i":I
    .restart local v16    # "i":I
    goto :goto_0

    .line 151
    .end local v13    # "albumIdIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v15    # "count":I
    .end local v16    # "i":I
    .end local v18    # "queryBuilder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v6

    if-eqz v19, :cond_3

    .line 152
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v6

    .line 166
    .restart local v13    # "albumIdIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v15    # "count":I
    .restart local v17    # "i":I
    .restart local v18    # "queryBuilder":Ljava/lang/StringBuilder;
    :cond_4
    move/from16 v0, v17

    if-ne v0, v15, :cond_6

    .line 167
    const-string v6, "?)"

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move/from16 v16, v17

    .end local v17    # "i":I
    .restart local v16    # "i":I
    goto :goto_0

    .line 170
    :cond_5
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 171
    sget-object v7, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 172
    .local v7, "albumsUri":Landroid/net/Uri;
    const/4 v6, 0x4

    new-array v8, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v9, "_id"

    aput-object v9, v8, v6

    const/4 v6, 0x1

    const-string v9, "album"

    aput-object v9, v8, v6

    const/4 v6, 0x2

    const-string v9, "numsongs"

    aput-object v9, v8, v6

    const/4 v6, 0x3

    const-string v9, "artist"

    aput-object v9, v8, v6

    .line 173
    .local v8, "albumsColumns":[Ljava/lang/String;
    const-string v6, "album"

    invoke-static {v6}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getOrderString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    move-object v6, v1

    move-object v9, v4

    move-object v10, v5

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6

    .end local v7    # "albumsUri":Landroid/net/Uri;
    .end local v8    # "albumsColumns":[Ljava/lang/String;
    .end local v16    # "i":I
    .restart local v17    # "i":I
    :cond_6
    move/from16 v16, v17

    .end local v17    # "i":I
    .restart local v16    # "i":I
    goto :goto_0
.end method

.method public static getArtistsCharacterMapCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 191
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 192
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 193
    .local v1, "artistsUri":Landroid/net/Uri;
    const-string v3, "artist"

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getCharacterMapProjection(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 194
    .local v2, "artistsColumns":[Ljava/lang/String;
    const-string v3, "1=1) GROUP BY (letter"

    const/4 v4, 0x0

    const-string v5, "CASE\n  WHEN letter GLOB \'[A-Z]\' THEN 0\n  WHEN letter GLOB \'[0-9]\' THEN 1\n  ELSE 2\nEND ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getArtistsCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 92
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 93
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 94
    .local v1, "artistsUri":Landroid/net/Uri;
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "artist"

    aput-object v5, v2, v4

    .line 95
    .local v2, "artistsColumns":[Ljava/lang/String;
    const-string v4, "artist"

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getOrderString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getCharacterMapProjection(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p0, "nameColumn"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 255
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "count(*) AS count"

    aput-object v1, v0, v3

    const-string v1, "SUBSTR(UPPER(%s), 1, 1) AS letter"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    return-object v0
.end method

.method public static getGpmPlaylistCursor(I)Landroid/database/Cursor;
    .locals 8
    .param p0, "playlistId"    # I

    .prologue
    .line 227
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 228
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "playlist_name"

    aput-object v3, v2, v1

    .line 229
    .local v2, "playlistColumns":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 231
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistUri(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 235
    :goto_0
    return-object v6

    .line 232
    :catch_0
    move-exception v7

    .line 233
    .local v7, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicDbUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get GPM playlist cursor for playlist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getGpmPlaylistMembersCursor(I)Landroid/database/Cursor;
    .locals 8
    .param p0, "playlistId"    # I

    .prologue
    .line 239
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 240
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "title"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "artist"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "album"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "SourceId"

    aput-object v3, v2, v1

    .line 241
    .local v2, "membersColumns":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 243
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistMembersUri(I)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 247
    :goto_0
    return-object v6

    .line 244
    :catch_0
    move-exception v7

    .line 245
    .local v7, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicDbUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get GPM playlist members cursor for playlist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getGpmPlaylistMembersUri(I)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # I

    .prologue
    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.google.android.music.MusicContent/playlists/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "members"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getGpmPlaylistUri(I)Landroid/net/Uri;
    .locals 2
    .param p0, "playlistId"    # I

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.google.android.music.MusicContent/playlists/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getGpmPlaylistsCursor()Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 215
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 216
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "playlist_name"

    aput-object v3, v2, v1

    .line 217
    .local v2, "playlistsColumns":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 219
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getGpmPlaylistsUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "playlist_name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 223
    :goto_0
    return-object v6

    .line 220
    :catch_0
    move-exception v7

    .line 221
    .local v7, "t":Ljava/lang/Throwable;
    sget-object v1, Lcom/navdy/client/app/framework/util/MusicDbUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Couldn\'t get GPM playlists cursor: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getGpmPlaylistsUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 203
    const-string v0, "content://com.google.android.music.MusicContent/playlists"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getOrderString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "nameColumn"    # Ljava/lang/String;

    .prologue
    .line 251
    const-string v0, "CASE%n  WHEN UPPER(%1$s) GLOB \'[A-Z]*\' THEN 0%n  WHEN UPPER(%1$s) GLOB \'[0-9]*\' THEN 1%n  ELSE 2%nEND ASC, UPPER(%1$s);"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaylistMembersCursor(I)Landroid/database/Cursor;
    .locals 1
    .param p0, "playlistId"    # I

    .prologue
    .line 72
    invoke-static {p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getPlaylistMembersCursor(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getPlaylistMembersCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p0, "playlistIdStr"    # Ljava/lang/String;

    .prologue
    .line 63
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 68
    .local v1, "playlistId":I
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getPlaylistMembersCursor(I)Landroid/database/Cursor;

    move-result-object v2

    .end local v1    # "playlistId":I
    :goto_0
    return-object v2

    .line 64
    :catch_0
    move-exception v0

    .line 65
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/navdy/client/app/framework/util/MusicDbUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid collection ID: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 66
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getPlaylistsCharacterMapCursor()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 177
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 178
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/navdy/client/app/providers/NavdyContentProviderConstants;->PLAYLISTS_CONTENT_URI:Landroid/net/Uri;

    .line 179
    .local v1, "playlistsUri":Landroid/net/Uri;
    const-string v3, "playlist_name"

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/MusicDbUtils;->getCharacterMapProjection(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 180
    .local v2, "playlistsColumns":[Ljava/lang/String;
    const-string v3, "1=1) GROUP BY (letter"

    const/4 v4, 0x0

    const-string v5, "CASE\n  WHEN letter GLOB \'[A-Z]\' THEN 0\n  WHEN letter GLOB \'[0-9]\' THEN 1\n  ELSE 2\nEND ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method

.method public static getPlaylistsCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getPlaylistsCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
