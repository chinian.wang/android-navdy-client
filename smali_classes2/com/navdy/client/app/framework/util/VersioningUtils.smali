.class public Lcom/navdy/client/app/framework/util/VersioningUtils;
.super Ljava/lang/Object;
.source "VersioningUtils.java"


# static fields
.field private static final SHARED_PREF_FAVORITES:Ljava/lang/String; = "favorites"

.field private static final SHARED_PREF_SUGGESTIONS:Ljava/lang/String; = "suggestions"

.field private static final VERSION:Ljava/lang/String; = "version"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/VersioningUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/VersioningUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)J
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .prologue
    .line 29
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VersioningUtils;->getSharedPrefFor(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 30
    .local v0, "sp":Landroid/content/SharedPreferences;
    const-string v1, "version"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    return-wide v2
.end method

.method private static getSharedPrefFor(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/String;
    .locals 3
    .param p0, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .prologue
    .line 40
    sget-object v1, Lcom/navdy/client/app/framework/util/VersioningUtils$2;->$SwitchMap$com$navdy$client$app$framework$servicehandler$DestinationsServiceHandler$DestinationType:[I

    invoke-virtual {p0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 43
    const-string v0, "suggestions"

    .line 49
    .local v0, "pref":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 46
    .end local v0    # "pref":Ljava/lang/String;
    :pswitch_0
    const-string v0, "favorites"

    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static increaseVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/Long;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 93
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/util/VersioningUtils;->getCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 94
    .local v0, "newVersion":Ljava/lang/Long;
    invoke-static {p0, p1, v0}, Lcom/navdy/client/app/framework/util/VersioningUtils;->setCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;Ljava/lang/Long;)V

    .line 95
    return-object v0
.end method

.method static increaseVersionAndSendTheseSuggestionsToDisplayAsync(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "suggestions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 70
    .local v0, "context":Landroid/content/Context;
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/Long;

    move-result-object v1

    .line 73
    .local v1, "serialNumber":Ljava/lang/Long;
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 74
    sget-object v2, Lcom/navdy/client/app/framework/util/VersioningUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "HUD is not connected so ignoring the request to increaseVersionAndSendTheseSuggestionsToDisplayAsync"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 84
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v2

    new-instance v3, Lcom/navdy/client/app/framework/util/VersioningUtils$1;

    invoke-direct {v3, v1, p0}, Lcom/navdy/client/app/framework/util/VersioningUtils$1;-><init>(Ljava/lang/Long;Ljava/util/List;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static increaseVersionAndSendToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V
    .locals 4
    .param p0, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .prologue
    .line 57
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/util/VersioningUtils;->increaseVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/Long;

    .line 60
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    sget-object v1, Lcom/navdy/client/app/framework/util/VersioningUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HUD is not connected so ignoring the request to increaseVersionAndSendToDisplay (destinationType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-static {p0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendDestinationsToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    goto :goto_0
.end method

.method private static setCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;Ljava/lang/Long;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
    .param p2, "version"    # Ljava/lang/Long;

    .prologue
    .line 34
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/VersioningUtils;->getSharedPrefFor(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 35
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "version"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 36
    return-void
.end method
