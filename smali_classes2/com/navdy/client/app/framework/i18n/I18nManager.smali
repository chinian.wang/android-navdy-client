.class public Lcom/navdy/client/app/framework/i18n/I18nManager;
.super Ljava/lang/Object;
.source "I18nManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;,
        Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;,
        Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;
    }
.end annotation


# static fields
.field private static final IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LIBERIA_COUNTRY:Ljava/lang/String; = "LR"

.field public static final METERS_TO_KILOMETERS:D = 0.001

.field public static final METERS_TO_MILES:D = 6.21371E-4

.field private static final MYANMAR_COUNTRY:Ljava/lang/String; = "MM"

.field private static final instance:Lcom/navdy/client/app/framework/i18n/I18nManager;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final unitSystemLock:Ljava/lang/Object;


# instance fields
.field private currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->logger:Lcom/navdy/service/library/log/Logger;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->unitSystemLock:Ljava/lang/Object;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    .line 51
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    sget-object v1, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    const-string v1, "LR"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    const-string v1, "MM"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    new-instance v0, Lcom/navdy/client/app/framework/i18n/I18nManager;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/i18n/I18nManager;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->instance:Lcom/navdy/client/app/framework/i18n/I18nManager;

    .line 58
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->initAddressFormats()V

    .line 59
    return-void
.end method

.method private constructor <init>()V
    .locals 5

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v1

    .line 72
    .local v1, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    if-nez v1, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->driverCountryIsImperial()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 74
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 79
    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 81
    new-instance v0, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/navdy/client/app/framework/i18n/I18nManager$LocaleReceiver;-><init>(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/client/app/framework/i18n/I18nManager$1;)V

    .line 82
    .local v0, "localeReceiver":Landroid/content/BroadcastReceiver;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->listeners:Ljava/util/List;

    .line 85
    return-void

    .line 76
    .end local v0    # "localeReceiver":Landroid/content/BroadcastReceiver;
    :cond_1
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/i18n/I18nManager;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/i18n/I18nManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/i18n/I18nManager;
    .param p1, "x1"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    return-object p1
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/i18n/I18nManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/i18n/I18nManager;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->driverCountryIsImperial()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->unitSystemLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/i18n/I18nManager;Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/i18n/I18nManager;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/i18n/I18nManager;->callListeners(Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;)V

    return-void
.end method

.method private callListeners(Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;)V
    .locals 4
    .param p1, "callListener"    # Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;

    .prologue
    .line 126
    iget-object v3, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->listeners:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 128
    .local v0, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 129
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 130
    .local v2, "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;

    .line 132
    .local v1, "listener":Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;
    if-eqz v1, :cond_0

    .line 133
    invoke-interface {p1, v1}, Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;->call(Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;)V

    goto :goto_0

    .line 135
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 138
    .end local v1    # "listener":Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;
    .end local v2    # "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;>;"
    :cond_1
    return-void
.end method

.method private driverCountryIsImperial()Z
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->IMPERIAL_UNIT_SYSTEM_LOCALES:Ljava/util/List;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/navdy/client/app/framework/i18n/I18nManager;->instance:Lcom/navdy/client/app/framework/i18n/I18nManager;

    return-object v0
.end method


# virtual methods
.method public addListener(Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;)V
    .locals 3
    .param p1, "listener"    # Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;

    .prologue
    .line 92
    iget-object v1, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->listeners:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v2, Lcom/navdy/client/app/framework/i18n/I18nManager;->unitSystemLock:Ljava/lang/Object;

    monitor-enter v2

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 97
    .local v0, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;->onUnitSystemChanged(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V

    .line 99
    return-void

    .line 97
    .end local v0    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getCurrentLocale()Ljava/util/Locale;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 142
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 143
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 144
    .local v2, "resources":Landroid/content/res/Resources;
    if-nez v2, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-object v3

    .line 147
    :cond_1
    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 148
    .local v0, "configuration":Landroid/content/res/Configuration;
    if-eqz v0, :cond_0

    .line 151
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    if-lt v3, v4, :cond_2

    .line 152
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v3

    goto :goto_0

    .line 154
    :cond_2
    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_0
.end method

.method public getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .locals 2

    .prologue
    .line 106
    sget-object v1, Lcom/navdy/client/app/framework/i18n/I18nManager;->unitSystemLock:Ljava/lang/Object;

    monitor-enter v1

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    monitor-exit v1

    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeListener(Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;)V
    .locals 2
    .param p1, "listener"    # Lcom/navdy/client/app/framework/i18n/I18nManager$Listener;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->listeners:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 103
    return-void
.end method

.method public setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V
    .locals 2
    .param p1, "unitSystem"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .prologue
    .line 112
    sget-object v1, Lcom/navdy/client/app/framework/i18n/I18nManager;->unitSystemLock:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    iput-object p1, p0, Lcom/navdy/client/app/framework/i18n/I18nManager;->currentUnitSystem:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 114
    invoke-static {p1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V

    .line 116
    new-instance v0, Lcom/navdy/client/app/framework/i18n/I18nManager$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/i18n/I18nManager$1;-><init>(Lcom/navdy/client/app/framework/i18n/I18nManager;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/i18n/I18nManager;->callListeners(Lcom/navdy/client/app/framework/i18n/I18nManager$CallListener;)V

    .line 122
    monitor-exit v1

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
