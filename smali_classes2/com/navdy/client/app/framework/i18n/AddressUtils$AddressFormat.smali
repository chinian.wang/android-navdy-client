.class public Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
.super Ljava/lang/Object;
.source "AddressUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/i18n/AddressUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddressFormat"
.end annotation


# instance fields
.field public foreignLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public localLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->localLines:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->foreignLines:Ljava/util/ArrayList;

    return-void
.end method

.method static getClone(Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;)Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    .locals 3
    .param p0, "original"    # Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    .prologue
    .line 63
    if-nez p0, :cond_0

    .line 64
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 66
    :cond_0
    new-instance v0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;-><init>()V

    .line 68
    .local v0, "af":Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->localLines:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->localLines:Ljava/util/ArrayList;

    .line 69
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->foreignLines:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/navdy/client/app/framework/i18n/AddressUtils$AddressFormat;->foreignLines:Ljava/util/ArrayList;

    goto :goto_0
.end method
