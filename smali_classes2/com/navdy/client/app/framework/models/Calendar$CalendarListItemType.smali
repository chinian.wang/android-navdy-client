.class public final enum Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
.super Ljava/lang/Enum;
.source "Calendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Calendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CalendarListItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

.field public static final enum CALENDAR:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

.field public static final enum GLOBAL_SWITCH:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

.field public static final enum TITLE:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    const-string v1, "GLOBAL_SWITCH"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->GLOBAL_SWITCH:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 19
    new-instance v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->TITLE:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 20
    new-instance v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    const-string v1, "CALENDAR"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->CALENDAR:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    sget-object v1, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->GLOBAL_SWITCH:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->TITLE:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->CALENDAR:Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->$VALUES:[Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->value:I

    .line 25
    iput p3, p0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->value:I

    .line 26
    return-void
.end method

.method public static fromValue(I)Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 33
    invoke-static {}, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->values()[Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->$VALUES:[Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/navdy/client/app/framework/models/Calendar$CalendarListItemType;->value:I

    return v0
.end method
