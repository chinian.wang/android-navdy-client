.class public Lcom/navdy/client/app/framework/models/TransparentCircle;
.super Landroid/view/View;
.source "TransparentCircle.java"


# static fields
.field private static final VERBOSE:Z


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private canvas:Landroid/graphics/Canvas;

.field private eraser:Landroid/graphics/Paint;

.field private logger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/TransparentCircle;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->logger:Lcom/navdy/service/library/log/Logger;

    .line 35
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/TransparentCircle;->initPaintObjectAsEraser()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/TransparentCircle;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->logger:Lcom/navdy/service/library/log/Logger;

    .line 44
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/TransparentCircle;->initPaintObjectAsEraser()V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/TransparentCircle;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->logger:Lcom/navdy/service/library/log/Logger;

    .line 52
    invoke-direct {p0}, Lcom/navdy/client/app/framework/models/TransparentCircle;->initPaintObjectAsEraser()V

    .line 53
    return-void
.end method

.method private initPaintObjectAsEraser()V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->eraser:Landroid/graphics/Paint;

    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->eraser:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 61
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->eraser:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 62
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/TransparentCircle;->getWidth()I

    move-result v4

    .line 82
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/TransparentCircle;->getHeight()I

    move-result v2

    .line 85
    .local v2, "height":I
    const/4 v3, 0x0

    .line 86
    .local v3, "radius":I
    if-le v4, v2, :cond_0

    .line 87
    div-int/lit8 v3, v2, 0x2

    .line 92
    :goto_0
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->bitmap:Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 93
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->canvas:Landroid/graphics/Canvas;

    const/high16 v6, -0x1000000

    invoke-virtual {v5, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 96
    div-int/lit8 v1, v4, 0x2

    .line 97
    .local v1, "centerWidth":I
    div-int/lit8 v0, v2, 0x2

    .line 98
    .local v0, "centerHeight":I
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->canvas:Landroid/graphics/Canvas;

    int-to-float v6, v1

    int-to-float v7, v0

    int-to-float v8, v3

    iget-object v9, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->eraser:Landroid/graphics/Paint;

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 99
    iget-object v5, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->bitmap:Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v10, v10, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 101
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 102
    return-void

    .line 89
    .end local v0    # "centerHeight":I
    .end local v1    # "centerWidth":I
    :cond_0
    div-int/lit8 v3, v4, 0x2

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 67
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 69
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->bitmap:Landroid/graphics/Bitmap;

    .line 70
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/models/TransparentCircle;->canvas:Landroid/graphics/Canvas;

    .line 72
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 73
    return-void
.end method
