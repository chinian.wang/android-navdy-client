.class public Lcom/navdy/client/app/framework/models/Address;
.super Ljava/lang/Object;
.source "Address.java"


# static fields
.field private static final VERBOSE:Z

.field public static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public city:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public postBox:Ljava/lang/String;

.field public postCode:Ljava/lang/String;

.field public state:Ljava/lang/String;

.field public street:Ljava/lang/String;

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/Address;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/Address;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "pobox"    # Ljava/lang/String;
    .param p2, "street"    # Ljava/lang/String;
    .param p3, "city"    # Ljava/lang/String;
    .param p4, "state"    # Ljava/lang/String;
    .param p5, "postcode"    # Ljava/lang/String;
    .param p6, "country"    # Ljava/lang/String;
    .param p7, "type"    # I
    .param p8, "name"    # Ljava/lang/String;
    .param p9, "label"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    .line 35
    iput-object p6, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    .line 36
    iput-object p8, p0, Lcom/navdy/client/app/framework/models/Address;->name:Ljava/lang/String;

    .line 37
    iput p7, p0, Lcom/navdy/client/app/framework/models/Address;->type:I

    .line 38
    iput-object p9, p0, Lcom/navdy/client/app/framework/models/Address;->label:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 106
    instance-of v0, p1, Lcom/navdy/client/app/framework/models/Address;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Address;->type:I

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget v0, v0, Lcom/navdy/client/app/framework/models/Address;->type:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    .line 108
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    .line 109
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    .line 110
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    .line 111
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/navdy/client/app/framework/models/Address;

    iget-object v0, v0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    .line 112
    invoke-static {v1, v0}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    check-cast p1, Lcom/navdy/client/app/framework/models/Address;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    .line 113
    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFullAddress()Ljava/lang/String;
    .locals 4

    .prologue
    .line 67
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    .line 100
    :goto_0
    return-object v2

    .line 78
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v0, "fullAddress":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 80
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 83
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 86
    :cond_3
    const-string v1, ""

    .line 87
    .local v1, "stateAndZipCode":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 88
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    .line 90
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 95
    :cond_5
    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    .end local v1    # "stateAndZipCode":Ljava/lang/String;
    :cond_6
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 98
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_7
    const-string v2, ", "

    invoke-static {v0, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->join(Ljava/lang/Iterable;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 92
    .restart local v1    # "stateAndZipCode":Ljava/lang/String;
    :cond_8
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 93
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 118
    iget v0, p0, Lcom/navdy/client/app/framework/models/Address;->type:I

    .line 119
    .local v0, "result":I
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 120
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 121
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v3, v1

    .line 122
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_3
    add-int v0, v3, v1

    .line 123
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_4
    add-int v0, v3, v1

    .line 124
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 125
    return v0

    :cond_1
    move v1, v2

    .line 119
    goto :goto_0

    :cond_2
    move v1, v2

    .line 120
    goto :goto_1

    :cond_3
    move v1, v2

    .line 121
    goto :goto_2

    :cond_4
    move v1, v2

    .line 122
    goto :goto_3

    :cond_5
    move v1, v2

    .line 123
    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Address{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/navdy/client/app/framework/models/Address;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postBox=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postBox:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", street=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->street:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", city=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", postCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->postCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", country=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->country:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", label=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Address;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
