.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->clearMapObjects()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 312
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 2
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 295
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$200(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 297
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$202(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$500(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$500(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 302
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$502(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 306
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 307
    iget-object v0, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$7;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$402(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 309
    :cond_2
    return-void
.end method
