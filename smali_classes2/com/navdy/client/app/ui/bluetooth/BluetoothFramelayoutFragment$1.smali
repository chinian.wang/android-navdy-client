.class Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;
.super Ljava/lang/Object;
.source "BluetoothFramelayoutFragment.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->setupBluetooth(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceListChanged(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "deviceList":Ljava/util/Set;, "Ljava/util/Set<Lcom/navdy/service/library/device/connection/ConnectionInfo;>;"
    const v3, 0x7f030060

    .line 130
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->addConnectionInfo(Ljava/util/Set;)V

    .line 131
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$000(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceListChanged. Here is the current deviceList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v2, v2, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$100(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)I

    move-result v0

    const v1, 0x7f03005e

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$100(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 134
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-virtual {v0, v3}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->showDialog(I)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->access$200(Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;)Landroid/support/v7/widget/RecyclerView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v1, p0, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment$1;->this$0:Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;

    iget-object v1, v1, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->mDevices:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/bluetooth/BluetoothFramelayoutFragment;->updateRecyclerView(Ljava/util/ArrayList;)V

    .line 139
    :cond_1
    return-void
.end method
