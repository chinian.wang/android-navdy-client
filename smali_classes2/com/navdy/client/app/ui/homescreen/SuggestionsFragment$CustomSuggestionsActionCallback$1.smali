.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->saveSuggestionAsFavorite(Lcom/navdy/client/app/framework/models/Suggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    .prologue
    .line 1108
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryCompleted(ILandroid/net/Uri;)V
    .locals 1
    .param p1, "nbRows"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    iget-object v0, v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->hideProgressDialog()V

    .line 1112
    if-gtz p1, :cond_1

    .line 1113
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->access$1200(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V

    .line 1121
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->access$1300(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)V

    .line 1118
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->access$1400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1119
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback$1;->this$1:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;->access$1400(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;)Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->onFavoriteListChanged()V

    goto :goto_0
.end method
