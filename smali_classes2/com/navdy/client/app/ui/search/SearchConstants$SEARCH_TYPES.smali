.class public final enum Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
.super Ljava/lang/Enum;
.source "SearchConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/search/SearchConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SEARCH_TYPES"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

.field public static final enum CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

.field public static final enum FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

.field public static final enum HOME:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

.field public static final enum SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

.field public static final enum WORK:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .line 39
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const-string v1, "FAVORITE"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .line 40
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->HOME:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .line 41
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const-string v1, "WORK"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->WORK:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .line 42
    new-instance v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    const-string v1, "CONTACT"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->SEARCH:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->FAVORITE:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->HOME:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->WORK:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->CONTACT:Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->code:I

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->$VALUES:[Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/navdy/client/app/ui/search/SearchConstants$SEARCH_TYPES;->code:I

    return v0
.end method
