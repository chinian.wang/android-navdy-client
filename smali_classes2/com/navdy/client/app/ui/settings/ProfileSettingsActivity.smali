.class public Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
.super Lcom/navdy/client/app/ui/base/BaseEditActivity;
.source "ProfileSettingsActivity.java"


# static fields
.field public static final PICK_AN_IMAGE:I = 0x0

.field public static final REMOVE_PICTURE:I = 0x3

.field public static final TAKE_A_PICTURE:I = 0x1


# instance fields
.field protected final appContext:Landroid/content/Context;

.field protected final customerPrefs:Landroid/content/SharedPreferences;

.field protected email:Ljava/lang/String;

.field protected emailInput:Landroid/widget/EditText;

.field protected emailLabel:Landroid/support/design/widget/TextInputLayout;

.field protected final greyColorStateList:Landroid/content/res/ColorStateList;

.field protected name:Ljava/lang/String;

.field protected nameInput:Landroid/widget/EditText;

.field protected nameLabel:Landroid/support/design/widget/TextInputLayout;

.field protected photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

.field protected photoHint:Landroid/widget/LinearLayout;

.field protected profileFragment:Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

.field protected final redColorStateList:Landroid/content/res/ColorStateList;

.field protected userPhoto:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    .line 64
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    .line 67
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    .line 68
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->appContext:Landroid/content/Context;

    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->appContext:Landroid/content/Context;

    const v1, 0x7f0f0059

    .line 70
    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 69
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->greyColorStateList:Landroid/content/res/ColorStateList;

    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->appContext:Landroid/content/Context;

    const v1, 0x7f0f00af

    .line 74
    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 73
    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->redColorStateList:Landroid/content/res/ColorStateList;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->updatePhoto(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateName()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateEmail()Z

    move-result v0

    return v0
.end method

.method static synthetic access$302(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->updatePhoto(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$702(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->somethingChanged:Z

    return p1
.end method

.method private saveProfileInfo()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 385
    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveName(Z)Z

    move-result v1

    .line 386
    .local v1, "success":Z
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveEmail(Z)Z

    move-result v1

    .line 388
    if-eqz v1, :cond_1

    .line 389
    const v3, 0x7f080464

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 390
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 391
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/navdy/client/app/tracking/Tracker;->registerUser(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    if-ne v3, v4, :cond_0

    move v0, v2

    .line 395
    .local v0, "isOnMainThread":Z
    :cond_0
    if-eqz v0, :cond_2

    .line 396
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$8;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$8;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 406
    .end local v0    # "isOnMainThread":Z
    :cond_1
    :goto_0
    return v1

    .line 403
    .restart local v0    # "isOnMainThread":Z
    :cond_2
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    goto :goto_0
.end method

.method private updatePhoto(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 3
    .param p1, "userPhoto"    # Landroid/graphics/Bitmap;
    .param p2, "fullName"    # Ljava/lang/String;

    .prologue
    .line 360
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    if-eqz v1, :cond_0

    .line 361
    const v1, 0x7f10032c

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 362
    .local v0, "hintText":Landroid/widget/TextView;
    if-eqz p1, :cond_1

    .line 363
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    invoke-virtual {v1, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(Landroid/graphics/Bitmap;)V

    .line 364
    if-eqz v0, :cond_0

    .line 365
    const v1, 0x7f08014d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 374
    .end local v0    # "hintText":Landroid/widget/TextView;
    :cond_0
    :goto_0
    return-void

    .line 368
    .restart local v0    # "hintText":Landroid/widget/TextView;
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    const v2, 0x7f0201ce

    invoke-virtual {v1, v2, p2}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;)V

    .line 369
    if-eqz v0, :cond_0

    .line 370
    const v1, 0x7f0800af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private updatePhoto(Ljava/lang/String;)V
    .locals 2
    .param p1, "fullName"    # Ljava/lang/String;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    const v1, 0x7f0201ce

    invoke-virtual {v0, v1, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;)V

    .line 357
    :cond_0
    return-void
.end method

.method private validateEmail()Z
    .locals 4

    .prologue
    .line 424
    const/4 v1, 0x1

    .line 425
    .local v1, "success":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, "newEmail":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 429
    const-string v2, "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 433
    :cond_0
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 434
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailLabel:Landroid/support/design/widget/TextInputLayout;

    const v3, 0x7f0c0227

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 435
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->greyColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 441
    :goto_0
    return v1

    .line 437
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailLabel:Landroid/support/design/widget/TextInputLayout;

    const v3, 0x7f0c0245

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 438
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->redColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 439
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private validateName()Z
    .locals 4

    .prologue
    .line 410
    const/4 v1, 0x1

    .line 411
    .local v1, "success":Z
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 412
    .local v0, "newName":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidName(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 413
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameLabel:Landroid/support/design/widget/TextInputLayout;

    const v3, 0x7f0c0227

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 414
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->greyColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 420
    :goto_0
    return v1

    .line 416
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameLabel:Landroid/support/design/widget/TextInputLayout;

    const v3, 0x7f0c0245

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TextInputLayout;->setHintTextAppearance(I)V

    .line 417
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->redColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v2, v3}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 418
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public doProfileOnCreate(Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;)V
    .locals 3
    .param p1, "profileFragment"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->profileFragment:Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    .line 98
    if-eqz p1, :cond_1

    .line 99
    invoke-virtual {p1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 100
    .local v0, "rootView":Landroid/view/View;
    const v1, 0x7f10016f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    .line 101
    const v1, 0x7f100171

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    .line 102
    const v1, 0x7f10016e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TextInputLayout;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameLabel:Landroid/support/design/widget/TextInputLayout;

    .line 103
    const v1, 0x7f100170

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/TextInputLayout;

    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailLabel:Landroid/support/design/widget/TextInputLayout;

    .line 105
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->greyColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 107
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    new-instance v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$1;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    new-instance v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$2;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$2;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->greyColorStateList:Landroid/content/res/ColorStateList;

    invoke-static {v1, v2}, Landroid/support/v4/view/ViewCompat;->setBackgroundTintList(Landroid/view/View;Landroid/content/res/ColorStateList;)V

    .line 132
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    new-instance v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$3;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 144
    .end local v0    # "rootView":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public isDifferentFromBefore(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "newString"    # Ljava/lang/String;
    .param p2, "oldString"    # Ljava/lang/String;

    .prologue
    .line 509
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    .line 510
    invoke-static {p1, p2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "responseIntent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x23ca

    .line 197
    const/4 v3, -0x1

    if-ne p2, v3, :cond_1

    .line 198
    const/4 v3, 0x1

    if-eq p1, v3, :cond_0

    if-ne p1, v5, :cond_3

    .line 199
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "temp_photo.jpg"

    invoke-static {v3, v4}, Lcom/navdy/service/library/util/IOUtils;->getTempFile(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 200
    .local v2, "tempFile":Ljava/io/File;
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 202
    .local v0, "destination":Landroid/net/Uri;
    if-ne p1, v5, :cond_2

    .line 203
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 207
    .local v1, "source":Landroid/net/Uri;
    :goto_0
    new-instance v3, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$4;

    invoke-direct {v3, p0, v1, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$4;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Landroid/net/Uri;Landroid/net/Uri;)V

    new-instance v4, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$5;

    invoke-direct {v4, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$5;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 273
    .end local v0    # "destination":Landroid/net/Uri;
    .end local v1    # "source":Landroid/net/Uri;
    .end local v2    # "tempFile":Ljava/io/File;
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 274
    return-void

    .line 205
    .restart local v0    # "destination":Landroid/net/Uri;
    .restart local v2    # "tempFile":Ljava/io/File;
    :cond_2
    move-object v1, v0

    .restart local v1    # "source":Landroid/net/Uri;
    goto :goto_0

    .line 218
    .end local v0    # "destination":Landroid/net/Uri;
    .end local v1    # "source":Landroid/net/Uri;
    .end local v2    # "tempFile":Ljava/io/File;
    :cond_3
    const/16 v3, 0x1a35

    if-ne p1, v3, :cond_1

    .line 219
    new-instance v3, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;

    invoke-direct {v3, p0, p3}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;Landroid/content/Intent;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    .line 270
    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$6;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public onChangePhoto(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090007

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "options":[Ljava/lang/CharSequence;
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 287
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f08045d

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 288
    new-instance v2, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity$7;-><init>(Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 321
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 322
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    instance-of v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    if-nez v1, :cond_0

    .line 85
    const v1, 0x7f0300fa

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->setContentView(I)V

    .line 86
    new-instance v1, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v2, 0x7f0802c0

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 88
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f1003db

    .line 89
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    .line 90
    .local v0, "profileFragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->doProfileOnCreate(Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;)V

    .line 92
    .end local v0    # "profileFragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 192
    const/4 v0, 0x1

    return v0
.end method

.method public onNextProfileStepClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->saveProfileInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->onProfileInfoSet()V

    .line 342
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 149
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveName(Z)Z

    .line 150
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateAndSaveEmail(Z)Z

    .line 151
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onPause()V

    .line 152
    return-void
.end method

.method public onPrivacyPolicyClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 329
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 330
    .local v0, "browserIntent":Landroid/content/Intent;
    const-string v1, "type"

    const-string v2, "Privacy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 331
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 332
    return-void
.end method

.method public onProfileInfoSet()V
    .locals 0

    .prologue
    .line 348
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->finish()V

    .line 349
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 156
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->onResume()V

    .line 159
    const v0, 0x7f100169

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photo:Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    .line 160
    const v0, 0x7f10016a

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->photoHint:Landroid/widget/LinearLayout;

    .line 163
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 164
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserProfilePhoto()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v1, "fname"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    const-string v1, "email"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->userPhoto:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->updatePhoto(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 174
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateName()Z

    .line 176
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 177
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 179
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateEmail()Z

    .line 183
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->somethingChanged:Z

    .line 185
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 187
    const-string v0, "Settings_Profile"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 188
    return-void
.end method

.method protected saveChanges()V
    .locals 0

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->saveProfileInfo()Z

    .line 382
    return-void
.end method

.method protected validateAndSaveEmail(Z)Z
    .locals 5
    .param p1, "success"    # Z

    .prologue
    .line 474
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    if-eqz v2, :cond_2

    .line 475
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 478
    .local v1, "newEmail":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, ".*[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000].*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 479
    const-string v2, "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 480
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->emailInput:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 483
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->isDifferentFromBefore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 484
    .local v0, "differentFromBefore":Z
    if-eqz v0, :cond_1

    .line 485
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->somethingChanged:Z

    .line 488
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateEmail()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 489
    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    .line 490
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_2

    .line 491
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    .line 492
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "email"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->email:Ljava/lang/String;

    .line 493
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 496
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 505
    .end local v0    # "differentFromBefore":Z
    .end local v1    # "newEmail":Ljava/lang/String;
    :cond_2
    :goto_0
    return p1

    .line 499
    .restart local v0    # "differentFromBefore":Z
    .restart local v1    # "newEmail":Ljava/lang/String;
    :cond_3
    if-eqz v0, :cond_4

    .line 500
    const v2, 0x7f080462

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 502
    :cond_4
    const/4 p1, 0x0

    goto :goto_0
.end method

.method protected validateAndSaveName(Z)Z
    .locals 5
    .param p1, "success"    # Z

    .prologue
    .line 445
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    .line 446
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->nameInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 448
    .local v1, "newName":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->isDifferentFromBefore(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 449
    .local v0, "differentFromBefore":Z
    if-eqz v0, :cond_0

    .line 450
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->somethingChanged:Z

    .line 453
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->validateName()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 454
    iput-object v1, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    .line 455
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    .line 456
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->customerPrefs:Landroid/content/SharedPreferences;

    .line 457
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "fname"

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->name:Ljava/lang/String;

    .line 458
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 461
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 470
    .end local v0    # "differentFromBefore":Z
    .end local v1    # "newName":Ljava/lang/String;
    :cond_1
    :goto_0
    return p1

    .line 464
    .restart local v0    # "differentFromBefore":Z
    .restart local v1    # "newName":Ljava/lang/String;
    :cond_2
    if-eqz v0, :cond_3

    .line 465
    const v2, 0x7f080463

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/settings/ProfileSettingsActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 467
    :cond_3
    const/4 p1, 0x0

    goto :goto_0
.end method
