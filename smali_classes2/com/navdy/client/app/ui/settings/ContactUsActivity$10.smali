.class Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;
.super Ljava/lang/Object;
.source "ContactUsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ContactUsActivity;->showGetStoragePermissionDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 461
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 467
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 468
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 469
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 470
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 471
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$10;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 472
    return-void
.end method
