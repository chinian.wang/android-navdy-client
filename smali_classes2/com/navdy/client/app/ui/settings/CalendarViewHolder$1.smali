.class Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;
.super Ljava/lang/Object;
.source "CalendarViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->setCalendar(Lcom/navdy/client/app/framework/models/Calendar;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

.field final synthetic val$calendar:Lcom/navdy/client/app/framework/models/Calendar;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;Lcom/navdy/client/app/framework/models/Calendar;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->val$calendar:Lcom/navdy/client/app/framework/models/Calendar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 69
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->access$000(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "calendars_enabled"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 72
    .local v1, "globalSwitchIsEnabled":Z
    if-nez v1, :cond_0

    .line 81
    :goto_0
    return-void

    .line 77
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "calendar_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->val$calendar:Lcom/navdy/client/app/framework/models/Calendar;

    iget-wide v6, v5, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "sharedPrefKey":Ljava/lang/String;
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->access$000(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/content/SharedPreferences;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->val$calendar:Lcom/navdy/client/app/framework/models/Calendar;

    iget-boolean v5, v5, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    invoke-interface {v4, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 79
    .local v0, "enabled":Z
    :goto_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->access$000(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 80
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;->this$0:Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->access$100(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .end local v0    # "enabled":Z
    :cond_1
    move v0, v3

    .line 78
    goto :goto_1

    .line 80
    .restart local v0    # "enabled":Z
    :cond_2
    const/16 v3, 0x8

    goto :goto_2
.end method
