.class Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;
.super Landroid/os/AsyncTask;
.source "CarSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->saveChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

.field final synthetic val$autoOnIsEnabled:Z

.field final synthetic val$uiScalingIsOn:Z


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;ZZ)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    iput-boolean p2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->val$uiScalingIsOn:Z

    iput-boolean p3, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->val$autoOnIsEnabled:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$300(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 213
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hud_ui_scaling"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->val$uiScalingIsOn:Z

    .line 214
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hud_auto_on_enabled"

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->val$autoOnIsEnabled:Z

    .line 215
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hud_obd_on_enabled"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    .line 216
    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->access$200(Lcom/navdy/client/app/ui/settings/CarSettingsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 217
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 218
    const-string v0, "Profile_Settings_Changed"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 219
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementDriverProfileSerial()V

    .line 220
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const v0, 0x7f08042b

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->hideProgressDialog()V

    .line 231
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 232
    return-void

    .line 228
    :cond_0
    const v0, 0x7f080455

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->showShortToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 202
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 206
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 207
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CarSettingsActivity$2;->this$0:Lcom/navdy/client/app/ui/settings/CarSettingsActivity;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/settings/CarSettingsActivity;->showProgressDialog()V

    .line 208
    return-void
.end method
