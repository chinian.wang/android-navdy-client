.class Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;
.super Ljava/lang/Object;
.source "ContactUsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ContactUsActivity;->onActivityResult(IILandroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

.field final synthetic val$responseIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/content/Intent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 567
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->val$responseIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/16 v8, 0x500

    const/16 v10, 0x55

    .line 570
    const/4 v4, 0x0

    .line 573
    .local v4, "tempFile":Ljava/io/File;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->val$responseIntent:Landroid/content/Intent;

    if-eqz v5, :cond_0

    .line 574
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->val$responseIntent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 575
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 576
    const-string v5, "data"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 577
    .local v2, "image":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "image status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 578
    if-eqz v2, :cond_0

    .line 579
    new-instance v4, Ljava/io/File;

    .end local v4    # "tempFile":Ljava/io/File;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 580
    .restart local v4    # "tempFile":Ljava/io/File;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {v5, v2, v4, v6, v10}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)V

    .line 586
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "image":Landroid/graphics/Bitmap;
    :cond_0
    if-nez v4, :cond_1

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 587
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1400(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "retrieving photo from storage"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 588
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1500(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->getTempFile(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 591
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 592
    invoke-static {v4, v8, v8}, Lcom/navdy/client/app/framework/util/ImageUtils;->getScaledBitmap(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 596
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    .line 597
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1600(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "bitmap was null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 598
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1700(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    .line 632
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_0
    return-void

    .line 602
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v8, 0x200000

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    .line 603
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-static {v5, v0, v4, v6, v10}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/graphics/Bitmap;Ljava/io/File;Landroid/graphics/Bitmap$CompressFormat;I)V

    .line 608
    :cond_4
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1800(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->getTempFile(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    .line 609
    .local v3, "photoFile":Ljava/io/File;
    if-eqz v3, :cond_6

    .line 615
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1900(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$2000(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    .line 617
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-virtual {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->incrementPhotoAttachments()V

    .line 618
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$2100(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/TextView;

    move-result-object v5

    const v6, 0x7f0800c6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 620
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$2200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/util/Iterator;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$2200(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 621
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5, v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$2300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/graphics/Bitmap;)V

    .line 630
    :cond_5
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 627
    :cond_6
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$13;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$1700(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    goto :goto_1
.end method
