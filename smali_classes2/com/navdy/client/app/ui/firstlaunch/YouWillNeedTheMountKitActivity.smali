.class public Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "YouWillNeedTheMountKitActivity.java"


# static fields
.field public static final MOUNT_UPSELL_URL:Ljava/lang/String; = "https://shop.navdy.com/products/mount-kit?utm_source=navdy-app&utm_medium=upsell"


# instance fields
.field private mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->onBackPressed()V

    .line 93
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const v3, 0x7f030083

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->setContentView(I)V

    .line 39
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 40
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v3, "vehicle-model"

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 42
    .local v2, "modelString":Ljava/lang/String;
    const v3, 0x7f1001f4

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 43
    .local v1, "description":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 44
    const v3, 0x7f080506

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I[Ljava/lang/Object;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    :cond_0
    const v3, 0x7f1000b1

    const v4, 0x7f020059

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->loadImage(II)V

    .line 48
    return-void
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z

    if-nez v1, :cond_0

    .line 62
    const-string v1, "Continue_With_Unsupported_Short_Mount"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 63
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_mount"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->startActivity(Landroid/content/Intent;)V

    .line 71
    return-void

    .line 66
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const-string v1, "Continue_With_Unrecommended_Short_Mount"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "extra_mount"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onPurchaseClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 86
    const-string v0, "Installation_Mount_Kit_Purchase"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 87
    const-string v0, "Purchase_Mount_Kit"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 88
    const-string v0, "https://shop.navdy.com/products/mount-kit?utm_source=navdy-app&utm_medium=upsell"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->openBrowserFor(Landroid/net/Uri;)V

    .line 89
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 53
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->hideSystemUI()V

    .line 55
    const-string v0, "Installation_Mount_Kit_Needed"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getMountInfo()Lcom/navdy/client/app/framework/models/MountInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 57
    return-void
.end method

.method public onUserHasMediumMountClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 74
    const-string v1, "Actually_I_Already_Have_Med_Tall_Mount"

    invoke-static {v1}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 77
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "box"

    const-string v3, "New_Box_Plus_Mounts"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 80
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_mount"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;->startActivity(Landroid/content/Intent;)V

    .line 83
    return-void
.end method
