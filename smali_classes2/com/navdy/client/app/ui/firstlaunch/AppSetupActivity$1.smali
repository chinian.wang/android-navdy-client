.class Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;
.super Ljava/lang/Object;
.source "AppSetupActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 103
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactsManager;->lookUpUserProfileInfo()Lcom/navdy/client/app/framework/models/UserAccountInfo;

    move-result-object v4

    .line 104
    .local v4, "userAccountInfo":Lcom/navdy/client/app/framework/models/UserAccountInfo;
    if-eqz v4, :cond_4

    .line 106
    const/4 v2, 0x0

    .line 107
    .local v2, "incrementAndSendToHud":Z
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/graphics/Bitmap;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v5, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_0

    .line 108
    iget-object v5, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    invoke-static {v5}, Lcom/navdy/client/app/tracking/Tracker;->saveUserPhotoToInternalStorage(Landroid/graphics/Bitmap;)V

    .line 109
    const/4 v2, 0x1

    .line 112
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$100(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 113
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$200(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 115
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$300(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidName(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidName(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 116
    const-string v5, "fname"

    iget-object v6, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 117
    const/4 v2, 0x1

    .line 120
    :cond_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    invoke-static {v5}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->access$400(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 121
    iget-object v5, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 122
    const-string v5, "email"

    iget-object v6, v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    const/4 v2, 0x1

    .line 137
    :cond_2
    :goto_0
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 140
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_3
    if-eqz v2, :cond_4

    .line 141
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v5

    new-instance v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1$1;

    invoke-direct {v6, p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;)V

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 150
    .end local v2    # "incrementAndSendToHud":Z
    :cond_4
    return-void

    .line 126
    .restart local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .restart local v2    # "incrementAndSendToHud":Z
    :cond_5
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity$1;->this$0:Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;

    const-string v6, "account"

    invoke-virtual {v5, v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/accounts/AccountManager;

    .line 127
    .local v3, "manager":Landroid/accounts/AccountManager;
    invoke-virtual {v3}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v6

    array-length v7, v6

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v7, :cond_2

    aget-object v0, v6, v5

    .line 128
    .local v0, "account":Landroid/accounts/Account;
    iget-object v8, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    const-string v9, "com.google"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 129
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isValidEmail(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 130
    const-string v5, "email"

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 131
    const/4 v2, 0x1

    .line 132
    goto :goto_0

    .line 127
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method
