.class Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;
.super Ljava/lang/Object;
.source "EditCarInfoUsingWebApiActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->processResponse(Lokhttp3/ResponseBody;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

.field final synthetic val$list:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "this$2"    # Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 201
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->hideProgressDialog()V

    .line 202
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 203
    .local v1, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    const/4 v4, 0x0

    const v5, 0x7f08046c

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 204
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    const v4, 0x7f080326

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    new-instance v0, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$year:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    invoke-direct {v0, v3, v4}, Lorg/droidparts/adapter/widget/StringSpinnerAdapter;-><init>(Landroid/widget/Spinner;Ljava/util/List;)V

    .line 207
    .local v0, "adapter":Lorg/droidparts/adapter/widget/StringSpinnerAdapter;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->this$0:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;

    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;->access$600(Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setting year list to: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 208
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$year:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 209
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$model:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v4, v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$modelAdapter:Lorg/droidparts/adapter/widget/StringSpinnerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 211
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->val$list:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v4, v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v4, v4, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$yearString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 212
    .local v2, "selection":I
    if-ltz v2, :cond_0

    .line 213
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1$1;->this$2:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2$1;->this$1:Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;

    iget-object v3, v3, Lcom/navdy/client/app/ui/firstlaunch/EditCarInfoUsingWebApiActivity$2;->val$year:Landroid/widget/Spinner;

    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 215
    :cond_0
    return-void
.end method
