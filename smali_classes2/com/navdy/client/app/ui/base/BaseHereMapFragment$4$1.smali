.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->onPhoneLocationChanged(Lcom/navdy/service/library/events/location/Coordinate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

.field final synthetic val$phoneLocation:Lcom/navdy/service/library/events/location/Coordinate;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->val$phoneLocation:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 194
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 6
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 182
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1100(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-nez v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v1, v1, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->val$phoneLocation:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1200(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1102(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/MapMarker;)Lcom/here/android/mpa/mapping/MapMarker;

    .line 185
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1100(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1100(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->this$1:Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;

    iget-object v0, v0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1100(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)Lcom/here/android/mpa/mapping/MapMarker;

    move-result-object v0

    new-instance v1, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->val$phoneLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v2, v2, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$4$1;->val$phoneLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v4, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/here/android/mpa/mapping/MapMarker;->setCoordinate(Lcom/here/android/mpa/common/GeoCoordinate;)Lcom/here/android/mpa/mapping/MapMarker;

    goto :goto_0
.end method
