.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    .prologue
    .line 955
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 958
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    instance-of v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;

    if-eqz v4, :cond_1

    .line 959
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;

    .line 960
    .local v2, "listener":Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$2000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;->setRoute(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;)V

    .line 961
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->popBackStack()V

    .line 974
    .end local v2    # "listener":Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteListener;
    :cond_0
    :goto_0
    return-void

    .line 963
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$2000(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v5, v5, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v5}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$2100(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    move-result-object v5

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/navdy/client/debug/RemoteNavControlFragment;->newInstance(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/debug/RemoteNavControlFragment;

    move-result-object v3

    .line 965
    .local v3, "navControlFragment":Lcom/navdy/client/debug/RemoteNavControlFragment;
    iget-object v4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;

    iget-object v4, v4, Lcom/navdy/client/debug/GoogleAddressPickerFragment$RouteDescriptionAdapter;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v4}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 966
    .local v0, "fm":Landroid/app/FragmentManager;
    if-eqz v0, :cond_0

    .line 967
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 968
    .local v1, "ft":Landroid/app/FragmentTransaction;
    const v4, 0x7f1000c6

    invoke-virtual {v1, v4, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 969
    const/16 v4, 0x1003

    invoke-virtual {v1, v4}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 970
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 971
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method
