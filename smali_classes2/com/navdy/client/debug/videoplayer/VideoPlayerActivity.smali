.class public Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
.super Landroid/app/Activity;
.source "VideoPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;
    }
.end annotation


# static fields
.field public static final BUCKET_NAME_EXTRA:Ljava/lang/String; = "bucket_name_extra"

.field public static final DB_URL_EXTRA:Ljava/lang/String; = "db_url_extra"

.field public static final SESSION_PATH_EXTRA:Ljava/lang/String; = "session_path_extra"

.field public static final VIDEO_URL_EXTRA:Ljava/lang/String; = "video_url_extra"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private bucketName:Ljava/lang/String;

.field private download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

.field downloadStatus:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1000da
    .end annotation
.end field

.field private downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

.field private localDbFile:Ljava/io/File;

.field private mediaController:Landroid/widget/MediaController;

.field private sessionPath:Ljava/lang/String;

.field private timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

.field private transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

.field private videoPlayerIsInitialized:Z

.field videoView:Landroid/widget/VideoView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f1000d9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoPlayerIsInitialized:Z

    .line 277
    new-instance v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$1;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sessionPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->localDbFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->localDbFile:Ljava/io/File;

    return-object p1
.end method

.method static synthetic access$300()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->startMockLocationTransmitter()V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
    .param p1, "x1"    # Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->bucketName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    return-object v0
.end method

.method static synthetic access$802(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;)Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;
    .param p1, "x1"    # Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    return-object p1
.end method

.method private initVideoPlayer()V
    .locals 4

    .prologue
    .line 115
    iget-boolean v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoPlayerIsInitialized:Z

    if-eqz v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoPlayerIsInitialized:Z

    .line 120
    new-instance v1, Landroid/widget/MediaController;

    invoke-direct {v1, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    .line 121
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    iget-object v2, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, v2}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    .line 123
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 124
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 126
    invoke-virtual {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_url_extra"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/net/URL;

    .line 127
    .local v0, "videoUrl":Ljava/net/URL;
    sget-object v1, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to play video at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 129
    if-eqz v0, :cond_0

    .line 130
    iget-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private startLocationTransmitter()V
    .locals 4

    .prologue
    .line 253
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    .line 254
    .local v1, "instance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 256
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 257
    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/AppInstance;->setLocationTransmitter(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V

    .line 259
    :cond_0
    return-void
.end method

.method private startMockLocationTransmitter()V
    .locals 7

    .prologue
    .line 262
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    .line 263
    .local v2, "instance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 265
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 266
    sget-object v4, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Starting location transmitter with db at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->localDbFile:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 267
    new-instance v3, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;

    invoke-direct {v3, p0, v0}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;-><init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 268
    .local v3, "transmitter":Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    iget-object v4, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->localDbFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->timeSource:Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;

    invoke-virtual {v3, v4, v5}, Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;->setSource(Ljava/lang/String;Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter$ITimeSource;)Z

    move-result v1

    .line 269
    .local v1, "hasLocations":Z
    if-eqz v1, :cond_1

    .line 270
    invoke-virtual {v2, v3}, Lcom/navdy/client/app/framework/AppInstance;->setLocationTransmitter(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V

    .line 275
    .end local v1    # "hasLocations":Z
    .end local v3    # "transmitter":Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    :cond_0
    :goto_0
    return-void

    .line 272
    .restart local v1    # "hasLocations":Z
    .restart local v3    # "transmitter":Lcom/navdy/client/debug/videoplayer/MockLocationTransmitter;
    :cond_1
    const-string v4, "No location info with video"

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v11, 0x400

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->requestWindowFeature(I)Z

    .line 79
    invoke-virtual {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v10

    .line 80
    invoke-virtual {v10, v11, v11}, Landroid/view/Window;->setFlags(II)V

    .line 83
    const v10, 0x7f030024

    invoke-virtual {p0, v10}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->setContentView(I)V

    .line 85
    invoke-virtual {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "session_path_extra"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sessionPath:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    const-string v11, "bucket_name_extra"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->bucketName:Ljava/lang/String;

    .line 88
    invoke-static {p0}, Lbutterknife/ButterKnife;->inject(Landroid/app/Activity;)V

    .line 90
    const-string v0, ""

    .line 91
    .local v0, "AWS_ACCOUNT_ID":Ljava/lang/String;
    const-string v1, ""

    .line 94
    .local v1, "AWS_SECRET":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    .line 95
    .local v4, "context":Landroid/content/Context;
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 96
    .local v8, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    .line 97
    .local v9, "packageName":Ljava/lang/String;
    const/16 v7, 0x80

    .line 98
    .local v7, "flags":I
    const/16 v10, 0x80

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 100
    .local v2, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 101
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v10, "AWS_ACCOUNT_ID"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    const-string v10, "AWS_SECRET"

    invoke-virtual {v3, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 109
    .end local v2    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v4    # "context":Landroid/content/Context;
    .end local v7    # "flags":I
    .end local v8    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v9    # "packageName":Ljava/lang/String;
    :goto_0
    new-instance v5, Lcom/amazonaws/auth/BasicAWSCredentials;

    invoke-direct {v5, v0, v1}, Lcom/amazonaws/auth/BasicAWSCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .local v5, "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    new-instance v10, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    invoke-direct {v10, v5}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;-><init>(Lcom/amazonaws/auth/AWSCredentials;)V

    iput-object v10, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    .line 112
    return-void

    .line 103
    .end local v5    # "credentials":Lcom/amazonaws/auth/BasicAWSCredentials;
    :catch_0
    move-exception v6

    .line 104
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v10, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to load meta-data, NameNotFound: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v6

    .line 106
    .local v6, "e":Ljava/lang/NullPointerException;
    sget-object v10, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to load meta-data, NullPointer: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 152
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->cancel(Z)Z

    .line 158
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    .line 162
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->startLocationTransmitter()V

    .line 163
    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 286
    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    if-eqz p1, :cond_0

    .line 288
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->stop()V

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 296
    :cond_1
    :goto_0
    return-void

    .line 293
    :cond_2
    if-eqz p1, :cond_1

    .line 294
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 136
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 138
    invoke-direct {p0}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->initVideoPlayer()V

    .line 140
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 144
    :cond_0
    new-instance v0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;-><init>(Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$1;)V

    iput-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    .line 145
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->downloadTask:Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity$DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 146
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 168
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->cancelPendingInputEvents()V

    .line 170
    iput-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->videoView:Landroid/widget/VideoView;

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    .line 175
    iget-object v0, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->removeAllViews()V

    .line 176
    iput-object v1, p0, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;->mediaController:Landroid/widget/MediaController;

    .line 179
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 180
    return-void
.end method
