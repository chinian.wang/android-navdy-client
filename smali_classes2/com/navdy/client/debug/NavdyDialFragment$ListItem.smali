.class final enum Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
.super Ljava/lang/Enum;
.source "NavdyDialFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/NavdyDialFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/NavdyDialFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

.field public static final enum BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

.field public static final enum CLEAR_BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

.field public static final enum GET_DIAL_STATUS:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

.field public static final enum REBOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;


# instance fields
.field private final mResId:I

.field private mString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 42
    new-instance v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    const-string v1, "GET_DIAL_STATUS"

    const v2, 0x7f08012b

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->GET_DIAL_STATUS:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 43
    new-instance v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    const-string v1, "BOND"

    const v2, 0x7f080129

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 44
    new-instance v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    const-string v1, "REBOND"

    const v2, 0x7f08012d

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->REBOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 45
    new-instance v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    const-string v1, "CLEAR_BOND"

    const v2, 0x7f08012a

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->CLEAR_BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->GET_DIAL_STATUS:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->REBOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->CLEAR_BOND:Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "item"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->mResId:I

    .line 52
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method bindResource(Landroid/content/res/Resources;)V
    .locals 1
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 56
    iget v0, p0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->mResId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->mString:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->mString:Ljava/lang/String;

    return-object v0
.end method
