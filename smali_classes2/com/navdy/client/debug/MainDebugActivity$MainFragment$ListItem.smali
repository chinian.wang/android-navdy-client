.class final enum Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
.super Ljava/lang/Enum;
.source "MainDebugActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/MainDebugActivity$MainFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum DEBUG:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum GESTURE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum MUSIC:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum NAVDY_DIAL:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum RECENTS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum SEARCH:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum SETTINGS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum STOP_RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum TICKET:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

.field public static final enum UPLOAD_TRIP_DATA:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;


# instance fields
.field private final mResId:I

.field private mString:Ljava/lang/String;

.field private targetFragment:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 115
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "SEARCH"

    const v2, 0x7f080297

    const-class v3, Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->SEARCH:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 116
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "RECENTS"

    const v2, 0x7f080295

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->RECENTS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 117
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "SETTINGS"

    const v2, 0x7f080298

    const-class v3, Lcom/navdy/client/debug/SettingsFragment;

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->SETTINGS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 118
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "GESTURE"

    const v2, 0x7f080292

    const-class v3, Lcom/navdy/client/debug/gesture/GestureControlFragment;

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->GESTURE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 119
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "DEBUG"

    const v2, 0x7f080291

    const-class v3, Lcom/navdy/client/debug/DebugActionsFragment;

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->DEBUG:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 120
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "TICKET"

    const/4 v2, 0x5

    const v3, 0x7f08029a

    const-class v4, Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->TICKET:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 121
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "NAVDY_DIAL"

    const/4 v2, 0x6

    const v3, 0x7f080294

    const-class v4, Lcom/navdy/client/debug/NavdyDialFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->NAVDY_DIAL:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 122
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "RECORD_ROUTE"

    const/4 v2, 0x7

    const v3, 0x7f080296

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 123
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "STOP_RECORD_ROUTE"

    const/16 v2, 0x8

    const v3, 0x7f080299

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->STOP_RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 124
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "UPLOAD_TRIP_DATA"

    const/16 v2, 0x9

    const v3, 0x7f0804e7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->UPLOAD_TRIP_DATA:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 125
    new-instance v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    const-string v1, "MUSIC"

    const/16 v2, 0xa

    const v3, 0x7f080293

    const-class v4, Lcom/navdy/client/debug/music/MusicControlFragment;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->MUSIC:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 114
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->SEARCH:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->RECENTS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->SETTINGS:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->GESTURE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v1, v0, v8

    sget-object v1, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->DEBUG:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->TICKET:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->NAVDY_DIAL:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->STOP_RECORD_ROUTE:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->UPLOAD_TRIP_DATA:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->MUSIC:Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "item"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 132
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;-><init>(Ljava/lang/String;IILjava/lang/Class;)V

    .line 133
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/Class;)V
    .locals 0
    .param p3, "mResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p4, "targetFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Fragment;>;"
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 136
    iput p3, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->mResId:I

    .line 137
    iput-object p4, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->targetFragment:Ljava/lang/Class;

    .line 138
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 114
    const-class v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method bindResource(Landroid/content/res/Resources;)V
    .locals 1
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 146
    iget v0, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->mResId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->mString:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public getTargetFragment()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->targetFragment:Ljava/lang/Class;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->mString:Ljava/lang/String;

    return-object v0
.end method
