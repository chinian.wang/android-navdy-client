.class public Lcom/navdy/client/debug/view/ConnectionStateIndicator;
.super Landroid/widget/ImageView;
.source "ConnectionStateIndicator.java"


# static fields
.field private static final CONNECTED:[I

.field private static final CONNECTING:[I

.field private static final DISCONNECTED:[I

.field private static final USING_BLUETOOTH:[I


# instance fields
.field private status:Lcom/navdy/service/library/device/connection/Connection$Status;

.field private usingBluetooth:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-array v0, v3, [I

    const v1, 0x7f010101

    aput v1, v0, v2

    sput-object v0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->USING_BLUETOOTH:[I

    .line 25
    new-array v0, v3, [I

    const v1, 0x7f010103

    aput v1, v0, v2

    sput-object v0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->CONNECTED:[I

    .line 26
    new-array v0, v3, [I

    const v1, 0x7f010102

    aput v1, v0, v2

    sput-object v0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->CONNECTING:[I

    .line 27
    new-array v0, v3, [I

    const v1, 0x7f010104

    aput v1, v0, v2

    sput-object v0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->DISCONNECTED:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->usingBluetooth:Z

    .line 30
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;

    iput-object v0, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->status:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 42
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 79
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 81
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    instance-of v2, v2, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v2, :cond_0

    .line 82
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 83
    .local v0, "anim":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 85
    .end local v0    # "anim":Landroid/graphics/drawable/AnimationDrawable;
    :cond_0
    return-void
.end method

.method public onCreateDrawableState(I)[I
    .locals 3
    .param p1, "extraSpace"    # I

    .prologue
    .line 47
    add-int/lit8 v1, p1, 0x2

    invoke-super {p0, v1}, Landroid/widget/ImageView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 49
    .local v0, "drawableState":[I
    iget-boolean v1, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->usingBluetooth:Z

    if-eqz v1, :cond_0

    .line 50
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->USING_BLUETOOTH:[I

    invoke-static {v0, v1}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->mergeDrawableStates([I[I)[I

    .line 52
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->status:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-nez v1, :cond_1

    .line 53
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->DISCONNECTED:[I

    invoke-static {v0, v1}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->mergeDrawableStates([I[I)[I

    .line 71
    :goto_0
    return-object v0

    .line 55
    :cond_1
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator$1;->$SwitchMap$com$navdy$service$library$device$connection$Connection$Status:[I

    iget-object v2, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->status:Lcom/navdy/service/library/device/connection/Connection$Status;

    invoke-virtual {v2}, Lcom/navdy/service/library/device/connection/Connection$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 57
    :pswitch_0
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->DISCONNECTED:[I

    invoke-static {v0, v1}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 60
    :pswitch_1
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->CONNECTING:[I

    invoke-static {v0, v1}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v1, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->CONNECTED:[I

    invoke-static {v0, v1}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->mergeDrawableStates([I[I)[I

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setConnectionStatus(Lcom/navdy/service/library/device/connection/Connection$Status;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/device/connection/Connection$Status;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->status:Lcom/navdy/service/library/device/connection/Connection$Status;

    if-eq v0, p1, :cond_0

    .line 98
    iput-object p1, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->status:Lcom/navdy/service/library/device/connection/Connection$Status;

    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->refreshDrawableState()V

    .line 101
    :cond_0
    return-void
.end method

.method public setConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;)V
    .locals 2
    .param p1, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;

    .prologue
    .line 89
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    .line 90
    .local v0, "bluetooth":Z
    :goto_0
    iget-boolean v1, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->usingBluetooth:Z

    if-eq v1, v0, :cond_0

    .line 91
    iput-boolean v0, p0, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->usingBluetooth:Z

    .line 92
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/ConnectionStateIndicator;->refreshDrawableState()V

    .line 94
    :cond_0
    return-void

    .line 89
    .end local v0    # "bluetooth":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
