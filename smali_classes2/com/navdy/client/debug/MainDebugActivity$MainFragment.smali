.class public Lcom/navdy/client/debug/MainDebugActivity$MainFragment;
.super Landroid/app/ListFragment;
.source "MainDebugActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/MainDebugActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MainFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    }
.end annotation


# static fields
.field private static final DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

.field private mListItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    .line 110
    sget-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 111
    sget-object v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->DATETIME_CHANGE_INTENT_FILTER:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 157
    return-void
.end method

.method private pushNavigationSearchFragment()V
    .locals 2

    .prologue
    .line 282
    const-class v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .line 283
    .local v0, "nextFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Fragment;>;"
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 284
    return-void
.end method

.method private recordRoute()V
    .locals 8

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 240
    .local v2, "li":Landroid/view/LayoutInflater;
    const v5, 0x7f0300d4

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 242
    .local v3, "promptsView":Landroid/view/View;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 244
    .local v1, "alertDialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 246
    const v5, 0x7f100336

    .line 247
    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 249
    .local v4, "userInput":Landroid/widget/EditText;
    const-string v5, "Go"

    new-instance v6, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$2;

    invoke-direct {v6, p0, v4}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$2;-><init>(Lcom/navdy/client/debug/MainDebugActivity$MainFragment;Landroid/widget/EditText;)V

    .line 250
    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const-string v6, "Cancel"

    new-instance v7, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$1;

    invoke-direct {v7, p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$1;-><init>(Lcom/navdy/client/debug/MainDebugActivity$MainFragment;)V

    .line 256
    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 263
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 265
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 266
    return-void
.end method

.method private stopRecordingRoute()V
    .locals 1

    .prologue
    .line 269
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->getInstance()Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/servicehandler/RecordingServiceManager;->sendStopDriveRecordingEvent()V

    .line 270
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->values()[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mListItems:Ljava/util/List;

    .line 164
    iget-object v1, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mListItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 165
    .local v0, "item":Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->bindResource(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 168
    .end local v0    # "item":Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0300b6

    const v4, 0x1020014

    .line 171
    invoke-static {}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->values()[Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 168
    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 172
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    const v1, 0x7f03008f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 178
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 181
    new-instance v1, Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-direct {v1, v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    .line 183
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 208
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 210
    invoke-virtual {p1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 212
    iget-object v2, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mListItems:Ljava/util/List;

    invoke-interface {v2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;

    .line 214
    .local v0, "clickedItem":Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;
    sget-object v2, Lcom/navdy/client/debug/MainDebugActivity$1;->$SwitchMap$com$navdy$client$debug$MainDebugActivity$MainFragment$ListItem:[I

    invoke-virtual {v0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 229
    invoke-virtual {v0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment$ListItem;->getTargetFragment()Ljava/lang/Class;

    move-result-object v1

    .line 230
    .local v1, "nextFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Fragment;>;"
    if-eqz v1, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 235
    .end local v1    # "nextFragment":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/app/Fragment;>;"
    :cond_0
    :goto_0
    return-void

    .line 216
    :pswitch_0
    invoke-direct {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->pushNavigationSearchFragment()V

    goto :goto_0

    .line 219
    :pswitch_1
    invoke-direct {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->recordRoute()V

    goto :goto_0

    .line 222
    :pswitch_2
    invoke-direct {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->stopRecordingRoute()V

    goto :goto_0

    .line 225
    :pswitch_3
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/navdy/client/app/framework/suggestion/DestinationSuggestionService;->uploadTripDatabase(Landroid/content/Context;Z)V

    .line 226
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Thanks :) You are awesome !!"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 214
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v0}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onPause()V

    .line 189
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 190
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 194
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 195
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 196
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const v3, 0x7f0804b1

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 199
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->mConnectionStatus:Lcom/navdy/client/debug/ConnectionStatusUpdater;

    invoke-virtual {v2}, Lcom/navdy/client/debug/ConnectionStatusUpdater;->onResume()V

    .line 200
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 201
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {v1}, Lcom/navdy/client/ota/OTAUpdateService;->isLaunchedByOtaUpdateService(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/navdy/client/debug/MainDebugActivity$MainFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/navdy/client/debug/SettingsFragment;

    invoke-static {v2, v3}, Lcom/navdy/client/debug/util/FragmentHelper;->pushFullScreenFragment(Landroid/app/FragmentManager;Ljava/lang/Class;)V

    .line 204
    :cond_1
    return-void
.end method
