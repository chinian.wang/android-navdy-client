.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getHereNavigationCoordinate(Ljava/lang/String;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

.field final synthetic val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

.field final synthetic val$streetAddress:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Ljava/lang/String;Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$streetAddress:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 543
    const/4 v5, 0x0

    .line 546
    .local v5, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 547
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1400()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "&app_code="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    .line 548
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1500()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "&gen=9&searchtext="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$streetAddress:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "UTF-8"

    invoke-static/range {v21 .. v22}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 551
    .local v18, "urlStr":Ljava/lang/String;
    new-instance v16, Ljava/net/URL;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 552
    .local v16, "url":Ljava/net/URL;
    invoke-virtual/range {v16 .. v16}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v17

    check-cast v17, Ljavax/net/ssl/HttpsURLConnection;

    .line 553
    .local v17, "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    invoke-virtual/range {v17 .. v17}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v4

    .line 554
    .local v4, "httpResponse":I
    const/16 v20, 0xc8

    move/from16 v0, v20

    if-lt v4, v0, :cond_0

    const/16 v20, 0x12c

    move/from16 v0, v20

    if-ge v4, v0, :cond_0

    .line 555
    invoke-virtual/range {v17 .. v17}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 556
    const-string v20, "UTF-8"

    move-object/from16 v0, v20

    invoke-static {v5, v0}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 558
    .local v6, "jsonStr":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 559
    .local v14, "root":Lorg/json/JSONObject;
    const-string v20, "Response"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .line 560
    .local v12, "response":Lorg/json/JSONObject;
    const-string v20, "View"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v20

    const/16 v21, 0x0

    .line 561
    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lorg/json/JSONObject;

    .line 562
    .local v19, "view":Lorg/json/JSONObject;
    const-string v20, "Result"

    invoke-virtual/range {v19 .. v20}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v20

    const/16 v21, 0x0

    .line 563
    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 564
    .local v13, "result":Lorg/json/JSONObject;
    const-string v20, "Location"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 565
    .local v9, "location":Lorg/json/JSONObject;
    const-string v20, "NavigationPosition"

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v20

    const/16 v21, 0x0

    .line 566
    invoke-virtual/range {v20 .. v21}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 567
    .local v11, "navPos":Lorg/json/JSONObject;
    const-string v20, "Latitude"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 568
    .local v8, "latStr":Ljava/lang/String;
    const-string v20, "Longitude"

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 569
    .local v10, "lonStr":Ljava/lang/String;
    new-instance v7, Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v8}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v20

    .line 570
    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    move-wide/from16 v0, v20

    move-wide/from16 v2, v22

    invoke-direct {v7, v0, v1, v2, v3}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 571
    .local v7, "latLng":Lcom/google/android/gms/maps/model/LatLng;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onSuccess(Lcom/google/android/gms/maps/model/LatLng;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    .end local v6    # "jsonStr":Ljava/lang/String;
    .end local v7    # "latLng":Lcom/google/android/gms/maps/model/LatLng;
    .end local v8    # "latStr":Ljava/lang/String;
    .end local v9    # "location":Lorg/json/JSONObject;
    .end local v10    # "lonStr":Ljava/lang/String;
    .end local v11    # "navPos":Lorg/json/JSONObject;
    .end local v12    # "response":Lorg/json/JSONObject;
    .end local v13    # "result":Lorg/json/JSONObject;
    .end local v14    # "root":Lorg/json/JSONObject;
    .end local v19    # "view":Lorg/json/JSONObject;
    :goto_0
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 582
    .end local v4    # "httpResponse":I
    .end local v16    # "url":Ljava/net/URL;
    .end local v17    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v18    # "urlStr":Ljava/lang/String;
    :goto_1
    return-void

    .line 573
    .restart local v4    # "httpResponse":I
    .restart local v16    # "url":Ljava/net/URL;
    .restart local v17    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v18    # "urlStr":Ljava/lang/String;
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/RuntimeException;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "response code:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-direct/range {v21 .. v22}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v20 .. v21}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 576
    .end local v4    # "httpResponse":I
    .end local v16    # "url":Ljava/net/URL;
    .end local v17    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v18    # "urlStr":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 577
    .local v15, "t":Ljava/lang/Throwable;
    :try_start_2
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v20

    const-string v21, "getNavigationCoordinate"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v15}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$7;->val$callback:Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v15}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$INavigationPositionCallback;->onFailure(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 580
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v15    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v20

    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v20
.end method
