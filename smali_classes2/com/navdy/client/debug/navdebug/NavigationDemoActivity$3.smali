.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;
.super Ljava/lang/Object;
.source "NavigationDemoActivity.java"

# interfaces
.implements Lcom/here/android/mpa/routing/RouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCalculateRouteFinished(Lcom/here/android/mpa/routing/RouteManager$Error;Ljava/util/List;)V
    .locals 4
    .param p1, "error"    # Lcom/here/android/mpa/routing/RouteManager$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/here/android/mpa/routing/RouteManager$Error;",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "results":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    const/4 v3, 0x0

    .line 430
    sget-object v1, Lcom/here/android/mpa/routing/RouteManager$Error;->NONE:Lcom/here/android/mpa/routing/RouteManager$Error;

    if-ne p1, v1, :cond_0

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/routing/RouteResult;

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 431
    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/here/android/mpa/routing/RouteResult;

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$602(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/routing/Route;)Lcom/here/android/mpa/routing/Route;

    .line 432
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    new-instance v2, Lcom/here/android/mpa/mapping/MapRoute;

    iget-object v3, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v3}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$600(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/routing/Route;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/here/android/mpa/mapping/MapRoute;-><init>(Lcom/here/android/mpa/routing/Route;)V

    invoke-static {v1, v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$702(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/mapping/MapRoute;)Lcom/here/android/mpa/mapping/MapRoute;

    .line 433
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$800(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$700(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/MapRoute;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 435
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$600(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/routing/Route;

    move-result-object v1

    invoke-virtual {v1}, Lcom/here/android/mpa/routing/Route;->getBoundingBox()Lcom/here/android/mpa/common/GeoBoundingBox;

    move-result-object v0

    .line 436
    .local v0, "gbb":Lcom/here/android/mpa/common/GeoBoundingBox;
    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$3;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$800(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/mapping/Map;

    move-result-object v1

    sget-object v2, Lcom/here/android/mpa/mapping/Map$Animation;->NONE:Lcom/here/android/mpa/mapping/Map$Animation;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v1, v0, v2, v3}, Lcom/here/android/mpa/mapping/Map;->zoomTo(Lcom/here/android/mpa/common/GeoBoundingBox;Lcom/here/android/mpa/mapping/Map$Animation;F)V

    .line 441
    .end local v0    # "gbb":Lcom/here/android/mpa/common/GeoBoundingBox;
    :goto_0
    return-void

    .line 439
    :cond_0
    sget-object v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Route calculation failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/here/android/mpa/routing/RouteManager$Error;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onProgress(I)V
    .locals 3
    .param p1, "percentage"    # I

    .prologue
    .line 445
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "... "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "percent done ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 446
    return-void
.end method
