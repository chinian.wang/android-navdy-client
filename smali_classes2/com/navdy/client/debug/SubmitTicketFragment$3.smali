.class Lcom/navdy/client/debug/SubmitTicketFragment$3;
.super Ljava/lang/Object;
.source "SubmitTicketFragment.java"

# interfaces
.implements Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/SubmitTicketFragment;->bSubmitTicket()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

.field final synthetic val$appContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/SubmitTicketFragment;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/SubmitTicketFragment;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->val$appContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v3, 0x7f080160

    invoke-virtual {v2, v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v4, 0x7f080161

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 308
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 266
    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    .line 267
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v4, 0x3

    const-string v5, "Uploading"

    const-string v6, "Uploading files"

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    .local v1, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;>;"
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$100(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 270
    new-instance v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    invoke-direct {v0}, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;-><init>()V

    .line 271
    .local v0, "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$100(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    .line 272
    const-string v3, "text/plain"

    iput-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    .line 273
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    :cond_0
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$200(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 276
    new-instance v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;

    invoke-direct {v0}, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;-><init>()V

    .line 277
    .restart local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$200(Lcom/navdy/client/debug/SubmitTicketFragment;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->filePath:Ljava/lang/String;

    .line 278
    const-string v3, "application/zip"

    iput-object v3, v0, Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;->mimeType:Ljava/lang/String;

    .line 279
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    .end local v0    # "attachment":Lcom/navdy/service/library/network/http/services/JiraClient$Attachment;
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 282
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/SubmitTicketFragment;->access$500(Lcom/navdy/client/debug/SubmitTicketFragment;)Lcom/navdy/service/library/network/http/services/JiraClient;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;

    invoke-direct {v4, p0, v2, v1}, Lcom/navdy/client/debug/SubmitTicketFragment$3$1;-><init>(Lcom/navdy/client/debug/SubmitTicketFragment$3;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v2, v1, v4}, Lcom/navdy/service/library/network/http/services/JiraClient;->attachFilesToTicket(Ljava/lang/String;Ljava/util/List;Lcom/navdy/service/library/network/http/services/JiraClient$ResultCallback;)V

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_2
    iget-object v3, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/navdy/client/debug/SubmitTicketFragment$3;->this$0:Lcom/navdy/client/debug/SubmitTicketFragment;

    const v6, 0x7f08048f

    invoke-virtual {v5, v6}, Lcom/navdy/client/debug/SubmitTicketFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Issue created "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/navdy/client/debug/SubmitTicketFragment;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
