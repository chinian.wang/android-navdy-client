.class public Lcom/navdy/client/ota/OTAUpdateService;
.super Landroid/app/Service;
.source "OTAUpdateService.java"

# interfaces
.implements Lcom/navdy/client/ota/OTAUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;,
        Lcom/navdy/client/ota/OTAUpdateService$State;
    }
.end annotation


# static fields
.field private static final BUFFER_SPACE_FOR_FILE_DOWNLOAD:J = 0x3200000L

.field public static final BUILD_SOURCE:Ljava/lang/String; = "BUILD_SOURCE"

.field public static final BUILD_TYPE:Ljava/lang/String; = "BUILD_TYPE"

.field public static final CHECK_UPDATE_ACTIVITY_REQ:I = 0x100

.field public static final CHECK_UPDATE_SERVICE_REQ:I = 0x80

.field public static final DOWNLOAD_ID:Ljava/lang/String; = "DOWNLOAD_ID"

.field public static final EXTRA_OTA_UPDATE_UI:Ljava/lang/String; = "OTA_UPDATE_UI"

.field public static final EXTRA_PERIODIC_CHECK:Ljava/lang/String; = "PERIODIC_CHECK"

.field public static final EXTRA_WIFI_TRIGGER:Ljava/lang/String; = "WIFI_TRIGGER"

.field public static final FORCE_FULL_UPDATE:Ljava/lang/String; = "FORCE_FULL_UPDATE"

.field private static final INCREMENTAL_INDEX:I = 0x2

.field public static final LAST_CONNECTED_DEVICE_ID:Ljava/lang/String; = "DEVICE_ID"

.field public static final LAST_UPDATE_CHECK:Ljava/lang/String; = "LAST_UPDATE_CHECK"

.field private static final MAJOR_MINOR_INCREMENTAL_LENGTH:I = 0x3

.field public static final PARTIAL_UPLOAD_SIZE:Ljava/lang/String; = "PARTIAL_UPLOAD_SIZE"

.field public static final SW_VERSION:Ljava/lang/String; = "SW_VERSION"

.field public static final SW_VERSION_NAME:Ljava/lang/String; = "SW_VERSION_NAME"

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mCheckingForUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mConfig:Lcom/navdy/client/ota/Config;

.field private mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mIsStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mLastKnownUploadSize:J

.field private volatile mNetworkDownloadIsApproved:Z

.field private mServiceInterface:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

.field private volatile mState:Lcom/navdy/client/ota/OTAUpdateService$State;

.field private mUIClientRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/ota/OTAUpdateUIClient;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

.field private mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

.field private volatile mUserApproved:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/ota/OTAUpdateService;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 76
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mLastKnownUploadSize:J

    .line 79
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 80
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 82
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUserApproved:Z

    .line 86
    iput-boolean v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mNetworkDownloadIsApproved:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mCheckingForUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/ota/OTAUpdateService;Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Lcom/navdy/client/ota/OTAUpdateService$State;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/navdy/client/ota/OTAUpdateService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    return-void
.end method

.method static synthetic access$1202(Lcom/navdy/client/ota/OTAUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUserApproved:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/navdy/client/ota/OTAUpdateService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->discardOldUpdateData()V

    return-void
.end method

.method static synthetic access$1400(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/navdy/client/ota/OTAUpdateService;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Ljava/lang/ref/WeakReference;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUIClientRef:Ljava/lang/ref/WeakReference;

    return-object p1
.end method

.method static synthetic access$1700()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getSWVersionText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/client/ota/OTAUpdateService;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mLastKnownUploadSize:J

    return-wide v0
.end method

.method static synthetic access$200(Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getSWVersion(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$2002(Lcom/navdy/client/ota/OTAUpdateService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mNetworkDownloadIsApproved:Z

    return p1
.end method

.method static synthetic access$300(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->needsFullUpdate(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/navdy/client/ota/OTAUpdateService;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->scheduleNextCheckForUpdate()V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/ota/OTAUpdateService;)Lcom/navdy/client/ota/OTAUpdateManager;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/ota/OTAUpdateService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->checkForUpdate()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/client/ota/OTAUpdateService;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$900(Lcom/navdy/client/ota/OTAUpdateService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/ota/OTAUpdateService;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    return-void
.end method

.method public static bPersistDeviceInfo(Lcom/navdy/service/library/events/DeviceInfo;)V
    .locals 5
    .param p0, "deviceInfo"    # Lcom/navdy/service/library/events/DeviceInfo;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 798
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 799
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 800
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DEVICE_ID"

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    .line 801
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SW_VERSION"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->systemVersion:Ljava/lang/String;

    .line 802
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SW_VERSION_NAME"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->clientVersion:Ljava/lang/String;

    .line 803
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "BUILD_TYPE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->buildType:Ljava/lang/String;

    .line 804
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/navdy/service/library/events/DeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "FORCE_FULL_UPDATE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/navdy/service/library/events/DeviceInfo;->forceFullUpdate:Ljava/lang/Boolean;

    .line 806
    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 805
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 807
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 809
    :cond_0
    return-void
.end method

.method public static bPersistUpdateInfo(Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 6
    .param p0, "updateInfo"    # Lcom/navdy/client/ota/model/UpdateInfo;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 813
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 814
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 815
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_STATUS"

    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 816
    invoke-virtual {v3}, Lcom/navdy/client/ota/OTAUpdateService$State;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_VERSION_NAME"

    iget-object v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    .line 817
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_VERSION"

    iget v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    .line 818
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_URL"

    iget-object v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    .line 819
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_DESCRIPTION"

    iget-object v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->description:Ljava/lang/String;

    .line 820
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_SIZE"

    iget-wide v4, p0, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 821
    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_IS_INCREMENTAL"

    iget-boolean v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    .line 822
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_FROM_VERSION"

    iget v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    .line 823
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "OTA_META_DATA"

    iget-object v3, p0, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 824
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 825
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 827
    :cond_0
    return-void
.end method

.method public static bReadLastDownloadId()I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 779
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 780
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 781
    const-string v2, "DOWNLOAD_ID"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 783
    :cond_0
    return v1
.end method

.method public static bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 830
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 831
    .local v0, "preferences":Landroid/content/SharedPreferences;
    new-instance v1, Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-direct {v1}, Lcom/navdy/client/ota/model/UpdateInfo;-><init>()V

    .line 832
    .local v1, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    const-string v2, "OTA_VERSION"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    .line 833
    const-string v2, "OTA_VERSION_NAME"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->versionName:Ljava/lang/String;

    .line 834
    const-string v2, "OTA_URL"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->url:Ljava/lang/String;

    .line 835
    const-string v2, "OTA_DESCRIPTION"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->description:Ljava/lang/String;

    .line 836
    const-string v2, "OTA_SIZE"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 837
    const-string v2, "OTA_IS_INCREMENTAL"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    .line 838
    const-string v2, "OTA_FROM_VERSION"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    .line 839
    const-string v2, "OTA_META_DATA"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/navdy/client/ota/model/UpdateInfo;->metaData:Ljava/lang/String;

    .line 840
    return-object v1
.end method

.method public static bResetDownloadId()V
    .locals 3

    .prologue
    .line 772
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 773
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 774
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DOWNLOAD_ID"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 776
    :cond_0
    return-void
.end method

.method public static bSaveLastCheckTime()V
    .locals 6

    .prologue
    .line 844
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 845
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "DEVICE_ID"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 846
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "LAST_UPDATE_CHECK"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 847
    return-void
.end method

.method public static bSetDownloadId(I)V
    .locals 3
    .param p0, "id"    # I

    .prologue
    .line 787
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 788
    .local v0, "preferences":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 789
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DOWNLOAD_ID"

    invoke-interface {v1, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 791
    :cond_0
    return-void
.end method

.method private canBeDownloadedInBackground()Z
    .locals 2

    .prologue
    .line 441
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 442
    .local v0, "c":Landroid/content/Context;
    iget-boolean v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mNetworkDownloadIsApproved:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToNetwork(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {v0}, Lcom/navdy/service/library/util/SystemUtils;->isConnectedToWifi(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private checkForUpdate()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 137
    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mCheckingForUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/ota/OTAUpdateService$1;

    invoke-direct {v2, p0}, Lcom/navdy/client/ota/OTAUpdateService$1;-><init>(Lcom/navdy/client/ota/OTAUpdateService;)V

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 176
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private checkPrecondition()V
    .locals 6

    .prologue
    .line 313
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    .line 314
    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    if-eqz v3, :cond_0

    .line 315
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 316
    .local v2, "lastConnectedDeviceId":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 317
    invoke-static {v2}, Lcom/navdy/client/ota/OTAUpdateService;->getSWVersion(Ljava/lang/String;)I

    move-result v0

    .line 319
    .local v0, "deviceSwVersion":I
    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget v3, v3, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    if-lt v0, v3, :cond_0

    .line 320
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Device is already up to date. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 322
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 323
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUpdateFile()Ljava/io/File;

    move-result-object v1

    .line 324
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 325
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 330
    .end local v0    # "deviceSwVersion":I
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "lastConnectedDeviceId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private clearUpdateFiles()V
    .locals 6

    .prologue
    .line 468
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUpdateFile()Ljava/io/File;

    move-result-object v0

    .line 469
    .local v0, "outputFile":Ljava/io/File;
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Clearing the update file as the device is up to date "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " , Size (Bytes) :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 470
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 471
    return-void
.end method

.method private client()Lcom/navdy/client/ota/OTAUpdateUIClient;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUIClientRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 855
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUIClientRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/ota/OTAUpdateUIClient;

    .line 857
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private discardOldUpdateData()V
    .locals 7

    .prologue
    .line 594
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bResetDownloadId()V

    .line 595
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/app/framework/PathManager;->getOtaUpdateFolderPath()Ljava/lang/String;

    move-result-object v3

    .line 596
    .local v3, "updateFolderPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 597
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v4

    if-nez v4, :cond_0

    .line 598
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 599
    .local v1, "children":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 600
    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v1, v4

    .line 601
    .local v0, "child":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 600
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 605
    .end local v0    # "child":Ljava/io/File;
    .end local v1    # "children":[Ljava/io/File;
    :cond_0
    return-void
.end method

.method private downloadStopped(Z)V
    .locals 2
    .param p1, "completed"    # Z

    .prologue
    .line 446
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->stopForeground(Z)V

    .line 447
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->notifyUpdateAvailable()V

    .line 448
    if-nez p1, :cond_0

    .line 449
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 453
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 454
    return-void

    .line 451
    :cond_0
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    goto :goto_0
.end method

.method private ensureSpaceAvailableForDownload(JJ)Z
    .locals 11
    .param p1, "sizeOfTheUpdate"    # J
    .param p3, "partialDownloadSize"    # J

    .prologue
    .line 531
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/PathManager;->getOtaUpdateFolderPath()Ljava/lang/String;

    move-result-object v2

    .line 532
    .local v2, "otaUpdateFolder":Ljava/lang/String;
    new-instance v3, Landroid/os/StatFs;

    invoke-direct {v3, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 533
    .local v3, "statFs":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v8, v8

    mul-long v0, v6, v8

    .line 534
    .local v0, "freeSpace":J
    sub-long v6, p1, p3

    const-wide/32 v8, 0x3200000

    add-long v4, v6, v8

    .line 535
    .local v4, "spaceRequired":J
    sget-object v6, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->isLoggable(I)Z

    move-result v6

    if-nez v6, :cond_0

    cmp-long v6, v0, v4

    if-gtz v6, :cond_1

    .line 536
    :cond_0
    sget-object v7, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "In "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "; Space required: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "; Space free: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " enough space:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    cmp-long v6, v0, v4

    if-lez v6, :cond_2

    const/4 v6, 0x1

    .line 537
    :goto_0
    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 536
    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 539
    :cond_1
    cmp-long v6, v0, v4

    if-lez v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    return v6

    .line 536
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 539
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static getBuildSource()Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 11

    .prologue
    .line 951
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v0

    .line 953
    .local v0, "buildType":Lcom/navdy/client/debug/util/S3Constants$BuildType;
    invoke-static {v0}, Lcom/navdy/client/debug/util/S3Constants;->getSourcesForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v7

    .line 956
    .local v7, "sourcesForBuildType":[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getUserPreferredSourceForTheDevice()Ljava/lang/String;

    move-result-object v4

    .line 957
    .local v4, "preference":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/debug/util/S3Constants;->getDefaultSourceForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v5

    .line 959
    .local v5, "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :try_start_0
    invoke-static {v4}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 963
    :goto_0
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "User preference for the source "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", Source :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 965
    const/4 v3, 0x0

    .line 966
    .local v3, "isSourceInAllowedSourcesForBuildType":Z
    array-length v9, v7

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v9, :cond_0

    aget-object v6, v7, v8

    .line 967
    .local v6, "sourceForBuildType":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    if-ne v6, v5, :cond_1

    .line 968
    const/4 v3, 0x1

    .line 972
    .end local v6    # "sourceForBuildType":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_0
    if-eqz v3, :cond_2

    .line 973
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "User preferred source is compatible with the build type"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 981
    .end local v5    # "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :goto_2
    return-object v5

    .line 960
    .end local v3    # "isSourceInAllowedSourcesForBuildType":Z
    .restart local v5    # "source":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :catch_0
    move-exception v2

    .line 961
    .local v2, "iae":Ljava/lang/IllegalArgumentException;
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error parsing the saved user preference for OTA source "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 966
    .end local v2    # "iae":Ljava/lang/IllegalArgumentException;
    .restart local v3    # "isSourceInAllowedSourcesForBuildType":Z
    .restart local v6    # "sourceForBuildType":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 977
    .end local v6    # "sourceForBuildType":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    :cond_2
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "User preferred source is not compatible with build type, setting to default"

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 979
    invoke-static {v0}, Lcom/navdy/client/debug/util/S3Constants;->getDefaultSourceForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v1

    .line 980
    .local v1, "defaultSourceForBuildType":Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    invoke-static {v5}, Lcom/navdy/client/ota/OTAUpdateService;->persistUserPreferredBuildSource(Lcom/navdy/client/debug/util/S3Constants$BuildSource;)V

    move-object v5, v1

    .line 981
    goto :goto_2
.end method

.method public static getBuildSource(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 1005
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v1

    .line 1006
    .local v1, "buildTypeOfDevice":Lcom/navdy/client/debug/util/S3Constants$BuildType;
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "BUILD_SOURCE"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lcom/navdy/client/debug/util/S3Constants;->getDefaultSourceForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1007
    .local v0, "buildSource":Ljava/lang/String;
    return-object v0
.end method

.method public static getBuildSources()[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    .locals 2

    .prologue
    .line 987
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v0

    .line 989
    .local v0, "buildType":Lcom/navdy/client/debug/util/S3Constants$BuildType;
    invoke-static {v0}, Lcom/navdy/client/debug/util/S3Constants;->getSourcesForBuildType(Lcom/navdy/client/debug/util/S3Constants$BuildType;)[Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    move-result-object v1

    .line 990
    .local v1, "sourcesForBuildType":[Lcom/navdy/client/debug/util/S3Constants$BuildSource;
    return-object v1
.end method

.method public static getBuildType()Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 2

    .prologue
    .line 994
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 995
    .local v1, "lastConnectedDeviceId":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildType(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildType;

    move-result-object v0

    .line 996
    .local v0, "buildType":Lcom/navdy/client/debug/util/S3Constants$BuildType;
    return-object v0
.end method

.method public static getBuildType(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildType;
    .locals 6
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 430
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "BUILD_TYPE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/navdy/client/app/ui/settings/SettingsConstants;->OTA_BUILD_TYPE_DEFAULT:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "buildTypeName":Ljava/lang/String;
    sget-object v0, Lcom/navdy/client/debug/util/S3Constants$BuildType;->user:Lcom/navdy/client/debug/util/S3Constants$BuildType;

    .line 433
    .local v0, "buildType":Lcom/navdy/client/debug/util/S3Constants$BuildType;
    :try_start_0
    invoke-static {v1}, Lcom/navdy/client/debug/util/S3Constants$BuildType;->valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/util/S3Constants$BuildType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 437
    :goto_0
    return-object v0

    .line 434
    :catch_0
    move-exception v2

    .line 435
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot parse the build type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getDeviceId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 381
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 382
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "DEVICE_ID"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    return-object v0
.end method

.method public static getLastDeviceVersionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 415
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->getSWVersionText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSWVersion(Ljava/lang/String;)I
    .locals 7
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 401
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 402
    .local v1, "preferences":Landroid/content/SharedPreferences;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SW_VERSION"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, "swVersionString":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 410
    :goto_0
    return v3

    .line 407
    :cond_0
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "formatException":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error parsing software version, not a number: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getSWVersionText(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 387
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 389
    .local v2, "preferences":Landroid/content/SharedPreferences;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SW_VERSION_NAME"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 391
    .local v0, "fullSWVersion":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 392
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "SW_VERSION_NAME"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 393
    .local v1, "fullSWVersionParts":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v1, v3

    .line 396
    .end local v1    # "fullSWVersionParts":[Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getSWVersion(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getSavedState()Lcom/navdy/client/ota/OTAUpdateService$State;
    .locals 5

    .prologue
    .line 762
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 763
    .local v0, "preferences":Landroid/content/SharedPreferences;
    const-string v3, "OTA_STATUS"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 764
    .local v2, "stateString":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 765
    .local v1, "state":Lcom/navdy/client/ota/OTAUpdateService$State;
    if-eqz v2, :cond_0

    .line 766
    invoke-static {v2}, Lcom/navdy/client/ota/OTAUpdateService$State;->valueOf(Ljava/lang/String;)Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v1

    .line 768
    :cond_0
    return-object v1
.end method

.method public static getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 739
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/ota/OTAUpdateService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 850
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private getUIPendingIntent()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 871
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 872
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 873
    .local v1, "intent":Landroid/content/Intent;
    const/16 v2, 0x100

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    return-object v2
.end method

.method private getUpdateFile()Ljava/io/File;
    .locals 2

    .prologue
    .line 522
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/PathManager;->getOTAUpdateFilePath()Ljava/lang/String;

    move-result-object v0

    .line 523
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public static getUserPreferredSourceForTheDevice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1000
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 1001
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->getBuildSource(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static isLaunchedByOtaUpdateService(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    .line 867
    if-eqz p0, :cond_0

    const-string v1, "OTA_UPDATE_UI"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "OTA_UPDATE_UI"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isUpdateFileValid()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 546
    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/PathManager;->getOTAUpdateFilePath()Ljava/lang/String;

    move-result-object v0

    .line 547
    .local v0, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 548
    .local v1, "updateFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 556
    :goto_0
    return v4

    .line 551
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-wide v2, v5, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    .line 552
    .local v2, "sizeOfTheUpdate":J
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-eqz v5, :cond_1

    .line 553
    sget-object v5, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Downloaded update file is invalid"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 556
    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private static needsFullUpdate(Ljava/lang/String;)Z
    .locals 3
    .param p0, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 419
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 420
    .local v0, "preferences":Landroid/content/SharedPreferences;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "FORCE_FULL_UPDATE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private performUpdate()V
    .locals 3

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->checkPrecondition()V

    .line 334
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$2;->$SwitchMap$com$navdy$client$ota$OTAUpdateService$State:[I

    iget-object v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1}, Lcom/navdy/client/ota/OTAUpdateService$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 364
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown OTA state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 336
    :pswitch_0
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The current sw is up to date"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 337
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->clearUpdateFiles()V

    goto :goto_0

    .line 340
    :pswitch_1
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "There is an update available, that needs to be downloaded"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 341
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    .line 342
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->notifyUpdateAvailable()V

    .line 343
    iget-boolean v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUserApproved:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->canBeDownloadedInBackground()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The update can be downloaded in the background, so start the download"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 345
    iget-boolean v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUserApproved:Z

    invoke-direct {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->startDownload(Z)V

    goto :goto_0

    .line 350
    :pswitch_2
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The update is downloaded and ready to be uploaded to the HUD"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 351
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    .line 352
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->tryUploading()V

    goto :goto_0

    .line 355
    :pswitch_3
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The update has been downloaded to the device and will be installed when the device restarts"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 358
    :pswitch_4
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The update is downloading..."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    :pswitch_5
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "The update is uploading..."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static persistUserPreferredBuildSource(Lcom/navdy/client/debug/util/S3Constants$BuildSource;)V
    .locals 4
    .param p0, "source"    # Lcom/navdy/client/debug/util/S3Constants$BuildSource;

    .prologue
    .line 1011
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 1012
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "BUILD_SOURCE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/navdy/client/debug/util/S3Constants$BuildSource;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1014
    return-void
.end method

.method private saveUploadProgress(J)V
    .locals 3
    .param p1, "partialUploadSize"    # J

    .prologue
    .line 304
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 305
    .local v0, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "PARTIAL_UPLOAD_SIZE"

    invoke-interface {v1, v2, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 306
    return-void
.end method

.method private scheduleNextCheckForUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 370
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Scheduling for next check for update"

    invoke-virtual {v3, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 371
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/navdy/client/ota/OTAUpdateService;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "PERIODIC_CHECK"

    invoke-virtual {v1, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 373
    const/16 v3, 0x80

    const/high16 v6, 0x10000000

    invoke-static {p0, v3, v1, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 374
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mConfig:Lcom/navdy/client/ota/Config;

    iget-wide v4, v3, Lcom/navdy/client/ota/Config;->mUpdateInterval:J

    .line 375
    .local v4, "triggerTimeMillis":J
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 376
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, v4

    invoke-virtual {v0, v8, v6, v7, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 377
    return-void
.end method

.method private setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V
    .locals 5
    .param p1, "state"    # Lcom/navdy/client/ota/OTAUpdateService$State;

    .prologue
    .line 745
    iput-object p1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 746
    if-eqz p1, :cond_0

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq p1, v2, :cond_0

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-eq p1, v2, :cond_0

    .line 747
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 748
    .local v1, "preferences":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 749
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "OTA_STATUS"

    invoke-virtual {p1}, Lcom/navdy/client/ota/OTAUpdateService$State;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 753
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 754
    .local v0, "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    if-eqz v0, :cond_1

    .line 755
    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-interface {v0, p1, v2}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 756
    const/4 v0, 0x0

    .line 758
    :cond_1
    return-void
.end method

.method private startDownload(Z)V
    .locals 12
    .param p1, "userInitiated"    # Z

    .prologue
    .line 476
    iget-object v8, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 477
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService$State;->DOWNLOADING_UPDATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v8}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 478
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadLastDownloadId()I

    move-result v1

    .line 479
    .local v1, "downloadId":I
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUpdateFile()Ljava/io/File;

    move-result-object v4

    .line 482
    .local v4, "outputFile":Ljava/io/File;
    const-wide/16 v6, 0x0

    .line 483
    .local v6, "partiallyDownloadedFileSize":J
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 484
    const/4 v8, -0x1

    if-ne v1, v8, :cond_4

    .line 485
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/navdy/service/library/util/IOUtils;->deleteFile(Landroid/content/Context;Ljava/lang/String;)Z

    .line 490
    :cond_0
    :goto_0
    iget-object v8, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-wide v8, v8, Lcom/navdy/client/ota/model/UpdateInfo;->size:J

    invoke-direct {p0, v8, v9, v6, v7}, Lcom/navdy/client/ota/OTAUpdateService;->ensureSpaceAvailableForDownload(JJ)Z

    move-result v8

    if-nez v8, :cond_1

    .line 491
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 492
    .local v0, "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    if-eqz v0, :cond_1

    .line 493
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->NOT_ENOUGH_SPACE:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    invoke-interface {v0, v8, v10, v11, v9}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V

    .line 494
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    .line 500
    .end local v0    # "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    :cond_1
    const/4 v8, -0x1

    if-ne v1, v8, :cond_2

    .line 501
    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    move-result v5

    .line 502
    .local v5, "worked":Z
    if-nez v5, :cond_2

    .line 503
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to create the output file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    .end local v5    # "worked":Z
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getDownloadingNotification()Landroid/app/Notification;

    move-result-object v3

    .line 516
    .local v3, "foregroundNotification":Landroid/app/Notification;
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v3}, Lcom/navdy/client/ota/OTAUpdateService;->startForeground(ILandroid/app/Notification;)V

    .line 517
    iget-object v8, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

    iget-object v9, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-interface {v8, v9, v4, v1}, Lcom/navdy/client/ota/OTAUpdateManager;->download(Lcom/navdy/client/ota/model/UpdateInfo;Ljava/io/File;I)V

    .line 519
    .end local v1    # "downloadId":I
    .end local v3    # "foregroundNotification":Landroid/app/Notification;
    .end local v4    # "outputFile":Ljava/io/File;
    .end local v6    # "partiallyDownloadedFileSize":J
    :cond_3
    :goto_1
    return-void

    .line 487
    .restart local v1    # "downloadId":I
    .restart local v4    # "outputFile":Ljava/io/File;
    .restart local v6    # "partiallyDownloadedFileSize":J
    :cond_4
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    goto :goto_0

    .line 506
    :catch_0
    move-exception v2

    .line 507
    .local v2, "e":Ljava/io/IOException;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 508
    .restart local v0    # "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    if-eqz v0, :cond_5

    .line 509
    sget-object v8, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    invoke-interface {v0, v8, v10, v11, v9}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V

    .line 510
    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    .line 512
    :cond_5
    const/4 v0, 0x0

    .line 513
    goto :goto_1
.end method

.method public static startService(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 734
    invoke-static {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getServiceIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 735
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 736
    return-void
.end method

.method private tryUploading()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 560
    sget-object v5, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Trying to transfer the update to HUD"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 561
    iget-object v5, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v7, v10}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 562
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 563
    .local v1, "mRemoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 564
    sget-object v5, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Device is connected, initiating transfer of update "

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 565
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->isUpdateFileValid()Z

    move-result v5

    if-nez v5, :cond_1

    .line 566
    invoke-direct {p0, v7}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    .line 567
    sget-object v5, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v5}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 587
    .end local v1    # "mRemoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_0
    :goto_0
    return-void

    .line 570
    .restart local v1    # "mRemoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUpdateFile()Ljava/io/File;

    move-result-object v4

    .line 571
    .local v4, "updateFile":Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "PARTIAL_UPLOAD_SIZE"

    const-wide/16 v8, 0x0

    invoke-interface {v5, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 572
    .local v2, "partialUploadedSize":J
    iput-wide v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mLastKnownUploadSize:J

    .line 573
    sget-object v5, Lcom/navdy/client/ota/OTAUpdateService$State;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v5}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 574
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUploadingNotification()Landroid/app/Notification;

    move-result-object v0

    .line 575
    .local v0, "foregroundNotification":Landroid/app/Notification;
    invoke-virtual {p0, v10, v0}, Lcom/navdy/client/ota/OTAUpdateService;->startForeground(ILandroid/app/Notification;)V

    .line 576
    iget-object v5, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget-boolean v5, v5, Lcom/navdy/client/ota/model/UpdateInfo;->incremental:Z

    if-eqz v5, :cond_2

    .line 577
    iget-object v5, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget v7, v7, Lcom/navdy/client/ota/model/UpdateInfo;->fromVersion:I

    iget-object v8, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget v8, v8, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    invoke-virtual {v6, v7, v8}, Lcom/navdy/client/app/framework/PathManager;->getOtaUpdateFileNameOnHUD(II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v2, v3, v6}, Lcom/navdy/client/ota/OTAUpdateManager;->uploadToHUD(Ljava/io/File;JLjava/lang/String;)V

    goto :goto_0

    .line 579
    :cond_2
    iget-object v5, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

    invoke-static {}, Lcom/navdy/client/app/framework/PathManager;->getInstance()Lcom/navdy/client/app/framework/PathManager;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    iget v7, v7, Lcom/navdy/client/ota/model/UpdateInfo;->version:I

    invoke-virtual {v6, v7}, Lcom/navdy/client/app/framework/PathManager;->getOtaUpdateFileNameOnHUD(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v2, v3, v6}, Lcom/navdy/client/ota/OTAUpdateManager;->uploadToHUD(Ljava/io/File;JLjava/lang/String;)V

    goto :goto_0

    .line 583
    .end local v0    # "foregroundNotification":Landroid/app/Notification;
    .end local v2    # "partialUploadedSize":J
    .end local v4    # "updateFile":Ljava/io/File;
    :cond_3
    invoke-direct {p0, v7}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    goto :goto_0
.end method

.method private uploadStopped(Z)V
    .locals 2
    .param p1, "completed"    # Z

    .prologue
    .line 457
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->stopForeground(Z)V

    .line 458
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->removeNotification()V

    .line 459
    if-eqz p1, :cond_0

    .line 460
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_INSTALL:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 464
    :goto_0
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 465
    return-void

    .line 462
    :cond_0
    sget-object v0, Lcom/navdy/client/ota/OTAUpdateService$State;->READY_TO_UPLOAD:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v0}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    goto :goto_0
.end method


# virtual methods
.method public getDownloadingNotification()Landroid/app/Notification;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 895
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 896
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUIPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    .line 897
    .local v1, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 898
    const v2, 0x7f0802e4

    invoke-virtual {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 899
    const v2, 0x7f0201c7

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 900
    const v2, 0x7f08013f

    invoke-virtual {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 901
    const/4 v2, 0x1

    invoke-virtual {v0, v3, v3, v2}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 902
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public getUploadingNotification()Landroid/app/Notification;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 934
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 935
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUIPendingIntent()Landroid/app/PendingIntent;

    move-result-object v1

    .line 936
    .local v1, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 937
    const v2, 0x7f0802e4

    invoke-virtual {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 938
    const v2, 0x7f0201c7

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 939
    const v2, 0x7f0804c3

    invoke-virtual {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 940
    const/4 v2, 0x1

    invoke-virtual {v0, v3, v3, v2}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 941
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    return-object v2
.end method

.method public notifyUpdateAvailable()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 877
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 878
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUIPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 879
    .local v3, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 880
    const v4, 0x7f0802e4

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 881
    const v4, 0x7f0201c7

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 882
    const v4, 0x7f0804e0

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 883
    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 884
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 885
    .local v2, "notification":Landroid/app/Notification;
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 886
    .local v1, "manager":Landroid/app/NotificationManager;
    invoke-virtual {v1, v5, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 887
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/ota/OTAUpdateService;->startService(Landroid/content/Context;)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mServiceInterface:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    return-object v0
.end method

.method public onCheckForUpdateFinished(Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;Lcom/navdy/client/ota/model/UpdateInfo;)V
    .locals 5
    .param p1, "result"    # Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;
    .param p2, "info"    # Lcom/navdy/client/ota/model/UpdateInfo;

    .prologue
    .line 181
    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mCheckingForUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 182
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 183
    .local v0, "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    if-eq p1, v2, :cond_3

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    if-eq p1, v2, :cond_3

    .line 185
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bSaveLastCheckTime()V

    .line 192
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->AVAILABLE:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    if-ne p1, v2, :cond_6

    .line 194
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bReadUpdateInfo()Lcom/navdy/client/ota/model/UpdateInfo;

    move-result-object v1

    .line 197
    .local v1, "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Lcom/navdy/client/ota/model/UpdateInfo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService$State;->UP_TO_DATE:Lcom/navdy/client/ota/OTAUpdateService$State;

    if-ne v2, v3, :cond_5

    .line 198
    :cond_0
    invoke-static {p2}, Lcom/navdy/client/ota/OTAUpdateService;->bPersistUpdateInfo(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 199
    iput-object p2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    .line 200
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateService$State;->UPDATE_AVAILABLE:Lcom/navdy/client/ota/OTAUpdateService$State;

    invoke-direct {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->setState(Lcom/navdy/client/ota/OTAUpdateService$State;)V

    .line 201
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->notifyUpdateAvailable()V

    .line 202
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->discardOldUpdateData()V

    .line 216
    .end local v1    # "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 217
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->performUpdate()V

    .line 219
    .end local v0    # "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    :cond_2
    :goto_1
    return-void

    .line 187
    .restart local v0    # "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    :cond_3
    if-eqz v0, :cond_2

    .line 188
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    if-ne p1, v2, :cond_4

    sget-object v2, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->NO_CONNECTIVITY:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    :goto_2
    invoke-interface {v0, v2}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onErrorCheckingForUpdate(Lcom/navdy/client/ota/OTAUpdateUIClient$Error;)V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateUIClient$Error;->SERVER_ERROR:Lcom/navdy/client/ota/OTAUpdateUIClient$Error;

    goto :goto_2

    .line 205
    .restart local v1    # "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_5
    if-eqz v0, :cond_1

    .line 206
    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-interface {v0, v2, v3}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    goto :goto_0

    .line 209
    .end local v1    # "updateInfo":Lcom/navdy/client/ota/model/UpdateInfo;
    :cond_6
    sget-object v2, Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;->UPTODATE:Lcom/navdy/client/ota/OTAUpdateListener$CheckOTAUpdateResult;

    if-ne p1, v2, :cond_1

    .line 210
    invoke-static {p2}, Lcom/navdy/client/ota/OTAUpdateService;->bPersistUpdateInfo(Lcom/navdy/client/ota/model/UpdateInfo;)V

    .line 211
    iput-object p2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    .line 212
    if-eqz v0, :cond_1

    .line 213
    iget-object v2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateInfo:Lcom/navdy/client/ota/model/UpdateInfo;

    invoke-interface {v0, v2, v3}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onStateChanged(Lcom/navdy/client/ota/OTAUpdateService$State;Lcom/navdy/client/ota/model/UpdateInfo;)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 93
    new-instance v0, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;

    invoke-direct {v0, p0, p0}, Lcom/navdy/client/ota/impl/OTAUpdateManagerImpl;-><init>(Landroid/content/Context;Lcom/navdy/client/ota/OTAUpdateListener;)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUpdateManager:Lcom/navdy/client/ota/OTAUpdateManager;

    .line 94
    new-instance v0, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;

    invoke-direct {v0, p0}, Lcom/navdy/client/ota/OTAUpdateService$OTAUpdateServiceInterfaceImpl;-><init>(Lcom/navdy/client/ota/OTAUpdateService;)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mServiceInterface:Lcom/navdy/client/ota/OTAUpdateServiceInterface;

    .line 95
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mCheckingForUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 96
    new-instance v0, Lcom/navdy/client/ota/Config;

    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/client/ota/Config;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/navdy/client/ota/OTAUpdateService;->mConfig:Lcom/navdy/client/ota/Config;

    .line 97
    return-void
.end method

.method public onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;IJJ)V
    .locals 7
    .param p1, "status"    # Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;
    .param p2, "downloadId"    # I
    .param p3, "completedSoFar"    # J
    .param p5, "total"    # J

    .prologue
    .line 223
    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    if-nez v3, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 226
    :cond_1
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->STARTED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-ne p1, v3, :cond_2

    .line 227
    invoke-static {p2}, Lcom/navdy/client/ota/OTAUpdateService;->bSetDownloadId(I)V

    goto :goto_0

    .line 230
    :cond_2
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->PAUSED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-ne p1, v3, :cond_3

    .line 231
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    goto :goto_0

    .line 234
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 235
    .local v0, "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    const-wide/16 v4, 0x0

    cmp-long v3, p5, v4

    if-lez v3, :cond_6

    long-to-float v3, p3

    long-to-float v4, p5

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 236
    .local v1, "percentage":I
    :goto_1
    if-eqz v0, :cond_4

    .line 238
    int-to-byte v3, v1

    :try_start_0
    invoke-interface {v0, p1, p3, p4, v3}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_4
    :goto_2
    const/4 v0, 0x0

    .line 244
    if-lez v1, :cond_5

    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-ne p1, v3, :cond_5

    .line 245
    invoke-virtual {p0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->publishDownloadProgress(I)V

    .line 247
    :cond_5
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOAD_FAILED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-ne p1, v3, :cond_7

    .line 248
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Download failed"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 249
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bResetDownloadId()V

    .line 250
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    goto :goto_0

    .line 235
    .end local v1    # "percentage":I
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 239
    .restart local v1    # "percentage":I
    :catch_0
    move-exception v2

    .line 240
    .local v2, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 253
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_7
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->COMPLETED:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-ne p1, v3, :cond_8

    .line 254
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Download complete"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 256
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/navdy/client/ota/OTAUpdateService;->mLastKnownUploadSize:J

    .line 257
    const-wide/16 v4, 0x0

    invoke-direct {p0, v4, v5}, Lcom/navdy/client/ota/OTAUpdateService;->saveUploadProgress(J)V

    .line 258
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->bResetDownloadId()V

    .line 259
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    .line 262
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->checkForUpdate()Z

    goto/16 :goto_0

    .line 265
    :cond_8
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->DOWNLOADING:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    if-eq p1, v3, :cond_0

    .line 266
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/navdy/client/ota/OTAUpdateService;->downloadStopped(Z)V

    goto/16 :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x1

    .line 102
    iget-object v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 105
    if-eqz p1, :cond_1

    const-string v1, "PERIODIC_CHECK"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WIFI_TRIGGER"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    :cond_0
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "Setting mUserApproved to true due to periodic check or wi-fi reachability"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 107
    iput-boolean v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mUserApproved:Z

    .line 110
    :cond_1
    invoke-static {}, Lcom/navdy/client/ota/OTAUpdateService;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "lastConnectedDeviceId":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 113
    invoke-virtual {p0}, Lcom/navdy/client/ota/OTAUpdateService;->stopSelf()V

    .line 114
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    .line 125
    :goto_0
    return v1

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    :cond_3
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStartCommand: not processing the command as the service is in transient state Downloading :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsDownloadingUpdate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Uploading :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/ota/OTAUpdateService;->mIsUploadingUpdateToHUD:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 118
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0

    .line 121
    :cond_4
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getSavedState()Lcom/navdy/client/ota/OTAUpdateService$State;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/ota/OTAUpdateService;->mState:Lcom/navdy/client/ota/OTAUpdateService$State;

    .line 122
    sget-object v1, Lcom/navdy/client/ota/OTAUpdateService;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onStart: Need to check for update again"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 124
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->checkForUpdate()Z

    .line 125
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    goto :goto_0
.end method

.method public onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JJLcom/navdy/service/library/events/file/FileTransferError;)V
    .locals 6
    .param p1, "status"    # Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;
    .param p2, "completedSoFar"    # J
    .param p4, "total"    # J
    .param p6, "error"    # Lcom/navdy/service/library/events/file/FileTransferError;

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->client()Lcom/navdy/client/ota/OTAUpdateUIClient;

    move-result-object v0

    .line 273
    .local v0, "client":Lcom/navdy/client/ota/OTAUpdateUIClient;
    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-lez v3, :cond_2

    long-to-float v3, p2

    long-to-float v4, p4

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 274
    .local v1, "percentage":I
    :goto_0
    if-eqz v0, :cond_0

    .line 275
    int-to-byte v3, v1

    invoke-interface {v0, p1, p2, p3, v3}, Lcom/navdy/client/ota/OTAUpdateUIClient;->onUploadProgress(Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;JB)V

    .line 277
    :cond_0
    if-lez v1, :cond_1

    sget-object v3, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->UPLOADING:Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;

    if-ne p1, v3, :cond_1

    .line 278
    iput-wide p2, p0, Lcom/navdy/client/ota/OTAUpdateService;->mLastKnownUploadSize:J

    .line 279
    invoke-direct {p0, p2, p3}, Lcom/navdy/client/ota/OTAUpdateService;->saveUploadProgress(J)V

    .line 280
    invoke-virtual {p0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->publishUploadProgress(I)V

    .line 282
    :cond_1
    sget-object v3, Lcom/navdy/client/ota/OTAUpdateService$2;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$UploadToHUDStatus:[I

    invoke-virtual {p1}, Lcom/navdy/client/ota/OTAUpdateListener$UploadToHUDStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 294
    invoke-direct {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    .line 296
    :goto_1
    :pswitch_0
    return-void

    .end local v1    # "percentage":I
    :cond_2
    move v1, v2

    .line 273
    goto :goto_0

    .line 284
    .restart local v1    # "percentage":I
    :pswitch_1
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    goto :goto_1

    .line 288
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/navdy/client/ota/OTAUpdateService;->uploadStopped(Z)V

    goto :goto_1

    .line 282
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public publishDownloadProgress(I)V
    .locals 7
    .param p1, "percentage"    # I

    .prologue
    const/4 v6, 0x1

    .line 906
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 907
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUIPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 908
    .local v3, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 909
    const v4, 0x7f0802e4

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 910
    const v4, 0x7f0201c7

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 911
    const v4, 0x7f08013f

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 912
    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 913
    const/16 v4, 0x64

    const/4 v5, 0x0

    invoke-virtual {v0, v4, p1, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 914
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 915
    .local v2, "notification":Landroid/app/Notification;
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 916
    .local v1, "manager":Landroid/app/NotificationManager;
    invoke-virtual {v1, v6, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 917
    return-void
.end method

.method public publishUploadProgress(I)V
    .locals 7
    .param p1, "percentage"    # I

    .prologue
    const/4 v6, 0x1

    .line 920
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 921
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-direct {p0}, Lcom/navdy/client/ota/OTAUpdateService;->getUIPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 922
    .local v3, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 923
    const v4, 0x7f0802e4

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 924
    const v4, 0x7f0201c7

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 925
    const v4, 0x7f0804c3

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 926
    const/16 v4, 0x64

    const/4 v5, 0x0

    invoke-virtual {v0, v4, p1, v5}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 927
    invoke-virtual {v0, v6}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 928
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 929
    .local v2, "notification":Landroid/app/Notification;
    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/navdy/client/ota/OTAUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 930
    .local v1, "manager":Landroid/app/NotificationManager;
    invoke-virtual {v1, v6, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 931
    return-void
.end method

.method public removeNotification()V
    .locals 2

    .prologue
    .line 890
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/navdy/client/ota/OTAUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 891
    .local v0, "manager":Landroid/app/NotificationManager;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 892
    return-void
.end method
