.class public Lcom/zendesk/service/RetrofitErrorResponse;
.super Ljava/lang/Object;
.source "RetrofitErrorResponse.java"

# interfaces
.implements Lcom/zendesk/service/ErrorResponse;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "RetrofitErrorResponse"


# instance fields
.field private mResponse:Lretrofit2/Response;

.field private mThrowable:Ljava/lang/Throwable;


# direct methods
.method private constructor <init>(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    .line 35
    return-void
.end method

.method private constructor <init>(Lretrofit2/Response;)V
    .locals 0
    .param p1, "response"    # Lretrofit2/Response;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    .line 48
    return-void
.end method

.method public static response(Lretrofit2/Response;)Lcom/zendesk/service/RetrofitErrorResponse;
    .locals 1
    .param p0, "response"    # Lretrofit2/Response;

    .prologue
    .line 43
    new-instance v0, Lcom/zendesk/service/RetrofitErrorResponse;

    invoke-direct {v0, p0}, Lcom/zendesk/service/RetrofitErrorResponse;-><init>(Lretrofit2/Response;)V

    return-object v0
.end method

.method public static throwable(Ljava/lang/Throwable;)Lcom/zendesk/service/RetrofitErrorResponse;
    .locals 1
    .param p0, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 30
    new-instance v0, Lcom/zendesk/service/RetrofitErrorResponse;

    invoke-direct {v0, p0}, Lcom/zendesk/service/RetrofitErrorResponse;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public getReason()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 82
    :goto_0
    return-object v1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 74
    .local v0, "reasonBuilder":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v1}, Lretrofit2/Response;->message()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v1}, Lretrofit2/Response;->message()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v1}, Lretrofit2/Response;->code()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public getResponseBody()Ljava/lang/String;
    .locals 5

    .prologue
    .line 103
    const-string v1, ""

    .line 105
    .local v1, "responseBody":Ljava/lang/String;
    iget-object v3, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v3}, Lretrofit2/Response;->errorBody()Lokhttp3/ResponseBody;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 107
    :try_start_0
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v3}, Lretrofit2/Response;->errorBody()Lokhttp3/ResponseBody;

    move-result-object v3

    invoke-virtual {v3}, Lokhttp3/ResponseBody;->bytes()[B

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-direct {v2, v3, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .end local v1    # "responseBody":Ljava/lang/String;
    .local v2, "responseBody":Ljava/lang/String;
    move-object v1, v2

    .line 115
    .end local v2    # "responseBody":Ljava/lang/String;
    .restart local v1    # "responseBody":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v3, Ljava/lang/AssertionError;

    const-string v4, "UTF-8 must be supported"

    invoke-direct {v3, v4}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    .line 110
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method public getResponseBodyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->errorBody()Lokhttp3/ResponseBody;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->errorBody()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentType()Lokhttp3/MediaType;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/MediaType;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getResponseHeaders()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/service/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v4, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    if-eqz v4, :cond_1

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 143
    :cond_0
    return-object v1

    .line 134
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v1, "headers":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/service/Header;>;"
    iget-object v4, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v4}, Lretrofit2/Response;->headers()Lokhttp3/Headers;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v4}, Lretrofit2/Response;->headers()Lokhttp3/Headers;

    move-result-object v4

    invoke-virtual {v4}, Lokhttp3/Headers;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 137
    iget-object v4, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v4}, Lretrofit2/Response;->headers()Lokhttp3/Headers;

    move-result-object v3

    .line 138
    .local v3, "responseHeaders":Lokhttp3/Headers;
    invoke-virtual {v3}, Lokhttp3/Headers;->names()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 139
    .local v0, "headerName":Ljava/lang/String;
    new-instance v4, Lcom/zendesk/service/Header;

    invoke-virtual {v3, v0}, Lokhttp3/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lcom/zendesk/service/Header;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->code()I

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->raw()Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->raw()Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->raw()Lokhttp3/Response;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isConversionError()Z
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/zendesk/service/RetrofitErrorResponse;->isNetworkError()Z

    move-result v0

    return v0
.end method

.method public isHTTPError()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mResponse:Lretrofit2/Response;

    invoke-virtual {v0}, Lretrofit2/Response;->isSuccessful()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNetworkError()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/zendesk/service/RetrofitErrorResponse;->mThrowable:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
