.class Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskPushRegistrationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->registerDeviceWithIdentifier(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$identifier:Ljava/lang/String;

.field final synthetic val$locale:Ljava/util/Locale;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$identifier:Ljava/lang/String;

    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$locale:Ljava/util/Locale;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 6
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAuthenticationType()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v0

    .line 49
    .local v0, "authenticationType":Lcom/zendesk/sdk/model/access/AuthenticationType;
    if-nez v0, :cond_1

    .line 50
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v2, :cond_0

    .line 51
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    new-instance v3, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v4, "Authentication type is null."

    invoke-direct {v3, v4}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$identifier:Ljava/lang/String;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$locale:Ljava/util/Locale;

    sget-object v5, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->Identifier:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->getPushRegistrationRequest(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;)Lcom/zendesk/sdk/model/push/PushRegistrationRequest;

    move-result-object v1

    .line 57
    .local v1, "request":Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {v2, v3, v1, v4}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->internalRegister(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
