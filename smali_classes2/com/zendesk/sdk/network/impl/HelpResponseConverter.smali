.class Lcom/zendesk/sdk/network/impl/HelpResponseConverter;
.super Ljava/lang/Object;
.source "HelpResponseConverter.java"


# instance fields
.field private helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;)V
    .locals 0
    .param p1, "helpResponse"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    .line 35
    return-void
.end method

.method private convertByArticles(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;>;"
    if-eqz p1, :cond_0

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 126
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private convertByCategories(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 9
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            ">;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "categorySectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;>;"
    .local p2, "sectionArticlesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    .line 71
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getCategories()Ljava/util/List;

    move-result-object v6

    invoke-static {v6}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 73
    .local v0, "categories":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;>;"
    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v6

    .line 74
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v7

    add-int/2addr v6, v7

    .line 75
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    add-int v5, v6, v7

    .line 77
    .local v5, "startingListSize":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    .local v2, "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;

    .line 80
    .local v1, "category":Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->getId()Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-static {v6}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 81
    .local v4, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    invoke-virtual {v1, v4}, Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;->setSections(Ljava/util/List;)V

    .line 82
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .line 84
    .local v3, "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getId()Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-direct {p0, v3, v6}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->convertSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 88
    .end local v1    # "category":Lcom/zendesk/sdk/model/helpcenter/help/CategoryItem;
    .end local v3    # "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    .end local v4    # "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    :cond_1
    return-object v2
.end method

.method private convertBySections(Ljava/util/Map;)Ljava/util/List;
    .locals 6
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "sectionArticlesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->ensureEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 96
    .local v2, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    add-int v3, v4, v5

    .line 98
    .local v3, "startingListSize":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    .local v0, "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .line 100
    .local v1, "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getId()Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-direct {p0, v1, v4}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->convertSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 103
    .end local v1    # "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    :cond_0
    return-object v0
.end method

.method private convertSection(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p1, "sectionItem"    # Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    .local p2, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    invoke-static {p2}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 110
    .local v1, "sectionArticles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getTotalArticlesCount()I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 111
    new-instance v2, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-direct {v2, p1}, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;-><init>(Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_0
    invoke-virtual {p1, v1}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->addChildren(Ljava/util/List;)V

    .line 115
    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 116
    .local v0, "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    const/4 v2, 0x0

    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 118
    return-object v0
.end method


# virtual methods
.method convert()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    if-nez v3, :cond_0

    .line 46
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 63
    :goto_0
    return-object v1

    .line 49
    :cond_0
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->getSectionArticlesMap(Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;)Ljava/util/Map;

    move-result-object v2

    .line 50
    .local v2, "sectionArticlesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {p0, v3}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->getCategorySectionsMap(Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;)Ljava/util/Map;

    move-result-object v0

    .line 54
    .local v0, "categorySectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;>;"
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getCategories()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    .line 55
    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    invoke-direct {p0, v0, v2}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->convertByCategories(Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .local v1, "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    goto :goto_0

    .line 57
    .end local v1    # "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    :cond_1
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 58
    invoke-direct {p0, v2}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->convertBySections(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    .restart local v1    # "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    goto :goto_0

    .line 60
    .end local v1    # "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    :cond_2
    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->helpResponse:Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;

    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getArticles()Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/zendesk/sdk/network/impl/HelpResponseConverter;->convertByArticles(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .restart local v1    # "helpItems":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    goto :goto_0
.end method

.method getCategorySectionsMap(Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;)Ljava/util/Map;
    .locals 6
    .param p1, "helpResponse"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getCategories()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 160
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 161
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    .line 179
    :cond_1
    return-object v1

    .line 164
    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getCategories()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 166
    .local v1, "categorySectionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;>;"
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;

    .line 167
    .local v2, "section":Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;->getParentId()Ljava/lang/Long;

    move-result-object v0

    .line 169
    .local v0, "categoryId":Ljava/lang/Long;
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 170
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object v3, v4

    .line 173
    .local v3, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    :goto_1
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 174
    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    :cond_3
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    .end local v3    # "sections":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/SectionItem;>;"
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method getSectionArticlesMap(Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;)Ljava/util/Map;
    .locals 6
    .param p1, "helpResponse"    # Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 134
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getArticles()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/util/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 135
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    .line 153
    :cond_1
    return-object v2

    .line 138
    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getSections()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 140
    .local v2, "sectionArticlesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;>;"
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/help/HelpResponse;->getArticles()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;

    .line 141
    .local v0, "article":Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/help/ArticleItem;->getParentId()Ljava/lang/Long;

    move-result-object v3

    .line 143
    .local v3, "sectionId":Ljava/lang/Long;
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 144
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object v1, v4

    .line 147
    .local v1, "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    :goto_1
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 148
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    :cond_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 144
    .end local v1    # "articles":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;>;"
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method
