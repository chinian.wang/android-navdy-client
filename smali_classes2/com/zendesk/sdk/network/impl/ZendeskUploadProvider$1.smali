.class Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUploadProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->uploadAttachment(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$mimeType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$fileName:Ljava/lang/String;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$file:Ljava/io/File;

    iput-object p5, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$mimeType:Ljava/lang/String;

    iput-object p6, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 7
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUploadService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$fileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$file:Ljava/io/File;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$mimeType:Ljava/lang/String;

    new-instance v5, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1$1;

    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v5, p0, v6}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskUploadService;->uploadAttachment(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 56
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUploadProvider$1;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
