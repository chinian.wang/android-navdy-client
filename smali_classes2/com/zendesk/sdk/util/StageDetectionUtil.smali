.class public Lcom/zendesk/sdk/util/StageDetectionUtil;
.super Ljava/lang/Object;
.source "StageDetectionUtil.java"


# static fields
.field private static final DEBUG_DN:Ljavax/security/auth/x500/X500Principal;

.field private static final LOG_TAG:Ljava/lang/String; = "StageDetectionUtil"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljavax/security/auth/x500/X500Principal;

    const-string v1, "CN=Android Debug,O=Android,C=US"

    invoke-direct {v0, v1}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/util/StageDetectionUtil;->DEBUG_DN:Ljavax/security/auth/x500/X500Principal;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    return-void
.end method

.method private static checkBuildFlag(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    const/4 v1, 0x0

    .line 187
    .local v1, "debuggable":Z
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".BuildConfig"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 188
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "DEBUG"

    invoke-virtual {v0, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 189
    .local v3, "field":Ljava/lang/reflect/Field;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 196
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "field":Ljava/lang/reflect/Field;
    :goto_0
    return v1

    .line 191
    :catch_0
    move-exception v2

    .line 192
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "StageDetectionUtil"

    const-string v5, "Error, not able to receive \'BuildConfig.DEBUG\'"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v2, v6}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static checkDebugCertificate(Landroid/content/Context;)Z
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v11, 0x0

    .line 131
    const/4 v4, 0x0

    .line 136
    .local v4, "debuggable":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x40

    invoke-virtual {v10, v12, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 137
    .local v6, "pinfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 138
    .local v8, "signatures":[Landroid/content/pm/Signature;
    const-string v10, "X.509"

    invoke-static {v10}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 140
    .local v3, "cf":Ljava/security/cert/CertificateFactory;
    if-eqz v8, :cond_0

    .line 141
    array-length v12, v8

    move v10, v11

    :goto_0
    if-ge v10, v12, :cond_0

    aget-object v7, v8, v10

    .line 142
    .local v7, "signature":Landroid/content/pm/Signature;
    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 144
    .local v9, "stream":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v3, v9}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    .line 145
    .local v2, "certificate":Ljava/security/cert/Certificate;
    instance-of v13, v2, Ljava/security/cert/X509Certificate;

    if-eqz v13, :cond_1

    .line 146
    move-object v0, v2

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v1, v0

    .line 147
    .local v1, "cert":Ljava/security/cert/X509Certificate;
    sget-object v13, Lcom/zendesk/sdk/util/StageDetectionUtil;->DEBUG_DN:Ljavax/security/auth/x500/X500Principal;

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 149
    if-eqz v4, :cond_1

    .line 161
    .end local v1    # "cert":Ljava/security/cert/X509Certificate;
    .end local v2    # "certificate":Ljava/security/cert/Certificate;
    .end local v3    # "cf":Ljava/security/cert/CertificateFactory;
    .end local v6    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "signature":Landroid/content/pm/Signature;
    .end local v8    # "signatures":[Landroid/content/pm/Signature;
    .end local v9    # "stream":Ljava/io/ByteArrayInputStream;
    :cond_0
    :goto_1
    return v4

    .line 141
    .restart local v2    # "certificate":Ljava/security/cert/Certificate;
    .restart local v3    # "cf":Ljava/security/cert/CertificateFactory;
    .restart local v6    # "pinfo":Landroid/content/pm/PackageInfo;
    .restart local v7    # "signature":Landroid/content/pm/Signature;
    .restart local v8    # "signatures":[Landroid/content/pm/Signature;
    .restart local v9    # "stream":Ljava/io/ByteArrayInputStream;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 156
    .end local v2    # "certificate":Ljava/security/cert/Certificate;
    .end local v3    # "cf":Ljava/security/cert/CertificateFactory;
    .end local v6    # "pinfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "signature":Landroid/content/pm/Signature;
    .end local v8    # "signatures":[Landroid/content/pm/Signature;
    .end local v9    # "stream":Ljava/io/ByteArrayInputStream;
    :catch_0
    move-exception v5

    .line 157
    .local v5, "e":Ljava/lang/Exception;
    :goto_2
    const-string v10, "StageDetectionUtil"

    const-string v12, "Error, not able to read the certificate"

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v10, v12, v5, v11}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_1

    .line 156
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    goto :goto_2
.end method

.method private static checkDebuggableFlag(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkLogging()Z
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/zendesk/logger/Logger;->isLoggable()Z

    move-result v0

    return v0
.end method

.method public static isDebug(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 44
    if-nez p0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v2

    .line 48
    :cond_1
    invoke-static {p0}, Lcom/zendesk/sdk/util/StageDetectionUtil;->isDebugInternal(Landroid/content/Context;)Z

    move-result v0

    .line 49
    .local v0, "checks":Z
    invoke-static {}, Lcom/zendesk/sdk/util/StageDetectionUtil;->checkLogging()Z

    move-result v1

    .line 51
    .local v1, "logging":Z
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isDebugInternal(Landroid/content/Context;)Z
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 83
    invoke-static {p0}, Lcom/zendesk/sdk/util/StageDetectionUtil;->checkDebuggableFlag(Landroid/content/Context;)Z

    move-result v2

    .line 84
    .local v2, "debugFlag":Z
    invoke-static {p0}, Lcom/zendesk/sdk/util/StageDetectionUtil;->checkDebugCertificate(Landroid/content/Context;)Z

    move-result v1

    .line 85
    .local v1, "debugCertificate":Z
    invoke-static {p0}, Lcom/zendesk/sdk/util/StageDetectionUtil;->checkBuildFlag(Landroid/content/Context;)Z

    move-result v0

    .line 87
    .local v0, "buildFlag":Z
    const-string v7, "StageDetectionUtil"

    const-string v8, "Debug flag: %s | Debug certificate: %s | Build flag: %s"

    new-array v9, v12, [Ljava/lang/Object;

    .line 90
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v6

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v11

    .line 87
    invoke-static {v7, v8, v9}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    new-array v7, v12, [Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 94
    .local v4, "isDebug":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Boolean;>;"
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v4, v7}, Ljava/util/Collections;->frequency(Ljava/util/Collection;Ljava/lang/Object;)I

    move-result v3

    .line 97
    .local v3, "frequency":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-lt v3, v7, :cond_0

    :goto_0
    return v5

    :cond_0
    move v5, v6

    goto :goto_0
.end method

.method public static isProduction(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    invoke-static {p0}, Lcom/zendesk/sdk/util/StageDetectionUtil;->isDebug(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
