.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;
.super Ljava/lang/Object;
.source "HelpRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;

.field final synthetic val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;

    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;->val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 291
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;

    iget-object v0, v0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$000(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    iget-object v2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;->val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-interface {v2}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getId()Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$ArticleViewHolder$1;->val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    invoke-interface {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->startActivity(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;)V

    .line 292
    return-void
.end method
