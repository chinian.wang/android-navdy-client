.class Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;
.super Ljava/lang/Object;
.source "HelpRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

.field final synthetic val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

.field final synthetic val$seeAllArticlesItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    .prologue
    .line 341
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    iput-object p3, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->val$seeAllArticlesItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 344
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    iget-object v0, v0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->textView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->access$700(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;

    iget-object v0, v0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder;->this$0:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-static {v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->access$200(Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;)Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;

    move-result-object v1

    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->val$item:Lcom/zendesk/sdk/model/helpcenter/help/HelpItem;

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    invoke-interface {v1, v0}, Lcom/zendesk/sdk/support/help/HelpMvp$Presenter;->onSeeAllClick(Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;)V

    .line 347
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter$SeeAllViewHolder$1;->val$seeAllArticlesItem:Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/help/SeeAllArticlesItem;->setLoading(Z)V

    .line 348
    return-void
.end method
