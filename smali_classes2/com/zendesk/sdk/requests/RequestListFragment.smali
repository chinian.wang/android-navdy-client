.class public Lcom/zendesk/sdk/requests/RequestListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "RequestListFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/network/NetworkAware;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;
    }
.end annotation


# static fields
.field private static final FILTER_BY_STATUSES:Ljava/lang/String; = "new,open,pending,hold,solved"

.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private mEmptyView:Landroid/view/View;

.field private mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

.field private mProgressView:Landroid/view/View;

.field private mRequestsCallback:Lcom/zendesk/service/SafeZendeskCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/zendesk/service/SafeZendeskCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/Request;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRetryable:Lcom/zendesk/sdk/network/Retryable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 65
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/requests/RequestListFragment;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestListFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/requests/RequestListFragment;Lcom/zendesk/sdk/ui/LoadingState;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestListFragment;
    .param p1, "x1"    # Lcom/zendesk/sdk/ui/LoadingState;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/requests/RequestListFragment;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/requests/RequestListFragment;)Lcom/zendesk/sdk/network/RequestLoadingListener;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestListFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V
    .locals 4
    .param p1, "loadingState"    # Lcom/zendesk/sdk/ui/LoadingState;

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 187
    if-nez p1, :cond_1

    .line 188
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "LoadingState was null, nothing to do"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_2

    .line 193
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Activity is not attached, nothing to do"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 197
    :cond_2
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment$2;->$SwitchMap$com$zendesk$sdk$ui$LoadingState:[I

    invoke-virtual {p1}, Lcom/zendesk/sdk/ui/LoadingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 200
    :pswitch_0
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 201
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mProgressView:Landroid/view/View;

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 202
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mEmptyView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 204
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/Retryable;->onRetryUnavailable()V

    goto :goto_0

    .line 211
    :pswitch_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 212
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mProgressView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 213
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mEmptyView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 215
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/Retryable;->onRetryUnavailable()V

    goto :goto_0

    .line 222
    :pswitch_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 223
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mProgressView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 224
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mEmptyView:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/zendesk/sdk/util/UiUtils;->setVisibility(Landroid/view/View;I)V

    .line 226
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    sget v1, Lcom/zendesk/sdk/R$string;->request_list_fragment_error_message:I

    invoke-virtual {p0, v1}, Lcom/zendesk/sdk/requests/RequestListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/zendesk/sdk/requests/RequestListFragment$1;

    invoke-direct {v2, p0}, Lcom/zendesk/sdk/requests/RequestListFragment$1;-><init>(Lcom/zendesk/sdk/requests/RequestListFragment;)V

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/network/Retryable;->onRetryAvailable(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setupCallback()V
    .locals 1

    .prologue
    .line 241
    new-instance v0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;-><init>(Lcom/zendesk/sdk/requests/RequestListFragment;)V

    invoke-static {v0}, Lcom/zendesk/service/SafeZendeskCallback;->from(Lcom/zendesk/service/ZendeskCallback;)Lcom/zendesk/service/SafeZendeskCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRequestsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 243
    return-void
.end method

.method private teardownCallback()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRequestsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-virtual {v0}, Lcom/zendesk/service/SafeZendeskCallback;->cancel()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRequestsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    .line 248
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 249
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/content/Context;)V

    .line 95
    instance-of v0, p1, Lcom/zendesk/sdk/network/Retryable;

    if-eqz v0, :cond_0

    .line 96
    check-cast p1, Lcom/zendesk/sdk/network/Retryable;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    .line 98
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    sget v1, Lcom/zendesk/sdk/R$layout;->fragment_request_list:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 72
    .local v0, "fragmentView":Landroid/view/View;
    sget v1, Lcom/zendesk/sdk/R$id;->request_list_fragment_progress:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mProgressView:Landroid/view/View;

    .line 73
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mEmptyView:Landroid/view/View;

    .line 75
    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDetach()V

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRetryable:Lcom/zendesk/sdk/network/Retryable;

    .line 104
    return-void
.end method

.method public onNetworkAvailable()V
    .locals 3

    .prologue
    .line 253
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "RequestListFragment received onNetworkAvailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->refreshRequests()V

    .line 255
    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 3

    .prologue
    .line 259
    sget-object v0, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v1, "RequestListFragment received onNetworkUnavailable"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 87
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 88
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->teardownCallback()V

    .line 89
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 81
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->setupCallback()V

    .line 82
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->refreshRequests()V

    .line 83
    return-void
.end method

.method public refreshRequests()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 120
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x1

    invoke-virtual {v4, v7, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v4

    if-nez v4, :cond_1

    .line 121
    sget-object v4, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Already loading requests, skipping refreshRequests()"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isAuthenticationAvailable()Z

    move-result v4

    if-nez v4, :cond_2

    .line 126
    const-string v2, "Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)"

    .line 127
    .local v2, "error":Ljava/lang/String;
    sget-object v4, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    if-eqz v4, :cond_0

    .line 130
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    new-instance v5, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v6, "Cannot load requests because no identity has been set up. See ZendeskConfig.INSTANCE.setIdentity(Identity identity)"

    invoke-direct {v5, v6}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/zendesk/sdk/network/RequestLoadingListener;->onLoadError(Lcom/zendesk/service/ErrorResponse;)V

    goto :goto_0

    .line 136
    .end local v2    # "error":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 137
    sget-object v4, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "No network connect"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    sget-object v4, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/RequestListFragment;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 139
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mIsLoading:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 140
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 141
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    instance-of v4, v0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    if-eqz v4, :cond_0

    .line 142
    check-cast v0, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;

    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkUnavailable()V

    goto :goto_0

    .line 147
    :cond_3
    sget-object v4, Lcom/zendesk/sdk/requests/RequestListFragment;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Requesting requests from the network"

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    sget-object v4, Lcom/zendesk/sdk/ui/LoadingState;->LOADING:Lcom/zendesk/sdk/ui/LoadingState;

    invoke-direct {p0, v4}, Lcom/zendesk/sdk/requests/RequestListFragment;->setLoadingState(Lcom/zendesk/sdk/ui/LoadingState;)V

    .line 150
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    if-eqz v4, :cond_4

    .line 151
    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    invoke-interface {v4}, Lcom/zendesk/sdk/network/RequestLoadingListener;->onLoadStarted()V

    .line 162
    :cond_4
    :try_start_0
    sget-object v4, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v4}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->provider()Lcom/zendesk/sdk/network/impl/ProviderStore;

    move-result-object v4

    invoke-interface {v4}, Lcom/zendesk/sdk/network/impl/ProviderStore;->requestProvider()Lcom/zendesk/sdk/network/RequestProvider;

    move-result-object v3

    .line 163
    .local v3, "requestProvider":Lcom/zendesk/sdk/network/RequestProvider;
    const-string v4, "new,open,pending,hold,solved"

    iget-object v5, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mRequestsCallback:Lcom/zendesk/service/SafeZendeskCallback;

    invoke-interface {v3, v4, v5}, Lcom/zendesk/sdk/network/RequestProvider;->getRequests(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 164
    .end local v3    # "requestProvider":Lcom/zendesk/sdk/network/RequestProvider;
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0
.end method

.method public setRequestLoadingListener(Lcom/zendesk/sdk/network/RequestLoadingListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/zendesk/sdk/network/RequestLoadingListener;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestListFragment;->mListener:Lcom/zendesk/sdk/network/RequestLoadingListener;

    .line 183
    return-void
.end method
