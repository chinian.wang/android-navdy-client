.class public final enum Lcom/zendesk/sdk/ui/LoadingState;
.super Ljava/lang/Enum;
.source "LoadingState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/ui/LoadingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/ui/LoadingState;

.field public static final enum DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

.field public static final enum ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

.field public static final enum LOADING:Lcom/zendesk/sdk/ui/LoadingState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    new-instance v0, Lcom/zendesk/sdk/ui/LoadingState;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, Lcom/zendesk/sdk/ui/LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/ui/LoadingState;->LOADING:Lcom/zendesk/sdk/ui/LoadingState;

    new-instance v0, Lcom/zendesk/sdk/ui/LoadingState;

    const-string v1, "DISPLAYING"

    invoke-direct {v0, v1, v3}, Lcom/zendesk/sdk/ui/LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/ui/LoadingState;->DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

    new-instance v0, Lcom/zendesk/sdk/ui/LoadingState;

    const-string v1, "ERRORED"

    invoke-direct {v0, v1, v4}, Lcom/zendesk/sdk/ui/LoadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    .line 13
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/zendesk/sdk/ui/LoadingState;

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->LOADING:Lcom/zendesk/sdk/ui/LoadingState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->DISPLAYING:Lcom/zendesk/sdk/ui/LoadingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/ui/LoadingState;->ERRORED:Lcom/zendesk/sdk/ui/LoadingState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/ui/LoadingState;->$VALUES:[Lcom/zendesk/sdk/ui/LoadingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/ui/LoadingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/zendesk/sdk/ui/LoadingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/ui/LoadingState;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/ui/LoadingState;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/zendesk/sdk/ui/LoadingState;->$VALUES:[Lcom/zendesk/sdk/ui/LoadingState;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/ui/LoadingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/ui/LoadingState;

    return-object v0
.end method
