.class public abstract Lcom/zendesk/sdk/model/network/ResponseWrapper;
.super Ljava/lang/Object;
.source "ResponseWrapper.java"


# instance fields
.field private count:Ljava/lang/Integer;

.field private nextPage:Ljava/lang/String;

.field private previousPage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/zendesk/sdk/model/network/ResponseWrapper;->count:Ljava/lang/Integer;

    return-object v0
.end method

.method public getNextPage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/model/network/ResponseWrapper;->nextPage:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousPage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/zendesk/sdk/model/network/ResponseWrapper;->previousPage:Ljava/lang/String;

    return-object v0
.end method
