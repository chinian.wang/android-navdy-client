.class public Lcom/zendesk/sdk/model/settings/MobileSettings;
.super Ljava/lang/Object;
.source "MobileSettings.java"


# instance fields
.field private account:Lcom/zendesk/sdk/model/settings/AccountSettings;

.field private sdk:Lcom/zendesk/sdk/model/settings/SdkSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountSettings()Lcom/zendesk/sdk/model/settings/AccountSettings;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/MobileSettings;->account:Lcom/zendesk/sdk/model/settings/AccountSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/zendesk/sdk/model/settings/AccountSettings;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/settings/AccountSettings;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/MobileSettings;->account:Lcom/zendesk/sdk/model/settings/AccountSettings;

    goto :goto_0
.end method

.method public getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/MobileSettings;->sdk:Lcom/zendesk/sdk/model/settings/SdkSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/zendesk/sdk/model/settings/SdkSettings;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/settings/SdkSettings;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/MobileSettings;->sdk:Lcom/zendesk/sdk/model/settings/SdkSettings;

    goto :goto_0
.end method
