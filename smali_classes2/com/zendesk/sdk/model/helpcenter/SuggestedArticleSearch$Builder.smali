.class public Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
.super Ljava/lang/Object;
.source "SuggestedArticleSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCategoryId:Ljava/lang/Long;

.field private mLabelNames:[Ljava/lang/String;

.field private mLocale:Ljava/util/Locale;

.field private mQuery:Ljava/lang/String;

.field private mSectionId:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method


# virtual methods
.method public build()Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    .locals 2

    .prologue
    .line 152
    new-instance v0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;-><init>(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$1;)V

    .line 154
    .local v0, "suggestedArticleSearch":Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mQuery:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->access$102(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 155
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mLocale:Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->access$202(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/util/Locale;)Ljava/util/Locale;

    .line 156
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mLabelNames:[Ljava/lang/String;

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->access$302(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/String;)Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mCategoryId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->access$402(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/Long;)Ljava/lang/Long;

    .line 158
    iget-object v1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mSectionId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->access$502(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Ljava/lang/Long;)Ljava/lang/Long;

    .line 160
    return-object v0
.end method

.method public forLocale(Ljava/util/Locale;)Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mLocale:Ljava/util/Locale;

    .line 107
    return-object p0
.end method

.method public withCategoryId(Ljava/lang/Long;)Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    .locals 0
    .param p1, "categoryId"    # Ljava/lang/Long;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mCategoryId:Ljava/lang/Long;

    .line 130
    return-object p0
.end method

.method public varargs withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    .locals 0
    .param p1, "labelNames"    # [Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mLabelNames:[Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public withQuery(Ljava/lang/String;)Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mQuery:Ljava/lang/String;

    .line 95
    return-object p0
.end method

.method public withSectionId(Ljava/lang/Long;)Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;
    .locals 0
    .param p1, "sectionId"    # Ljava/lang/Long;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch$Builder;->mSectionId:Ljava/lang/Long;

    .line 141
    return-object p0
.end method
