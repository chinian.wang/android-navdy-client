.class Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;
.super Ljava/lang/Object;
.source "BelvedereDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/belvedere/BelvedereDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AttachmentSource"
.end annotation


# instance fields
.field private final drawable:I

.field private final text:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1, "drawable"    # I
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput p1, p0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->drawable:I

    .line 171
    iput-object p2, p0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->text:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public static from(Lcom/zendesk/belvedere/BelvedereIntent;Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;
    .locals 3
    .param p0, "belvedereIntent"    # Lcom/zendesk/belvedere/BelvedereIntent;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    sget-object v0, Lcom/zendesk/belvedere/BelvedereDialog$4;->$SwitchMap$com$zendesk$belvedere$BelvedereSource:[I

    invoke-virtual {p0}, Lcom/zendesk/belvedere/BelvedereIntent;->getSource()Lcom/zendesk/belvedere/BelvedereSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 165
    new-instance v0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;

    const/4 v1, -0x1

    sget v2, Lcom/zendesk/belvedere/R$string;->belvedere_dialog_unknown:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;-><init>(ILjava/lang/String;)V

    :goto_0
    return-object v0

    .line 161
    :pswitch_0
    new-instance v0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;

    sget v1, Lcom/zendesk/belvedere/R$drawable;->ic_camera:I

    sget v2, Lcom/zendesk/belvedere/R$string;->belvedere_dialog_camera:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 163
    :pswitch_1
    new-instance v0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;

    sget v1, Lcom/zendesk/belvedere/R$drawable;->ic_image:I

    sget v2, Lcom/zendesk/belvedere/R$string;->belvedere_dialog_gallery:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;-><init>(ILjava/lang/String;)V

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getDrawable()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->drawable:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/zendesk/belvedere/BelvedereDialog$AttachmentSource;->text:Ljava/lang/String;

    return-object v0
.end method
