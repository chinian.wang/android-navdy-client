.class public final Lcom/zendesk/belvedere/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/belvedere/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f0b002b

.field public static final abc_action_bar_default_height_material:I = 0x7f0b0002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f0b002d

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f0b002e

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f0b0040

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f0b0041

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f0b0042

.field public static final abc_action_bar_progress_bar_size:I = 0x7f0b0003

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0b0043

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0b0044

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f0b0045

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f0b0046

.field public static final abc_action_button_min_height_material:I = 0x7f0b0047

.field public static final abc_action_button_min_width_material:I = 0x7f0b0048

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f0b0049

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f0b0001

.field public static final abc_button_inset_horizontal_material:I = 0x7f0b004a

.field public static final abc_button_inset_vertical_material:I = 0x7f0b004b

.field public static final abc_button_padding_horizontal_material:I = 0x7f0b004c

.field public static final abc_button_padding_vertical_material:I = 0x7f0b004d

.field public static final abc_config_prefDialogWidth:I = 0x7f0b0007

.field public static final abc_control_corner_material:I = 0x7f0b004f

.field public static final abc_control_inset_material:I = 0x7f0b0050

.field public static final abc_control_padding_material:I = 0x7f0b0051

.field public static final abc_dialog_fixed_height_major:I = 0x7f0b0008

.field public static final abc_dialog_fixed_height_minor:I = 0x7f0b0009

.field public static final abc_dialog_fixed_width_major:I = 0x7f0b000a

.field public static final abc_dialog_fixed_width_minor:I = 0x7f0b000b

.field public static final abc_dialog_min_width_major:I = 0x7f0b000c

.field public static final abc_dialog_min_width_minor:I = 0x7f0b000d

.field public static final abc_dialog_padding_material:I = 0x7f0b0054

.field public static final abc_dialog_padding_top_material:I = 0x7f0b0055

.field public static final abc_disabled_alpha_material_dark:I = 0x7f0b0057

.field public static final abc_disabled_alpha_material_light:I = 0x7f0b0058

.field public static final abc_dropdownitem_icon_width:I = 0x7f0b0059

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0b005a

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0b005b

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f0b005c

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f0b005d

.field public static final abc_edit_text_inset_top_material:I = 0x7f0b005e

.field public static final abc_floating_window_z:I = 0x7f0b005f

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f0b0060

.field public static final abc_panel_menu_list_width:I = 0x7f0b0061

.field public static final abc_search_view_preferred_width:I = 0x7f0b0064

.field public static final abc_seekbar_track_background_height_material:I = 0x7f0b0065

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f0b0066

.field public static final abc_select_dialog_padding_start_material:I = 0x7f0b0067

.field public static final abc_switch_padding:I = 0x7f0b0038

.field public static final abc_text_size_body_1_material:I = 0x7f0b0068

.field public static final abc_text_size_body_2_material:I = 0x7f0b0069

.field public static final abc_text_size_button_material:I = 0x7f0b006a

.field public static final abc_text_size_caption_material:I = 0x7f0b006b

.field public static final abc_text_size_display_1_material:I = 0x7f0b006c

.field public static final abc_text_size_display_2_material:I = 0x7f0b006d

.field public static final abc_text_size_display_3_material:I = 0x7f0b006e

.field public static final abc_text_size_display_4_material:I = 0x7f0b006f

.field public static final abc_text_size_headline_material:I = 0x7f0b0070

.field public static final abc_text_size_large_material:I = 0x7f0b0071

.field public static final abc_text_size_medium_material:I = 0x7f0b0072

.field public static final abc_text_size_menu_material:I = 0x7f0b0074

.field public static final abc_text_size_small_material:I = 0x7f0b0075

.field public static final abc_text_size_subhead_material:I = 0x7f0b0076

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f0b0004

.field public static final abc_text_size_title_material:I = 0x7f0b0077

.field public static final abc_text_size_title_material_toolbar:I = 0x7f0b0005

.field public static final disabled_alpha_material_dark:I = 0x7f0b00b2

.field public static final disabled_alpha_material_light:I = 0x7f0b00b3

.field public static final highlight_alpha_material_colored:I = 0x7f0b00d8

.field public static final highlight_alpha_material_dark:I = 0x7f0b00d9

.field public static final highlight_alpha_material_light:I = 0x7f0b00da

.field public static final notification_large_icon_height:I = 0x7f0b00fd

.field public static final notification_large_icon_width:I = 0x7f0b00fe

.field public static final notification_subtext_size:I = 0x7f0b0102


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
