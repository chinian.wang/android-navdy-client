.class public Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
.super Ljava/lang/Object;
.source "SimpleMinimumClearance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$ComputeMCCoordinateSequenceFilter;,
        Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;
    }
.end annotation


# instance fields
.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private minClearance:D

.field private minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    return-object v0
.end method

.method static synthetic access$100(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
    .param p1, "x1"    # D
    .param p3, "x2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "x3"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->updateClearance(DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-void
.end method

.method static synthetic access$200(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p0, "x0"    # Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
    .param p1, "x1"    # D
    .param p3, "x2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "x3"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p5, "x4"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 60
    invoke-direct/range {p0 .. p5}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->updateClearance(DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-void
.end method

.method private compute()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 99
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    .line 100
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    new-instance v1, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;

    invoke-direct {v1, p0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;-><init>(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;)V

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    goto :goto_0
.end method

.method public static getDistance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 64
    new-instance v0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 65
    .local v0, "rp":Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->getDistance()D

    move-result-wide v2

    return-wide v2
.end method

.method public static getLine(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 70
    new-instance v0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 71
    .local v0, "rp":Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    return-object v1
.end method

.method private updateClearance(DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "candidateValue"    # D
    .param p3, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    .line 106
    iput-wide p1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    .line 107
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, v0, v1

    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x1

    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v2, v0, v1

    .line 110
    :cond_0
    return-void
.end method

.method private updateClearance(DLcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 5
    .param p1, "candidateValue"    # D
    .param p3, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "seg0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p5, "seg1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 115
    iget-wide v2, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    cmpg-double v1, p1, v2

    if-gez v1, :cond_0

    .line 116
    iput-wide p1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    .line 117
    iget-object v1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x0

    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v3, p3}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v3, v1, v2

    .line 118
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0, p4, p5}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 119
    .local v0, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v2, 0x1

    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, p3}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    aput-object v3, v1, v2

    .line 121
    .end local v0    # "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    :cond_0
    return-void
.end method


# virtual methods
.method public getDistance()D
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->compute()V

    .line 86
    iget-wide v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearance:D

    return-wide v0
.end method

.method public getLine()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->compute()V

    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method
