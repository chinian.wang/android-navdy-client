.class public Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;
.super Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;
.source "RandomPointsBuilder.java"


# instance fields
.field private extentLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

.field protected maskPoly:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>()V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->maskPoly:Lcom/vividsolutions/jts/geom/Geometry;

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 1
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->maskPoly:Lcom/vividsolutions/jts/geom/Geometry;

    .line 72
    return-void
.end method


# virtual methods
.method protected createCoord(DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 111
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 112
    .local v0, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 113
    return-object v0
.end method

.method protected createRandomCoord(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 118
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double v0, v4, v6

    .line 119
    .local v0, "x":D
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 120
    .local v2, "y":D
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->createCoord(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    return-object v4
.end method

.method public getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 5

    .prologue
    .line 91
    iget v4, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->numPts:I

    new-array v3, v4, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 92
    .local v3, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .line 93
    .local v0, "i":I
    :cond_0
    :goto_0
    iget v4, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->numPts:I

    if-ge v0, v4, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->createRandomCoord(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 95
    .local v2, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->extentLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    if-eqz v4, :cond_1

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->isInExtent(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 97
    :cond_1
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    aput-object v2, v3, v0

    move v0, v1

    .line 98
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0

    .line 99
    .end local v2    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    iget-object v4, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v4, v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createMultiPoint([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/MultiPoint;

    move-result-object v4

    return-object v4
.end method

.method protected isInExtent(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->extentLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->extentLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    invoke-interface {v0, p1}, Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 106
    :goto_0
    return v0

    .line 105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->getExtent()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    goto :goto_0
.end method

.method public setExtent(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "mask"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 82
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only polygonal extents are supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    iput-object p1, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->maskPoly:Lcom/vividsolutions/jts/geom/Geometry;

    .line 85
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->setExtent(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 86
    new-instance v0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/shape/random/RandomPointsBuilder;->extentLocator:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 87
    return-void
.end method
