.class public Lcom/vividsolutions/jts/algorithm/CentroidLine;
.super Ljava/lang/Object;
.source "CentroidLine.java"


# instance fields
.field private centSum:Lcom/vividsolutions/jts/geom/Coordinate;

.field private totalLength:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->totalLength:D

    .line 53
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 63
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v3, :cond_1

    .line 64
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->add([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 80
    :cond_0
    return-void

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_2

    move-object v2, p1

    .line 67
    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 69
    .local v2, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->add([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 70
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 71
    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->add([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    .end local v1    # "i":I
    .end local v2    # "poly":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_2
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 75
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 76
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 77
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidLine;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public add([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 14
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 96
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 97
    aget-object v1, p1, v0

    add-int/lit8 v8, v0, 0x1

    aget-object v8, p1, v8

    invoke-virtual {v1, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 98
    .local v6, "segmentLen":D
    iget-wide v8, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->totalLength:D

    add-double/2addr v8, v6

    iput-wide v8, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->totalLength:D

    .line 100
    aget-object v1, p1, v0

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-int/lit8 v1, v0, 0x1

    aget-object v1, p1, v1

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v8, v10

    div-double v2, v8, v12

    .line 101
    .local v2, "midx":D
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double v10, v6, v2

    add-double/2addr v8, v10

    iput-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 102
    aget-object v1, p1, v0

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-int/lit8 v1, v0, 0x1

    aget-object v1, p1, v1

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v8, v10

    div-double v4, v8, v12

    .line 103
    .local v4, "midy":D
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double v10, v6, v4

    add-double/2addr v8, v10

    iput-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    .end local v2    # "midx":D
    .end local v4    # "midy":D
    .end local v6    # "segmentLen":D
    :cond_0
    return-void
.end method

.method public getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 84
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 85
    .local v0, "cent":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->totalLength:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 86
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidLine;->totalLength:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 87
    return-object v0
.end method
