.class public Lcom/vividsolutions/jts/geom/LineString;
.super Lcom/vividsolutions/jts/geom/Geometry;
.source "LineString.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/Lineal;


# static fields
.field private static final serialVersionUID:J = 0x2b2b51ba435c8e38L


# instance fields
.field protected points:Lcom/vividsolutions/jts/geom/CoordinateSequence;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "points"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 92
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/geom/Geometry;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 93
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/LineString;->init(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V

    .line 94
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "points"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 80
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 81
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/geom/LineString;->init(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V

    .line 82
    return-void
.end method

.method private init(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V
    .locals 3
    .param p1, "points"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getCoordinateSequenceFactory()Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;->create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object p1

    .line 101
    :cond_0
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 102
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid number of points in LineString (found "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - must be 0 or >= 2)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_1
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 106
    return-void
.end method


# virtual methods
.method public apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateFilter;

    .prologue
    .line 252
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/vividsolutions/jts/geom/CoordinateFilter;->filter(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    :cond_0
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;

    .prologue
    .line 259
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 262
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {p1, v1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V

    .line 263
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isDone()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    :cond_2
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isGeometryChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->geometryChanged()V

    goto :goto_0

    .line 261
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

    .prologue
    .line 275
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryComponentFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 276
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V
    .locals 0
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryFilter;

    .prologue
    .line 271
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 272
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 285
    invoke-super {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 286
    .local v0, "ls":Lcom/vividsolutions/jts/geom/LineString;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iput-object v1, v0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 287
    return-object v0
.end method

.method protected compareToSameClass(Ljava/lang/Object;)I
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 315
    move-object v3, p1

    check-cast v3, Lcom/vividsolutions/jts/geom/LineString;

    .line 317
    .local v3, "line":Lcom/vividsolutions/jts/geom/LineString;
    const/4 v1, 0x0

    .line 318
    .local v1, "i":I
    const/4 v2, 0x0

    .line 319
    .local v2, "j":I
    :goto_0
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, v3, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 320
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    iget-object v5, v3, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v5, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 321
    .local v0, "comparison":I
    if-eqz v0, :cond_0

    .line 333
    .end local v0    # "comparison":I
    :goto_1
    return v0

    .line 324
    .restart local v0    # "comparison":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 325
    add-int/lit8 v2, v2, 0x1

    .line 326
    goto :goto_0

    .line 327
    .end local v0    # "comparison":I
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 328
    const/4 v0, 0x1

    goto :goto_1

    .line 330
    :cond_2
    iget-object v4, v3, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    if-ge v2, v4, :cond_3

    .line 331
    const/4 v0, -0x1

    goto :goto_1

    .line 333
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "comp"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;

    .prologue
    .line 338
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 339
    .local v0, "line":Lcom/vividsolutions/jts/geom/LineString;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget-object v2, v0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-virtual {p2, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method protected computeEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 2

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    .line 232
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->expandEnvelope(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    goto :goto_0
.end method

.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 6
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    const/4 v2, 0x0

    .line 236
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineString;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 239
    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 240
    .local v1, "otherLineString":Lcom/vividsolutions/jts/geom/LineString;
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v3

    iget-object v4, v1, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 243
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 244
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    iget-object v4, v1, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v4, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {p0, v3, v4, p2, p3}, Lcom/vividsolutions/jts/geom/LineString;->equal(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 248
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/vividsolutions/jts/operation/BoundaryOp;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/BoundaryOp;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/BoundaryOp;->getBoundary()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public getBoundaryDimension()I
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, -0x1

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    goto :goto_0
.end method

.method public getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v0, p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    return v0
.end method

.method public getEndPoint()Lcom/vividsolutions/jts/geom/Point;
    .locals 1

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/LineString;->getPointN(I)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "LineString"

    return-object v0
.end method

.method public getLength()D
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-static {v0}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->length(Lcom/vividsolutions/jts/geom/CoordinateSequence;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getNumPoints()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    return v0
.end method

.method public getPointN(I)Lcom/vividsolutions/jts/geom/Point;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1, p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    return-object v0
.end method

.method public getStartPoint()Lcom/vividsolutions/jts/geom/Point;
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    .line 152
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/LineString;->getPointN(I)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    goto :goto_0
.end method

.method public isCoordinate(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 220
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 221
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v1, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 222
    const/4 v1, 0x1

    .line 225
    :goto_1
    return v1

    .line 220
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 225
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 310
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    return v0
.end method

.method public isRing()Z
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->isSimple()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public normalize()V
    .locals 4

    .prologue
    .line 297
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_0

    .line 298
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int v1, v2, v0

    .line 300
    .local v1, "j":I
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 301
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2, v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v3, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-static {v2}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->reverse([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 307
    .end local v1    # "j":I
    :cond_0
    return-void

    .line 297
    .restart local v1    # "j":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public reverse()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3

    .prologue
    .line 206
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineString;->points:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 207
    .local v1, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-static {v1}, Lcom/vividsolutions/jts/geom/CoordinateSequences;->reverse(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V

    .line 208
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 209
    .local v0, "revLine":Lcom/vividsolutions/jts/geom/LineString;
    return-object v0
.end method
