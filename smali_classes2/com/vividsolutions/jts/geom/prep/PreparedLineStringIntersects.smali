.class Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;
.super Ljava/lang/Object;
.source "PreparedLineStringIntersects.java"


# instance fields
.field protected prepLine:Lcom/vividsolutions/jts/geom/prep/PreparedLineString;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedLineString;)V
    .locals 0
    .param p1, "prepLine"    # Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->prepLine:Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    .line 77
    return-void
.end method

.method public static intersects(Lcom/vividsolutions/jts/geom/prep/PreparedLineString;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "prep"    # Lcom/vividsolutions/jts/geom/prep/PreparedLineString;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 63
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedLineString;)V

    .line 64
    .local v0, "op":Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 90
    invoke-static {p1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 92
    .local v0, "lineSegStr":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 93
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->prepLine:Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->intersects(Ljava/util/Collection;)Z

    move-result v1

    .line 96
    .local v1, "segsIntersect":Z
    if-eqz v1, :cond_1

    .line 117
    .end local v1    # "segsIntersect":Z
    :cond_0
    :goto_0
    return v2

    .line 102
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v4

    if-ne v4, v2, :cond_2

    move v2, v3

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->prepLine:Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->isAnyTargetComponentInTest(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 113
    :cond_3
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    if-nez v2, :cond_4

    .line 114
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->isAnyTestPointInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v2

    goto :goto_0

    :cond_4
    move v2, v3

    .line 117
    goto :goto_0
.end method

.method protected isAnyTestPointInTarget(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 5
    .param p1, "testGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 134
    new-instance v2, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v2}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    .line 135
    .local v2, "locator":Lcom/vividsolutions/jts/algorithm/PointLocator;
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/util/ComponentCoordinateExtracter;->getCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v0

    .line 136
    .local v0, "coords":Ljava/util/List;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 137
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 138
    .local v3, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/geom/prep/PreparedLineStringIntersects;->prepLine:Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/PointLocator;->intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 139
    const/4 v4, 0x1

    .line 141
    .end local v3    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method
