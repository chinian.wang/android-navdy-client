.class public Lcom/vividsolutions/jts/geom/MultiPoint;
.super Lcom/vividsolutions/jts/geom/GeometryCollection;
.source "MultiPoint.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/Puntal;


# static fields
.field private static final serialVersionUID:J = -0x6fb1ed4162e0fa39L


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "points"    # [Lcom/vividsolutions/jts/geom/Point;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 74
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Point;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "points"    # [Lcom/vividsolutions/jts/geom/Point;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 64
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 65
    return-void
.end method


# virtual methods
.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/MultiPoint;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryCollection;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v0

    goto :goto_0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/MultiPoint;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v0

    return-object v0
.end method

.method public getBoundaryDimension()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, -0x1

    return v0
.end method

.method protected getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/MultiPoint;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/Point;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    const-string v0, "MultiPoint"

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method
