.class public Lcom/vividsolutions/jts/geom/LineSegment;
.super Ljava/lang/Object;
.source "LineSegment.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x2d2172135f411b63L


# instance fields
.field public p0:Lcom/vividsolutions/jts/geom/Coordinate;

.field public p1:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 76
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 3
    .param p1, "x0"    # D
    .param p3, "y0"    # D
    .param p5, "x1"    # D
    .param p7, "y1"    # D

    .prologue
    .line 67
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1, p5, p6, p7, p8}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 0
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 63
    iput-object p2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 2
    .param p1, "ls"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 71
    iget-object v0, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 72
    return-void
.end method

.method public static midPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 221
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v2, v4

    div-double/2addr v2, v8

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v4, v6

    div-double/2addr v4, v8

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    return-object v0
.end method


# virtual methods
.method public angle()D
    .locals 6

    .prologue
    .line 201
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v0, v2

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v4, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 439
    .local v4, "factor":D
    const-wide/16 v6, 0x0

    cmpl-double v6, v4, v6

    if-lez v6, :cond_0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpg-double v6, v4, v6

    if-gez v6, :cond_0

    .line 440
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v6

    .line 446
    :goto_0
    return-object v6

    .line 442
    :cond_0
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 443
    .local v0, "dist0":D
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 444
    .local v2, "dist1":D
    cmpg-double v6, v0, v2

    if-gez v6, :cond_1

    .line 445
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0

    .line 446
    :cond_1
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method public closestPoints(Lcom/vividsolutions/jts/geom/LineSegment;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 13
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    const/4 v10, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 457
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->intersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    .line 458
    .local v5, "intPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v5, :cond_1

    .line 459
    new-array v4, v10, [Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v5, v4, v11

    aput-object v5, v4, v12

    .line 499
    :cond_0
    :goto_0
    return-object v4

    .line 466
    :cond_1
    new-array v4, v10, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 467
    .local v4, "closestPt":[Lcom/vividsolutions/jts/geom/Coordinate;
    const-wide v8, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 470
    .local v8, "minDistance":D
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v10}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 471
    .local v0, "close00":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    .line 472
    aput-object v0, v4, v11

    .line 473
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v10, v4, v12

    .line 475
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v10}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 476
    .local v1, "close01":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 477
    .local v6, "dist":D
    cmpg-double v10, v6, v8

    if-gez v10, :cond_2

    .line 478
    move-wide v8, v6

    .line 479
    aput-object v1, v4, v11

    .line 480
    iget-object v10, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v10, v4, v12

    .line 483
    :cond_2
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v10}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 484
    .local v2, "close10":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 485
    cmpg-double v10, v6, v8

    if-gez v10, :cond_3

    .line 486
    move-wide v8, v6

    .line 487
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v10, v4, v11

    .line 488
    aput-object v2, v4, v12

    .line 491
    :cond_3
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v10}, Lcom/vividsolutions/jts/geom/LineSegment;->closestPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 492
    .local v3, "close11":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v3, v10}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 493
    cmpg-double v10, v6, v8

    if-gez v10, :cond_0

    .line 494
    move-wide v8, v6

    .line 495
    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v10, v4, v11

    .line 496
    aput-object v3, v4, v12

    goto :goto_0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 609
    move-object v1, p1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 610
    .local v1, "other":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, v1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    .line 611
    .local v0, "comp0":I
    if-eqz v0, :cond_0

    .line 612
    .end local v0    # "comp0":I
    :goto_0
    return v0

    .restart local v0    # "comp0":I
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, v1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public distance(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public distance(Lcom/vividsolutions/jts/geom/LineSegment;)D
    .locals 4
    .param p1, "ls"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 232
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distanceLineLine(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public distancePerpendicular(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {p1, v0, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->distancePointLinePerpendicular(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 574
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/LineSegment;

    if-nez v2, :cond_1

    .line 578
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 577
    check-cast v0, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 578
    .local v0, "other":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public equalsTopo(Lcom/vividsolutions/jts/geom/LineSegment;)Z
    .locals 2
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 626
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 80
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method public getLength()D
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 14

    .prologue
    const-wide/16 v12, 0x1f

    const/16 v10, 0x20

    .line 587
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 588
    .local v0, "bits0":J
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    mul-long/2addr v6, v12

    xor-long/2addr v0, v6

    .line 589
    long-to-int v6, v0

    shr-long v8, v0, v10

    long-to-int v7, v8

    xor-int v4, v6, v7

    .line 591
    .local v4, "hash0":I
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 592
    .local v2, "bits1":J
    iget-object v6, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    mul-long/2addr v6, v12

    xor-long/2addr v2, v6

    .line 593
    long-to-int v6, v2

    shr-long v8, v2, v10

    long-to-int v7, v8

    xor-int v5, v6, v7

    .line 596
    .local v5, "hash1":I
    xor-int v6, v4, v5

    return v6
.end method

.method public intersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 518
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    .line 519
    .local v0, "li":Lcom/vividsolutions/jts/algorithm/LineIntersector;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 520
    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->hasIntersection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 521
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->getIntersection(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 522
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isHorizontal()Z
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVertical()Z
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lineIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 5
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 545
    :try_start_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/HCoordinate;->intersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    :try_end_0
    .catch Lcom/vividsolutions/jts/algorithm/NotRepresentableException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 551
    :goto_0
    return-object v0

    .line 548
    :catch_0
    move-exception v1

    .line 551
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public midPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->midPoint(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/LineSegment;->reverse()V

    .line 190
    :cond_0
    return-void
.end method

.method public orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;)I
    .locals 2
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v0, v1, p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    return v0
.end method

.method public orientationIndex(Lcom/vividsolutions/jts/geom/LineSegment;)I
    .locals 5
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 141
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    .line 142
    .local v0, "orient0":I
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->orientationIndex(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v1

    .line 144
    .local v1, "orient1":I
    if-ltz v0, :cond_0

    if-ltz v1, :cond_0

    .line 145
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 150
    :goto_0
    return v2

    .line 147
    :cond_0
    if-gtz v0, :cond_1

    if-gtz v1, :cond_1

    .line 148
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    .line 150
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public pointAlong(D)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 9
    .param p1, "segmentLengthFraction"    # D

    .prologue
    .line 269
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 270
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    mul-double/2addr v4, p1

    add-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 271
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    mul-double/2addr v4, p1

    add-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 272
    return-object v0
.end method

.method public pointAlongOffset(DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 29
    .param p1, "segmentLengthFraction"    # D
    .param p3, "offsetDistance"    # D

    .prologue
    .line 294
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    mul-double v24, v24, p1

    add-double v14, v22, v24

    .line 295
    .local v14, "segx":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    mul-double v24, v24, p1

    add-double v16, v22, v24

    .line 297
    .local v16, "segy":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v24, v0

    sub-double v4, v22, v24

    .line 298
    .local v4, "dx":D
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    sub-double v6, v22, v24

    .line 299
    .local v6, "dy":D
    mul-double v22, v4, v4

    mul-double v24, v6, v6

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 300
    .local v8, "len":D
    const-wide/16 v18, 0x0

    .line 301
    .local v18, "ux":D
    const-wide/16 v20, 0x0

    .line 302
    .local v20, "uy":D
    const-wide/16 v22, 0x0

    cmpl-double v3, p3, v22

    if-eqz v3, :cond_1

    .line 303
    const-wide/16 v22, 0x0

    cmpg-double v3, v8, v22

    if-gtz v3, :cond_0

    .line 304
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v22, "Cannot compute offset from zero-length line segment"

    move-object/from16 v0, v22

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 307
    :cond_0
    mul-double v22, p3, v4

    div-double v18, v22, v8

    .line 308
    mul-double v22, p3, v6

    div-double v20, v22, v8

    .line 312
    :cond_1
    sub-double v10, v14, v20

    .line 313
    .local v10, "offsetx":D
    add-double v12, v16, v18

    .line 315
    .local v12, "offsety":D
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2, v10, v11, v12, v13}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 316
    .local v2, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v2
.end method

.method public project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 393
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 399
    :goto_0
    return-object v0

    .line 395
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 396
    .local v2, "r":D
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 397
    .local v0, "coord":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v8

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iput-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 398
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iput-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    goto :goto_0
.end method

.method public project(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 12
    .param p1, "seg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    .line 415
    iget-object v7, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v7}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 416
    .local v2, "pf0":D
    iget-object v7, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v7}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 418
    .local v4, "pf1":D
    cmpl-double v7, v2, v10

    if-ltz v7, :cond_1

    cmpl-double v7, v4, v10

    if-ltz v7, :cond_1

    .line 429
    :cond_0
    :goto_0
    return-object v6

    .line 419
    :cond_1
    cmpg-double v7, v2, v8

    if-gtz v7, :cond_2

    cmpg-double v7, v4, v8

    if-lez v7, :cond_0

    .line 421
    :cond_2
    iget-object v6, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v6}, Lcom/vividsolutions/jts/geom/LineSegment;->project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 422
    .local v0, "newp0":Lcom/vividsolutions/jts/geom/Coordinate;
    cmpg-double v6, v2, v8

    if-gez v6, :cond_3

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 423
    :cond_3
    cmpl-double v6, v2, v10

    if-lez v6, :cond_4

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 425
    :cond_4
    iget-object v6, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v6}, Lcom/vividsolutions/jts/geom/LineSegment;->project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 426
    .local v1, "newp1":Lcom/vividsolutions/jts/geom/Coordinate;
    cmpg-double v6, v4, v8

    if-gez v6, :cond_5

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 427
    :cond_5
    cmpl-double v6, v4, v10

    if-lez v6, :cond_6

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 429
    :cond_6
    new-instance v6, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v6, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0
.end method

.method public projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 14
    .param p1, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/16 v6, 0x0

    .line 334
    iget-object v8, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 356
    :goto_0
    return-wide v6

    .line 335
    :cond_0
    iget-object v8, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 347
    :cond_1
    iget-object v8, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v8, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v0, v8, v10

    .line 348
    .local v0, "dx":D
    iget-object v8, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v8, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v10, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v2, v8, v10

    .line 349
    .local v2, "dy":D
    mul-double v8, v0, v0

    mul-double v10, v2, v2

    add-double v4, v8, v10

    .line 352
    .local v4, "len":D
    cmpg-double v8, v4, v6

    if-gtz v8, :cond_2

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    goto :goto_0

    .line 354
    :cond_2
    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v10, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v10, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v8, v10

    mul-double/2addr v8, v0

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v12, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v12, v12, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v10, v12

    mul-double/2addr v10, v2

    add-double/2addr v8, v10

    div-double v6, v8, v4

    .line 356
    .local v6, "r":D
    goto :goto_0
.end method

.method public reverse()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 176
    .local v0, "temp":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 177
    iput-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 178
    return-void
.end method

.method public segmentFraction(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 4
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/LineSegment;->projectionFactor(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 376
    .local v0, "segFrac":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    .line 377
    const-wide/16 v0, 0x0

    .line 380
    :cond_0
    :goto_0
    return-wide v0

    .line 378
    :cond_1
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_2

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 379
    :cond_2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0
.end method

.method public setCoordinates(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 93
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 94
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 95
    return-void
.end method

.method public setCoordinates(Lcom/vividsolutions/jts/geom/LineSegment;)V
    .locals 2
    .param p1, "ls"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 86
    iget-object v0, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->setCoordinates(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 87
    return-void
.end method

.method public toGeometry(Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/LineString;
    .locals 3
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 562
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 633
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LINESTRING( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
