.class public Lcom/vividsolutions/jts/geom/GeometryCollection;
.super Lcom/vividsolutions/jts/geom/Geometry;
.source "GeometryCollection.java"


# static fields
.field private static final serialVersionUID:J = -0x4f07bcb1f857d89bL


# instance fields
.field protected geometries:[Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 2
    .param p1, "geometries"    # [Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 71
    invoke-direct {p0, p2}, Lcom/vividsolutions/jts/geom/Geometry;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 72
    if-nez p1, :cond_0

    .line 73
    const/4 v0, 0x0

    new-array p1, v0, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 75
    :cond_0
    invoke-static {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->hasNullElements([Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "geometries must not contain null elements"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_1
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    .line 79
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/PrecisionModel;I)V
    .locals 1
    .param p1, "geometries"    # [Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "precisionModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;
    .param p3, "SRID"    # I

    .prologue
    .line 59
    new-instance v0, Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v0, p2, p3}, Lcom/vividsolutions/jts/geom/GeometryFactory;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;I)V

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;-><init>([Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 60
    return-void
.end method


# virtual methods
.method public apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateFilter;

    .prologue
    .line 199
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_0
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;

    .prologue
    .line 205
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-nez v1, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 208
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 209
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isDone()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 213
    :cond_2
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;->isGeometryChanged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometryChanged()V

    goto :goto_0

    .line 207
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryComponentFilter;

    .prologue
    .line 225
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryComponentFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 226
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method public apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/vividsolutions/jts/geom/GeometryFilter;

    .prologue
    .line 218
    invoke-interface {p1, p0}, Lcom/vividsolutions/jts/geom/GeometryFilter;->filter(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 219
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    :cond_0
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 238
    invoke-super {p0}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 239
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/Geometry;

    iput-object v2, v0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    .line 240
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 241
    iget-object v3, v0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Geometry;

    aput-object v2, v3, v1

    .line 240
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 243
    :cond_0
    return-object v0
.end method

.method protected compareToSameClass(Ljava/lang/Object;)I
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 262
    new-instance v1, Ljava/util/TreeSet;

    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 263
    .local v1, "theseElements":Ljava/util/TreeSet;
    new-instance v0, Ljava/util/TreeSet;

    check-cast p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    .line 264
    .local v0, "otherElements":Ljava/util/TreeSet;
    invoke-virtual {p0, v1, v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->compare(Ljava/util/Collection;Ljava/util/Collection;)I

    move-result v2

    return v2
.end method

.method protected compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;
    .param p2, "comp"    # Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;

    .prologue
    .line 268
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 270
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    .line 271
    .local v3, "n1":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v4

    .line 272
    .local v4, "n2":I
    const/4 v2, 0x0

    .line 273
    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    if-ge v2, v4, :cond_1

    .line 274
    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    .line 275
    .local v6, "thisGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    .line 276
    .local v5, "otherGeom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v6, v5, p2}, Lcom/vividsolutions/jts/geom/Geometry;->compareToSameClass(Ljava/lang/Object;Lcom/vividsolutions/jts/geom/CoordinateSequenceComparator;)I

    move-result v1

    .line 277
    .local v1, "holeComp":I
    if-eqz v1, :cond_0

    .line 282
    .end local v1    # "holeComp":I
    .end local v5    # "otherGeom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v6    # "thisGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :goto_1
    return v1

    .line 278
    .restart local v1    # "holeComp":I
    .restart local v5    # "otherGeom":Lcom/vividsolutions/jts/geom/Geometry;
    .restart local v6    # "thisGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 279
    goto :goto_0

    .line 280
    .end local v1    # "holeComp":I
    .end local v5    # "otherGeom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v6    # "thisGeom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    if-ge v2, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    .line 281
    :cond_2
    if-ge v2, v4, :cond_3

    const/4 v1, -0x1

    goto :goto_1

    .line 282
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected computeEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 3

    .prologue
    .line 254
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    .line 255
    .local v0, "envelope":Lcom/vividsolutions/jts/geom/Envelope;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 256
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 255
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    :cond_0
    return-object v0
.end method

.method public equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z
    .locals 6
    .param p1, "other"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "tolerance"    # D

    .prologue
    const/4 v2, 0x0

    .line 183
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->isEquivalentClass(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v2

    :cond_1
    move-object v1, p1

    .line 186
    check-cast v1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 187
    .local v1, "otherCollection":Lcom/vividsolutions/jts/geom/GeometryCollection;
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    iget-object v4, v1, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v4, v4

    if-ne v3, v4, :cond_0

    .line 190
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 191
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v0

    iget-object v4, v1, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4, p2, p3}, Lcom/vividsolutions/jts/geom/Geometry;->equalsExact(Lcom/vividsolutions/jts/geom/Geometry;D)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public getArea()D
    .locals 6

    .prologue
    .line 166
    const-wide/16 v0, 0x0

    .line 167
    .local v0, "area":D
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 168
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v2

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->getArea()D

    move-result-wide v4

    add-double/2addr v0, v4

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 170
    :cond_0
    return-wide v0
.end method

.method public getBoundary()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0, p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->checkNotGeometryCollection(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 155
    invoke-static {}, Lcom/vividsolutions/jts/util/Assert;->shouldNeverReachHere()V

    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBoundaryDimension()I
    .locals 3

    .prologue
    .line 126
    const/4 v0, -0x1

    .line 127
    .local v0, "dimension":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getBoundaryDimension()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    :cond_0
    return v0
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    goto :goto_0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumPoints()I

    move-result v5

    new-array v1, v5, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 97
    .local v1, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v4, -0x1

    .line 98
    .local v4, "k":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 99
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 100
    .local v0, "childCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v5, v0

    if-ge v3, v5, :cond_0

    .line 101
    add-int/lit8 v4, v4, 0x1

    .line 102
    aget-object v5, v0, v3

    aput-object v5, v1, v4

    .line 100
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 98
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 105
    .end local v0    # "childCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "j":I
    :cond_1
    return-object v1
.end method

.method public getDimension()I
    .locals 3

    .prologue
    .line 118
    const/4 v0, -0x1

    .line 119
    .local v0, "dimension":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 119
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    :cond_0
    return v0
.end method

.method public getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getGeometryType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    const-string v0, "GeometryCollection"

    return-object v0
.end method

.method public getLength()D
    .locals 6

    .prologue
    .line 175
    const-wide/16 v2, 0x0

    .line 176
    .local v2, "sum":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getLength()D

    move-result-wide v4

    add-double/2addr v2, v4

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    return-wide v2
.end method

.method public getNumGeometries()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v0, v0

    return v0
.end method

.method public getNumPoints()I
    .locals 3

    .prologue
    .line 142
    const/4 v1, 0x0

    .line 143
    .local v1, "numPoints":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 144
    iget-object v2, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Geometry;->getNumPoints()I

    move-result v2

    add-int/2addr v1, v2

    .line 143
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    :cond_0
    return v1
.end method

.method public isEmpty()Z
    .locals 2

    .prologue
    .line 109
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 110
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    const/4 v1, 0x0

    .line 114
    :goto_1
    return v1

    .line 109
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 114
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 247
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->normalize()V

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 251
    return-void
.end method

.method public reverse()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4

    .prologue
    .line 295
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v1, v3

    .line 296
    .local v1, "n":I
    new-array v2, v1, [Lcom/vividsolutions/jts/geom/Geometry;

    .line 297
    .local v2, "revGeoms":[Lcom/vividsolutions/jts/geom/Geometry;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 298
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;->geometries:[Lcom/vividsolutions/jts/geom/Geometry;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->reverse()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    aput-object v3, v2, v0

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 300
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v3

    return-object v3
.end method
