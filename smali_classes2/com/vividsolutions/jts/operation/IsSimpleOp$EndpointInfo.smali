.class Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;
.super Ljava/lang/Object;
.source "IsSimpleOp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/IsSimpleOp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EndpointInfo"
.end annotation


# instance fields
.field degree:I

.field isClosed:Z

.field pt:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 290
    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->isClosed:Z

    .line 291
    iput v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->degree:I

    .line 292
    return-void
.end method


# virtual methods
.method public addEndpoint(Z)V
    .locals 1
    .param p1, "isClosed"    # Z

    .prologue
    .line 298
    iget v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->degree:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->degree:I

    .line 299
    iget-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->isClosed:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->isClosed:Z

    .line 300
    return-void
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/IsSimpleOp$EndpointInfo;->pt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method
