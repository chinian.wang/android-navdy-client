.class public Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;
.super Ljava/lang/Object;
.source "BufferCurveMaximumDistanceFinder.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MaxMidpointDistanceFilter"
.end annotation


# instance fields
.field private geom:Lcom/vividsolutions/jts/geom/Geometry;

.field private maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

.field private minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .line 111
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .line 115
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 116
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)V
    .locals 12
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "index"    # I

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 120
    if-nez p2, :cond_0

    .line 132
    :goto_0
    return-void

    .line 123
    :cond_0
    add-int/lit8 v3, p2, -0x1

    invoke-interface {p1, v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 124
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p1, p2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 125
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v6, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v4, v6

    div-double/2addr v4, v10

    iget-wide v6, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v6, v8

    div-double/2addr v6, v10

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 129
    .local v0, "midPt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize()V

    .line 130
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-static {v3, v0, v4}, Lcom/vividsolutions/jts/operation/buffer/validate/DistanceToPointFinder;->computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    .line 131
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    iget-object v4, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    goto :goto_0
.end method

.method public getMaxPointDistance()Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxMidpointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    return-object v0
.end method

.method public isDone()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public isGeometryChanged()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method
