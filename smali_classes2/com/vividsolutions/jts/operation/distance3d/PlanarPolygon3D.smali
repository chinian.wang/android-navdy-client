.class public Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;
.super Ljava/lang/Object;
.source "PlanarPolygon3D.java"


# instance fields
.field private facingPlane:I

.field private plane:Lcom/vividsolutions/jts/math/Plane3D;

.field private poly:Lcom/vividsolutions/jts/geom/Polygon;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 1
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    .line 64
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->poly:Lcom/vividsolutions/jts/geom/Polygon;

    .line 65
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->findBestFitPlane(Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/math/Plane3D;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->plane:Lcom/vividsolutions/jts/math/Plane3D;

    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->plane:Lcom/vividsolutions/jts/math/Plane3D;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/math/Plane3D;->closestAxisPlane()I

    move-result v0

    iput v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    .line 67
    return-void
.end method

.method private averageNormal(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/math/Vector3D;
    .locals 18
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 101
    invoke-interface/range {p1 .. p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v14

    .line 102
    .local v14, "n":I
    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    .line 103
    .local v3, "sum":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    invoke-direct/range {v5 .. v11}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    .line 104
    .local v5, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v7, Lcom/vividsolutions/jts/geom/Coordinate;

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    invoke-direct/range {v7 .. v13}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    .line 105
    .local v7, "p2":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    add-int/lit8 v4, v14, -0x1

    if-ge v2, v4, :cond_0

    .line 106
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v5}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 107
    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v7}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 108
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v12, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v10, v12

    iget-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v16, v0

    add-double v12, v12, v16

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 109
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v12, v7, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double/2addr v10, v12

    iget-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    add-double v12, v12, v16

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 110
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v12, v7, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v10, v12

    iget-wide v12, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v0, v7, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    add-double v12, v12, v16

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 105
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 112
    :cond_0
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    int-to-double v10, v14

    div-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 113
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    int-to-double v10, v14

    div-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 114
    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    int-to-double v10, v14

    div-double/2addr v8, v10

    iput-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 115
    invoke-static {v3}, Lcom/vividsolutions/jts/math/Vector3D;->create(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/math/Vector3D;->normalize()Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v15

    .line 116
    .local v15, "norm":Lcom/vividsolutions/jts/math/Vector3D;
    return-object v15
.end method

.method private averagePoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 9
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    const-wide/16 v2, 0x0

    .line 129
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide v4, v2

    move-wide v6, v2

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    .line 130
    .local v1, "a":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-interface {p1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v8

    .line 131
    .local v8, "n":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_0

    .line 132
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v4, 0x0

    invoke-interface {p1, v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 133
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    const/4 v4, 0x1

    invoke-interface {p1, v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 134
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    const/4 v4, 0x2

    invoke-interface {p1, v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v4

    add-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_0
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    int-to-double v4, v8

    div-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 137
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    int-to-double v4, v8

    div-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 138
    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    int-to-double v4, v8

    div-double/2addr v2, v4

    iput-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 139
    return-object v1
.end method

.method private findBestFitPlane(Lcom/vividsolutions/jts/geom/Polygon;)Lcom/vividsolutions/jts/math/Plane3D;
    .locals 4
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 84
    .local v2, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->averagePoint(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 85
    .local v0, "basePt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->averageNormal(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v1

    .line 86
    .local v1, "normal":Lcom/vividsolutions/jts/math/Vector3D;
    new-instance v3, Lcom/vividsolutions/jts/math/Plane3D;

    invoke-direct {v3, v1, v0}, Lcom/vividsolutions/jts/math/Plane3D;-><init>(Lcom/vividsolutions/jts/math/Vector3D;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v3
.end method

.method private locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I
    .locals 4
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ring"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 162
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    .line 163
    .local v1, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    invoke-static {v1, v3}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->project(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 164
    .local v2, "seqProj":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    invoke-static {p1, v3}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->project(Lcom/vividsolutions/jts/geom/Coordinate;I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 165
    .local v0, "ptProj":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0, v2}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;->locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/CoordinateSequence;)I

    move-result v3

    return v3
.end method

.method private static project(Lcom/vividsolutions/jts/geom/Coordinate;I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "facingPlane"    # I

    .prologue
    .line 186
    packed-switch p1, :pswitch_data_0

    .line 190
    :pswitch_0
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    :goto_0
    return-object v0

    .line 187
    :pswitch_1
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    goto :goto_0

    .line 188
    :pswitch_2
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    goto :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static project(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p1, "facingPlane"    # I

    .prologue
    .line 177
    packed-switch p1, :pswitch_data_0

    .line 180
    :pswitch_0
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->projectToYZ(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    :goto_0
    return-object v0

    .line 178
    :pswitch_1
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->projectToXY(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    goto :goto_0

    .line 179
    :pswitch_2
    invoke-static {p0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->projectToXZ(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v0

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getPlane()Lcom/vividsolutions/jts/math/Plane3D;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->plane:Lcom/vividsolutions/jts/math/Plane3D;

    return-object v0
.end method

.method public getPolygon()Lcom/vividsolutions/jts/geom/Polygon;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->poly:Lcom/vividsolutions/jts/geom/Polygon;

    return-object v0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 4
    .param p1, "intPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v1, 0x0

    .line 151
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->poly:Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 158
    :cond_0
    :goto_0
    return v1

    .line 154
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->poly:Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 155
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->poly:Lcom/vividsolutions/jts/geom/Polygon;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 158
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LineString;)Z
    .locals 5
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "ring"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 169
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v1

    .line 170
    .local v1, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    invoke-static {v1, v3}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->project(Lcom/vividsolutions/jts/geom/CoordinateSequence;I)Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v2

    .line 171
    .local v2, "seqProj":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    iget v3, p0, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->facingPlane:I

    invoke-static {p1, v3}, Lcom/vividsolutions/jts/operation/distance3d/PlanarPolygon3D;->project(Lcom/vividsolutions/jts/geom/Coordinate;I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 172
    .local v0, "ptProj":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x2

    invoke-static {v0, v2}, Lcom/vividsolutions/jts/algorithm/RayCrossingCounter;->locatePointInRing(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/CoordinateSequence;)I

    move-result v4

    if-eq v3, v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
