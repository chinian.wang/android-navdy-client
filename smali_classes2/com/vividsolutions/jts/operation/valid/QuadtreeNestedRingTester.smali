.class public Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;
.super Ljava/lang/Object;
.source "QuadtreeNestedRingTester.java"


# instance fields
.field private graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private quadtree:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

.field private rings:Ljava/util/List;

.field private totalEnv:Lcom/vividsolutions/jts/geom/Envelope;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 1
    .param p1, "graph"    # Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    .line 55
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Envelope;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->totalEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 62
    return-void
.end method

.method private buildQuadtree()V
    .locals 4

    .prologue
    .line 108
    new-instance v3, Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    invoke-direct {v3}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;-><init>()V

    iput-object v3, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->quadtree:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    .line 110
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 111
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 112
    .local v2, "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    .line 113
    .local v0, "env":Lcom/vividsolutions/jts/geom/Envelope;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->quadtree:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    invoke-virtual {v3, v0, v2}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->insert(Lcom/vividsolutions/jts/geom/Envelope;Ljava/lang/Object;)V

    .line 110
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "env":Lcom/vividsolutions/jts/geom/Envelope;
    .end local v2    # "ring":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/LinearRing;)V
    .locals 2
    .param p1, "ring"    # Lcom/vividsolutions/jts/geom/LinearRing;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->totalEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 70
    return-void
.end method

.method public getNestedPoint()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public isNonNested()Z
    .locals 13

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 74
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->buildQuadtree()V

    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v9, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-ge v0, v9, :cond_4

    .line 77
    iget-object v9, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->rings:Ljava/util/List;

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 78
    .local v1, "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 80
    .local v3, "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v9, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->quadtree:Lcom/vividsolutions/jts/index/quadtree/Quadtree;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/vividsolutions/jts/index/quadtree/Quadtree;->query(Lcom/vividsolutions/jts/geom/Envelope;)Ljava/util/List;

    move-result-object v6

    .line 82
    .local v6, "results":Ljava/util/List;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-ge v5, v9, :cond_3

    .line 83
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/vividsolutions/jts/geom/LinearRing;

    .line 84
    .local v7, "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/LinearRing;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v8

    .line 86
    .local v8, "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-ne v1, v7, :cond_1

    .line 82
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 89
    :cond_1
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v9

    invoke-virtual {v7}, Lcom/vividsolutions/jts/geom/LinearRing;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v12

    invoke-virtual {v9, v12}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 92
    iget-object v9, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->graph:Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-static {v3, v7, v9}, Lcom/vividsolutions/jts/operation/valid/IsValidOp;->findPtNotNode([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/LinearRing;Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 93
    .local v2, "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz v2, :cond_2

    move v9, v10

    :goto_2
    const-string v12, "Unable to find a ring point not a node of the search ring"

    invoke-static {v9, v12}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 96
    invoke-static {v2, v8}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    .line 97
    .local v4, "isInside":Z
    if-eqz v4, :cond_0

    .line 98
    iput-object v2, p0, Lcom/vividsolutions/jts/operation/valid/QuadtreeNestedRingTester;->nestedPt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 103
    .end local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "isInside":Z
    .end local v5    # "j":I
    .end local v6    # "results":Ljava/util/List;
    .end local v7    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v8    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_3
    return v11

    .restart local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .restart local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .restart local v5    # "j":I
    .restart local v6    # "results":Ljava/util/List;
    .restart local v7    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .restart local v8    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_2
    move v9, v11

    .line 93
    goto :goto_2

    .line 76
    .end local v2    # "innerRingPt":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v7    # "searchRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v8    # "searchRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .end local v1    # "innerRing":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v3    # "innerRingPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v5    # "j":I
    .end local v6    # "results":Ljava/util/List;
    :cond_4
    move v11, v10

    .line 103
    goto :goto_3
.end method
