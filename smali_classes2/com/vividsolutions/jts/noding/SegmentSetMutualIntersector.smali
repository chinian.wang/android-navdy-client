.class public abstract Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;
.super Ljava/lang/Object;
.source "SegmentSetMutualIntersector.java"


# instance fields
.field protected segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract process(Ljava/util/Collection;)V
.end method

.method public abstract setBaseSegments(Ljava/util/Collection;)V
.end method

.method public setSegmentIntersector(Lcom/vividsolutions/jts/noding/SegmentIntersector;)V
    .locals 0
    .param p1, "segInt"    # Lcom/vividsolutions/jts/noding/SegmentIntersector;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/SegmentSetMutualIntersector;->segInt:Lcom/vividsolutions/jts/noding/SegmentIntersector;

    .line 62
    return-void
.end method
