.class public Lcom/vividsolutions/jts/math/Vector3D;
.super Ljava/lang/Object;
.source "Vector3D.java"


# instance fields
.field private x:D

.field private y:D

.field private z:D


# direct methods
.method public constructor <init>(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    iput-wide p1, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    .line 121
    iput-wide p3, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    .line 122
    iput-wide p5, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    .line 123
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "v"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    .line 94
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    .line 95
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    .line 96
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p1, "from"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "to"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iget-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    .line 115
    iget-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    .line 116
    iget-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    .line 117
    return-void
.end method

.method public static create(DDD)Lcom/vividsolutions/jts/math/Vector3D;
    .locals 8
    .param p0, "x"    # D
    .param p2, "y"    # D
    .param p4, "z"    # D

    .prologue
    .line 78
    new-instance v1, Lcom/vividsolutions/jts/math/Vector3D;

    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/math/Vector3D;-><init>(DDD)V

    return-object v1
.end method

.method public static create(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/math/Vector3D;
    .locals 1
    .param p0, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 89
    new-instance v0, Lcom/vividsolutions/jts/math/Vector3D;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/math/Vector3D;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method

.method private divide(D)Lcom/vividsolutions/jts/math/Vector3D;
    .locals 7
    .param p1, "d"    # D

    .prologue
    .line 165
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    div-double/2addr v0, p1

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    div-double/2addr v2, p1

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    div-double/2addr v4, p1

    invoke-static/range {v0 .. v5}, Lcom/vividsolutions/jts/math/Vector3D;->create(DDD)Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v0

    return-object v0
.end method

.method public static dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 6
    .param p0, "v1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "v2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public static dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 18
    .param p0, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "C"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "D"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 57
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v2, v14, v16

    .line 58
    .local v2, "ABx":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v4, v14, v16

    .line 59
    .local v4, "ABy":D
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v16, v0

    sub-double v6, v14, v16

    .line 60
    .local v6, "ABz":D
    move-object/from16 v0, p3

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v16, v0

    sub-double v8, v14, v16

    .line 61
    .local v8, "CDx":D
    move-object/from16 v0, p3

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v16, v0

    sub-double v10, v14, v16

    .line 62
    .local v10, "CDy":D
    move-object/from16 v0, p3

    iget-wide v14, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v16, v0

    sub-double v12, v14, v16

    .line 63
    .local v12, "CDz":D
    mul-double v14, v2, v8

    mul-double v16, v4, v10

    add-double v14, v14, v16

    mul-double v16, v6, v12

    add-double v14, v14, v16

    return-wide v14
.end method

.method public static length(Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 6
    .param p0, "v"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static normalize(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p0, "v"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 169
    invoke-static {p0}, Lcom/vividsolutions/jts/math/Vector3D;->length(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v8

    .line 170
    .local v8, "len":D
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    div-double/2addr v2, v8

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    div-double/2addr v4, v8

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    div-double/2addr v6, v8

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    return-object v1
.end method


# virtual methods
.method public dot(Lcom/vividsolutions/jts/math/Vector3D;)D
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/math/Vector3D;

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    return-wide v0
.end method

.method public getZ()D
    .locals 2

    .prologue
    .line 134
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    return-wide v0
.end method

.method public length()D
    .locals 6

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public normalize()Lcom/vividsolutions/jts/math/Vector3D;
    .locals 8

    .prologue
    const-wide/16 v0, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/vividsolutions/jts/math/Vector3D;->length()D

    move-result-wide v6

    .line 159
    .local v6, "length":D
    cmpl-double v2, v6, v0

    if-lez v2, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/vividsolutions/jts/math/Vector3D;->length()D

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/math/Vector3D;->divide(D)Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    :cond_0
    move-wide v2, v0

    move-wide v4, v0

    invoke-static/range {v0 .. v5}, Lcom/vividsolutions/jts/math/Vector3D;->create(DDD)Lcom/vividsolutions/jts/math/Vector3D;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/math/Vector3D;->z:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
