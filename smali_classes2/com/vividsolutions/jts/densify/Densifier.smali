.class public Lcom/vividsolutions/jts/densify/Densifier;
.super Ljava/lang/Object;
.source "Densifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;
    }
.end annotation


# instance fields
.field private distanceTolerance:D

.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "inputGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/vividsolutions/jts/densify/Densifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/densify/Densifier;)D
    .locals 2
    .param p0, "x0"    # Lcom/vividsolutions/jts/densify/Densifier;

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/vividsolutions/jts/densify/Densifier;->distanceTolerance:D

    return-wide v0
.end method

.method static synthetic access$100([Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/geom/PrecisionModel;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p0, "x0"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "x1"    # D
    .param p3, "x2"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 52
    invoke-static {p0, p1, p2, p3}, Lcom/vividsolutions/jts/densify/Densifier;->densifyPoints([Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/geom/PrecisionModel;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public static densify(Lcom/vividsolutions/jts/geom/Geometry;D)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 62
    new-instance v0, Lcom/vividsolutions/jts/densify/Densifier;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/densify/Densifier;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 63
    .local v0, "densifier":Lcom/vividsolutions/jts/densify/Densifier;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/densify/Densifier;->setDistanceTolerance(D)V

    .line 64
    invoke-virtual {v0}, Lcom/vividsolutions/jts/densify/Densifier;->getResultGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method private static densifyPoints([Lcom/vividsolutions/jts/geom/Coordinate;DLcom/vividsolutions/jts/geom/PrecisionModel;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 17
    .param p0, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "distanceTolerance"    # D
    .param p3, "precModel"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 76
    new-instance v11, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v11}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 77
    .local v11, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    new-instance v2, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 78
    .local v2, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move-object/from16 v0, p0

    array-length v14, v0

    add-int/lit8 v14, v14, -0x1

    if-ge v6, v14, :cond_1

    .line 79
    aget-object v14, p0, v6

    iput-object v14, v11, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 80
    add-int/lit8 v14, v6, 0x1

    aget-object v14, p0, v14

    iput-object v14, v11, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 81
    iget-object v14, v11, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v15, 0x0

    invoke-virtual {v2, v14, v15}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 82
    invoke-virtual {v11}, Lcom/vividsolutions/jts/geom/LineSegment;->getLength()D

    move-result-wide v8

    .line 83
    .local v8, "len":D
    div-double v14, v8, p1

    double-to-int v14, v14

    add-int/lit8 v3, v14, 0x1

    .line 84
    .local v3, "densifiedSegCount":I
    const/4 v14, 0x1

    if-le v3, v14, :cond_0

    .line 85
    int-to-double v14, v3

    div-double v4, v8, v14

    .line 86
    .local v4, "densifiedSegLen":D
    const/4 v7, 0x1

    .local v7, "j":I
    :goto_1
    if-ge v7, v3, :cond_0

    .line 87
    int-to-double v14, v7

    mul-double/2addr v14, v4

    div-double v12, v14, v8

    .line 88
    .local v12, "segFract":D
    invoke-virtual {v11, v12, v13}, Lcom/vividsolutions/jts/geom/LineSegment;->pointAlong(D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v10

    .line 89
    .local v10, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 90
    const/4 v14, 0x0

    invoke-virtual {v2, v10, v14}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 86
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 78
    .end local v4    # "densifiedSegLen":D
    .end local v7    # "j":I
    .end local v10    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v12    # "segFract":D
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "densifiedSegCount":I
    .end local v8    # "len":D
    :cond_1
    move-object/from16 v0, p0

    array-length v14, v0

    add-int/lit8 v14, v14, -0x1

    aget-object v14, p0, v14

    const/4 v15, 0x0

    invoke-virtual {v2, v14, v15}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 95
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v14

    return-object v14
.end method


# virtual methods
.method public getResultGeometry()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;-><init>(Lcom/vividsolutions/jts/densify/Densifier;)V

    iget-object v1, p0, Lcom/vividsolutions/jts/densify/Densifier;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/densify/Densifier$DensifyTransformer;->transform(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public setDistanceTolerance(D)V
    .locals 3
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 121
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tolerance must be positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    iput-wide p1, p0, Lcom/vividsolutions/jts/densify/Densifier;->distanceTolerance:D

    .line 124
    return-void
.end method
