.class public Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
.super Ljava/lang/Object;
.source "QuadEdge.java"


# instance fields
.field private data:Ljava/lang/Object;

.field private next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

.field private rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

.field private vertex:Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->data:Ljava/lang/Object;

    .line 164
    return-void
.end method

.method public static connect(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 3
    .param p0, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p1, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 101
    .local v0, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 102
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 103
    return-object v0
.end method

.method public static makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 5
    .param p0, "o"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p1, "d"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 70
    new-instance v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-direct {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;-><init>()V

    .line 71
    .local v1, "q0":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    new-instance v2, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-direct {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;-><init>()V

    .line 72
    .local v2, "q1":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    new-instance v3, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-direct {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;-><init>()V

    .line 73
    .local v3, "q2":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    new-instance v4, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-direct {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;-><init>()V

    .line 75
    .local v4, "q3":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iput-object v2, v1, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 76
    iput-object v3, v2, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 77
    iput-object v4, v3, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 78
    iput-object v1, v4, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 80
    invoke-virtual {v1, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 81
    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 82
    invoke-virtual {v3, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 83
    invoke-virtual {v4, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 85
    move-object v0, v1

    .line 86
    .local v0, "base":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setOrig(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 87
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setDest(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 88
    return-object v0
.end method

.method public static splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 7
    .param p0, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .param p1, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 120
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 121
    .local v0, "alpha":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 123
    .local v1, "beta":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    .line 124
    .local v2, "t1":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    .line 125
    .local v3, "t2":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    .line 126
    .local v4, "t3":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    .line 128
    .local v5, "t4":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 129
    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 130
    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 131
    invoke-virtual {v1, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 132
    return-void
.end method

.method public static swap(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 4
    .param p0, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 141
    .local v0, "a":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 142
    .local v1, "b":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-static {p0, v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 143
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 144
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 145
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v2

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 146
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setOrig(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 147
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setDest(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 148
    return-void
.end method


# virtual methods
.method public final dNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public final dPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->invRot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->invRot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public delete()V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 210
    return-void
.end method

.method public final dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 1

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    return-object v0
.end method

.method public equalsNonOriented(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 2
    .param p1, "qe"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    const/4 v0, 0x1

    .line 391
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->equalsOriented(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v0

    .line 393
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->equalsOriented(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 395
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equalsOriented(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 2
    .param p1, "qe"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals2D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const/4 v0, 0x1

    .line 409
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getLength()D
    .locals 2

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getPrimary()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    .line 179
    .end local p0    # "this":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :goto_0
    return-object p0

    .restart local p0    # "this":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object p0

    goto :goto_0
.end method

.method public final invRot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public isLive()Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 305
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->invRot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public final lPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public final oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v0
.end method

.method public final oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iget-object v0, v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iget-object v0, v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v0
.end method

.method public final orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->vertex:Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    return-object v0
.end method

.method public final rNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iget-object v0, v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->invRot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public final rPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    return-object v0
.end method

.method public final rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v0
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->data:Ljava/lang/Object;

    .line 189
    return-void
.end method

.method setDest(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V
    .locals 1
    .param p1, "d"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setOrig(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 354
    return-void
.end method

.method public setNext(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 0
    .param p1, "next"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->next:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .line 229
    return-void
.end method

.method setOrig(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V
    .locals 0
    .param p1, "o"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->vertex:Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 345
    return-void
.end method

.method public final sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    iget-object v0, v0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    return-object v0
.end method

.method public toLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 3

    .prologue
    .line 420
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->vertex:Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 430
    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->vertex:Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 431
    .local v0, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 432
    .local v1, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0, v1}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
