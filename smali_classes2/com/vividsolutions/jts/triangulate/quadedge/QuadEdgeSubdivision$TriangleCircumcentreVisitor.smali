.class Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision$TriangleCircumcentreVisitor;
.super Ljava/lang/Object;
.source "QuadEdgeSubdivision.java"

# interfaces
.implements Lcom/vividsolutions/jts/triangulate/quadedge/TriangleVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TriangleCircumcentreVisitor"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 625
    return-void
.end method


# virtual methods
.method public visit([Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V
    .locals 7
    .param p1, "triEdges"    # [Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 629
    const/4 v6, 0x0

    aget-object v6, p1, v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 630
    .local v0, "a":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v6, 0x1

    aget-object v6, p1, v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 631
    .local v1, "b":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v6, 0x2

    aget-object v6, p1, v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 634
    .local v2, "c":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/geom/Triangle;->circumcentre(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 635
    .local v3, "cc":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v4, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-direct {v4, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 637
    .local v4, "ccVertex":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    const/4 v6, 0x3

    if-ge v5, v6, :cond_0

    .line 638
    aget-object v6, p1, v5

    invoke-virtual {v6}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->rot()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->setOrig(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)V

    .line 637
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 640
    :cond_0
    return-void
.end method
