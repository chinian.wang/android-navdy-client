.class public Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
.super Ljava/lang/Object;
.source "Vertex.java"


# static fields
.field public static final BEHIND:I = 0x3

.field public static final BETWEEN:I = 0x4

.field public static final BEYOND:I = 0x2

.field public static final DESTINATION:I = 0x6

.field public static final LEFT:I = 0x0

.field public static final ORIGIN:I = 0x5

.field public static final RIGHT:I = 0x1


# instance fields
.field private p:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(DD)V
    .locals 1
    .param p1, "_x"    # D
    .param p3, "_y"    # D

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 75
    return-void
.end method

.method public constructor <init>(DDD)V
    .locals 9
    .param p1, "_x"    # D
    .param p3, "_y"    # D
    .param p5, "_z"    # D

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    iput-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "_p"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 83
    return-void
.end method

.method private bisector(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/algorithm/HCoordinate;
    .locals 14
    .param p1, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 257
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v6

    sub-double v10, v4, v6

    .line 258
    .local v10, "dx":D
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v4

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v6

    sub-double v12, v4, v6

    .line 259
    .local v12, "dy":D
    new-instance v1, Lcom/vividsolutions/jts/algorithm/HCoordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v6, v10, v6

    add-double v2, v4, v6

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v6, v12, v6

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/algorithm/HCoordinate;-><init>(DDD)V

    .line 260
    .local v1, "l1":Lcom/vividsolutions/jts/algorithm/HCoordinate;
    new-instance v3, Lcom/vividsolutions/jts/algorithm/HCoordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    sub-double/2addr v4, v12

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v6, v10, v6

    add-double/2addr v4, v6

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v6

    add-double/2addr v6, v10

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v8, v12, v8

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-direct/range {v3 .. v9}, Lcom/vividsolutions/jts/algorithm/HCoordinate;-><init>(DDD)V

    .line 261
    .local v3, "l2":Lcom/vividsolutions/jts/algorithm/HCoordinate;
    new-instance v0, Lcom/vividsolutions/jts/algorithm/HCoordinate;

    invoke-direct {v0, v1, v3}, Lcom/vividsolutions/jts/algorithm/HCoordinate;-><init>(Lcom/vividsolutions/jts/algorithm/HCoordinate;Lcom/vividsolutions/jts/algorithm/HCoordinate;)V

    return-object v0
.end method

.method private distance(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D
    .locals 8
    .param p1, "v1"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "v2"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 265
    invoke-virtual {p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    invoke-virtual {p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v4

    sub-double/2addr v2, v4

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static interpolateZ(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 12
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 390
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 391
    .local v6, "segLen":D
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v2

    .line 392
    .local v2, "ptLen":D
    iget-wide v8, p2, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v10, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double v0, v8, v10

    .line 393
    .local v0, "dz":D
    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    div-double v10, v2, v6

    mul-double/2addr v10, v0

    add-double v4, v8, v10

    .line 394
    .local v4, "pz":D
    return-wide v4
.end method

.method public static interpolateZ(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 32
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "v0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "v1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "v2"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 366
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    .line 367
    .local v20, "x0":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    .line 368
    .local v22, "y0":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v26, v0

    sub-double v2, v26, v20

    .line 369
    .local v2, "a":D
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v26, v0

    sub-double v4, v26, v20

    .line 370
    .local v4, "b":D
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v26, v0

    sub-double v6, v26, v22

    .line 371
    .local v6, "c":D
    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v26, v0

    sub-double v8, v26, v22

    .line 372
    .local v8, "d":D
    mul-double v26, v2, v8

    mul-double v28, v4, v6

    sub-double v10, v26, v28

    .line 373
    .local v10, "det":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v26, v0

    sub-double v12, v26, v20

    .line 374
    .local v12, "dx":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v26, v0

    sub-double v14, v26, v22

    .line 375
    .local v14, "dy":D
    mul-double v26, v8, v12

    mul-double v28, v4, v14

    sub-double v26, v26, v28

    div-double v16, v26, v10

    .line 376
    .local v16, "t":D
    neg-double v0, v6

    move-wide/from16 v26, v0

    mul-double v26, v26, v12

    mul-double v28, v2, v14

    add-double v26, v26, v28

    div-double v18, v26, v10

    .line 377
    .local v18, "u":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v30, v0

    sub-double v28, v28, v30

    mul-double v28, v28, v16

    add-double v26, v26, v28

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v30, v0

    sub-double v28, v28, v30

    mul-double v28, v28, v18

    add-double v24, v26, v28

    .line 378
    .local v24, "z":D
    return-wide v24
.end method


# virtual methods
.method public circleCenter(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 12
    .param p1, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "c"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 315
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v10

    invoke-direct {v0, v8, v9, v10, v11}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    .line 317
    .local v0, "a":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->bisector(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/algorithm/HCoordinate;

    move-result-object v1

    .line 319
    .local v1, "cab":Lcom/vividsolutions/jts/algorithm/HCoordinate;
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->bisector(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/algorithm/HCoordinate;

    move-result-object v2

    .line 321
    .local v2, "cbc":Lcom/vividsolutions/jts/algorithm/HCoordinate;
    new-instance v5, Lcom/vividsolutions/jts/algorithm/HCoordinate;

    invoke-direct {v5, v1, v2}, Lcom/vividsolutions/jts/algorithm/HCoordinate;-><init>(Lcom/vividsolutions/jts/algorithm/HCoordinate;Lcom/vividsolutions/jts/algorithm/HCoordinate;)V

    .line 322
    .local v5, "hcc":Lcom/vividsolutions/jts/algorithm/HCoordinate;
    const/4 v3, 0x0

    .line 324
    .local v3, "cc":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :try_start_0
    new-instance v4, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/algorithm/HCoordinate;->getX()D

    move-result-wide v8

    invoke-virtual {v5}, Lcom/vividsolutions/jts/algorithm/HCoordinate;->getY()D

    move-result-wide v10

    invoke-direct {v4, v8, v9, v10, v11}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V
    :try_end_0
    .catch Lcom/vividsolutions/jts/algorithm/NotRepresentableException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "cc":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .local v4, "cc":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    move-object v3, v4

    .line 329
    .end local v4    # "cc":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .restart local v3    # "cc":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :goto_0
    return-object v3

    .line 325
    :catch_0
    move-exception v6

    .line 326
    .local v6, "nre":Lcom/vividsolutions/jts/algorithm/NotRepresentableException;
    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "a: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  b: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  c: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 327
    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v7, v6}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public circumRadiusRatio(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D
    .locals 10
    .param p1, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "c"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 280
    invoke-virtual {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->circleCenter(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    .line 281
    .local v6, "x":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-direct {p0, v6, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->distance(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D

    move-result-wide v4

    .line 282
    .local v4, "radius":D
    invoke-direct {p0, p0, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->distance(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D

    move-result-wide v0

    .line 283
    .local v0, "edgeLength":D
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->distance(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D

    move-result-wide v2

    .line 284
    .local v2, "el":D
    cmpg-double v7, v2, v0

    if-gez v7, :cond_0

    .line 285
    move-wide v0, v2

    .line 287
    :cond_0
    invoke-direct {p0, p2, p0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->distance(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D

    move-result-wide v2

    .line 288
    cmpg-double v7, v2, v0

    if-gez v7, :cond_1

    .line 289
    move-wide v0, v2

    .line 291
    :cond_1
    div-double v8, v4, v0

    return-wide v8
.end method

.method public classify(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)I
    .locals 12
    .param p1, "p0"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "p1"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    const-wide/16 v10, 0x0

    .line 126
    move-object v2, p0

    .line 127
    .local v2, "p2":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {p2, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->sub(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    .line 128
    .local v0, "a":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {v2, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->sub(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    .line 129
    .local v1, "b":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->crossProduct(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D

    move-result-wide v4

    .line 130
    .local v4, "sa":D
    cmpl-double v3, v4, v10

    if-lez v3, :cond_0

    .line 131
    const/4 v3, 0x0

    .line 142
    :goto_0
    return v3

    .line 132
    :cond_0
    cmpg-double v3, v4, v10

    if-gez v3, :cond_1

    .line 133
    const/4 v3, 0x1

    goto :goto_0

    .line 134
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v6

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v8

    mul-double/2addr v6, v8

    cmpg-double v3, v6, v10

    if-ltz v3, :cond_2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v6

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v8

    mul-double/2addr v6, v8

    cmpg-double v3, v6, v10

    if-gez v3, :cond_3

    .line 135
    :cond_2
    const/4 v3, 0x3

    goto :goto_0

    .line 136
    :cond_3
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->magn()D

    move-result-wide v6

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->magn()D

    move-result-wide v8

    cmpg-double v3, v6, v8

    if-gez v3, :cond_4

    .line 137
    const/4 v3, 0x2

    goto :goto_0

    .line 138
    :cond_4
    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 139
    const/4 v3, 0x5

    goto :goto_0

    .line 140
    :cond_5
    invoke-virtual {p2, v2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 141
    const/4 v3, 0x6

    goto :goto_0

    .line 142
    :cond_6
    const/4 v3, 0x4

    goto :goto_0
.end method

.method cross()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 6

    .prologue
    .line 192
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    neg-double v4, v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    return-object v0
.end method

.method crossProduct(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method dot(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D
    .locals 6
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v2

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z
    .locals 4
    .param p1, "_x"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x1

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;D)Z
    .locals 2
    .param p1, "_x"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "tolerance"    # D

    .prologue
    .line 118
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    cmpg-double v0, v0, p2

    if-gez v0, :cond_0

    .line 119
    const/4 v0, 0x1

    .line 121
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    return-wide v0
.end method

.method public getY()D
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    return-wide v0
.end method

.method public getZ()D
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    return-wide v0
.end method

.method public interpolateZValue(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)D
    .locals 32
    .param p1, "v0"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "v1"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p3, "v2"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 337
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v20

    .line 338
    .local v20, "x0":D
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v22

    .line 339
    .local v22, "y0":D
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v26

    sub-double v2, v26, v20

    .line 340
    .local v2, "a":D
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v26

    sub-double v4, v26, v20

    .line 341
    .local v4, "b":D
    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v26

    sub-double v6, v26, v22

    .line 342
    .local v6, "c":D
    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v26

    sub-double v8, v26, v22

    .line 343
    .local v8, "d":D
    mul-double v26, v2, v8

    mul-double v28, v4, v6

    sub-double v10, v26, v28

    .line 344
    .local v10, "det":D
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v26

    sub-double v12, v26, v20

    .line 345
    .local v12, "dx":D
    invoke-virtual/range {p0 .. p0}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v26

    sub-double v14, v26, v22

    .line 346
    .local v14, "dy":D
    mul-double v26, v8, v12

    mul-double v28, v4, v14

    sub-double v26, v26, v28

    div-double v16, v26, v10

    .line 347
    .local v16, "t":D
    neg-double v0, v6

    move-wide/from16 v26, v0

    mul-double v26, v26, v12

    mul-double v28, v2, v14

    add-double v26, v26, v28

    div-double v18, v26, v10

    .line 348
    .local v18, "u":D
    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v26

    invoke-virtual/range {p2 .. p2}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v28

    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v30

    sub-double v28, v28, v30

    mul-double v28, v28, v16

    add-double v26, v26, v28

    invoke-virtual/range {p3 .. p3}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v28

    invoke-virtual/range {p1 .. p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v30

    sub-double v28, v28, v30

    mul-double v28, v28, v18

    add-double v24, v26, v28

    .line 349
    .local v24, "z":D
    return-wide v24
.end method

.method public final isCCW(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z
    .locals 8
    .param p1, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "c"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 238
    iget-object v0, p1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v2

    iget-object v2, p2, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-object v2, p1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v2, v4

    iget-object v4, p2, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v6, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v6, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInCircle(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z
    .locals 4
    .param p1, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p2, "b"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .param p3, "c"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 211
    iget-object v0, p1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v1, p2, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p3, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/triangulate/quadedge/TrianglePredicate;->isInCircleRobust(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method

.method public final leftOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 2
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 252
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->isCCW(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v0

    return v0
.end method

.method magn()D
    .locals 6

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v0, v2

    iget-object v2, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public midPoint(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 12
    .param p1, "a"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 301
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v8

    add-double/2addr v0, v8

    div-double v2, v0, v10

    .line 302
    .local v2, "xm":D
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v8

    add-double/2addr v0, v8

    div-double v4, v0, v10

    .line 303
    .local v4, "ym":D
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getZ()D

    move-result-wide v8

    add-double/2addr v0, v8

    div-double v6, v0, v10

    .line 304
    .local v6, "zm":D
    new-instance v1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DDD)V

    return-object v1
.end method

.method public final rightOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z
    .locals 2
    .param p1, "e"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    .prologue
    .line 248
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->isCCW(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v0

    return v0
.end method

.method public setZ(D)V
    .locals 1
    .param p1, "_z"    # D

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iput-wide p1, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 99
    return-void
.end method

.method sub(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 8
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 182
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    sub-double/2addr v2, v4

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    return-object v0
.end method

.method sum(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 8
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 177
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getX()D

    move-result-wide v4

    add-double/2addr v2, v4

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getY()D

    move-result-wide v6

    add-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    return-object v0
.end method

.method times(D)Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    .locals 7
    .param p1, "c"    # D

    .prologue
    .line 172
    new-instance v0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v2, p1

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v4, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v4, p1

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;-><init>(DD)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "POINT ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->p:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
