.class public Lcom/vividsolutions/jts/linearref/LocationIndexedLine;
.super Ljava/lang/Object;
.source "LocationIndexedLine.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 56
    invoke-direct {p0}, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->checkGeometryType()V

    .line 57
    return-void
.end method

.method private checkGeometryType()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/LineString;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    instance-of v0, v0, Lcom/vividsolutions/jts/geom/MultiLineString;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input geometry must be linear"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    return-void
.end method


# virtual methods
.method public clampIndex(Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 2
    .param p1, "index"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 225
    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/linearref/LinearLocation;

    .line 226
    .local v0, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    iget-object v1, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->clamp(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 227
    return-object v0
.end method

.method public extractLine(Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 1
    .param p1, "startIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "endIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/ExtractLineByLocation;->extract(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/linearref/LinearLocation;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    return-object v0
.end method

.method public extractPoint(Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "index"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public extractPoint(Lcom/vividsolutions/jts/linearref/LinearLocation;D)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "index"    # Lcom/vividsolutions/jts/linearref/LinearLocation;
    .param p2, "offsetDistance"    # D

    .prologue
    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegment(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentFraction()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2, p3}, Lcom/vividsolutions/jts/geom/LineSegment;->pointAlongOffset(DD)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getEndIndex()Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getEndLocation(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public getStartIndex()Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1

    .prologue
    .line 194
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearLocation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>()V

    return-object v0
.end method

.method public indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOfAfter(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public indicesOf(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "subLine"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfLine;->indicesOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public isValidIndex(Lcom/vividsolutions/jts/linearref/LinearLocation;)Z
    .locals 1
    .param p1, "index"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->isValid(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    return v0
.end method

.method public project(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/vividsolutions/jts/linearref/LocationIndexedLine;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method
