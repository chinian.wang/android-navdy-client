.class public Lcom/navdy/client/debug/NavdyDialFragment;
.super Landroid/app/ListFragment;
.source "NavdyDialFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private appInstance:Lcom/navdy/client/app/framework/AppInstance;

.field private lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

.field private listItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/debug/NavdyDialFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/NavdyDialFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 66
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->values()[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->listItems:Ljava/util/List;

    .line 73
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->listItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 74
    .local v0, "item":Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    invoke-virtual {p0}, Lcom/navdy/client/debug/NavdyDialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->bindResource(Landroid/content/res/Resources;)V

    goto :goto_0

    .line 76
    .end local v0    # "item":Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/navdy/client/debug/NavdyDialFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x1090016

    const v4, 0x1020014

    .line 79
    invoke-static {}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->values()[Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 76
    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/NavdyDialFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 80
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    const v1, 0x7f03009a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 98
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 99
    return-object v0
.end method

.method public onDialBondResponse(Lcom/navdy/service/library/events/dial/DialBondResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/navdy/service/library/events/dial/DialBondResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 155
    const-string v0, ""

    .line 156
    .local v0, "msg":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$1;->$SwitchMap$com$navdy$service$library$events$dial$DialError:[I

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->status:Lcom/navdy/service/library/events/dial/DialError;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/dial/DialError;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 192
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown dial response status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->status:Lcom/navdy/service/library/events/dial/DialError;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 195
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 196
    return-void

    .line 158
    :pswitch_0
    const-string v0, "No Dial Found"

    .line 159
    goto :goto_0

    .line 162
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already paired with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 163
    goto :goto_0

    .line 166
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Paired with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialBondResponse;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    goto :goto_0

    .line 170
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    if-eqz v1, :cond_2

    .line 171
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    sget-object v2, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    sget-object v2, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    if-ne v1, v2, :cond_1

    .line 173
    :cond_0
    const-string v0, "Pairing removed"

    .line 177
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    goto :goto_0

    .line 175
    :cond_1
    const-string v0, "Cannot pair"

    goto :goto_1

    .line 179
    :cond_2
    const-string v0, "Cannot pair"

    .line 181
    goto :goto_0

    .line 184
    :pswitch_4
    const-string v0, "Pairing in progresss"

    .line 185
    goto :goto_0

    .line 188
    :pswitch_5
    const-string v0, "Error occurred"

    .line 189
    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onDialStatusResponse(Lcom/navdy/service/library/events/dial/DialStatusResponse;)V
    .locals 4
    .param p1, "status"    # Lcom/navdy/service/library/events/dial/DialStatusResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const-string v1, "Dial is not paired"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 151
    return-void

    .line 143
    :cond_0
    const-string v1, "Dial \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is Paired "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 145
    const-string v1, "but Not Connected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 147
    :cond_1
    const-string v1, "and Connected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 104
    invoke-super/range {p0 .. p5}, Landroid/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 105
    invoke-virtual {p1, p3, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 106
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->listItems:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;

    .line 107
    .local v0, "clickedItem":Lcom/navdy/client/debug/NavdyDialFragment$ListItem;
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v2, "Not connected to Hud"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    .line 135
    :goto_0
    return-void

    .line 111
    :cond_0
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment$1;->$SwitchMap$com$navdy$client$debug$NavdyDialFragment$ListItem:[I

    invoke-virtual {v0}, Lcom/navdy/client/debug/NavdyDialFragment$ListItem;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 113
    :pswitch_0
    new-instance v1, Lcom/navdy/service/library/events/dial/DialStatusRequest;

    invoke-direct {v1}, Lcom/navdy/service/library/events/dial/DialStatusRequest;-><init>()V

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 114
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent dial status request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_1
    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 119
    new-instance v1, Lcom/navdy/service/library/events/dial/DialBondRequest;

    sget-object v2, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 120
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent dial bonding request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :pswitch_2
    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 125
    new-instance v1, Lcom/navdy/service/library/events/dial/DialBondRequest;

    sget-object v2, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_REBOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 126
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent dial repair bonding request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :pswitch_3
    sget-object v1, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    iput-object v1, p0, Lcom/navdy/client/debug/NavdyDialFragment;->lastAction:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    .line 131
    new-instance v1, Lcom/navdy/service/library/events/dial/DialBondRequest;

    sget-object v2, Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;->DIAL_CLEAR_BOND:Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/events/dial/DialBondRequest;-><init>(Lcom/navdy/service/library/events/dial/DialBondRequest$DialAction;)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 132
    sget-object v1, Lcom/navdy/client/debug/NavdyDialFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "sent dial clear bond request"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 90
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 91
    invoke-super {p0}, Landroid/app/ListFragment;->onPause()V

    .line 92
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/ListFragment;->onResume()V

    .line 85
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 86
    return-void
.end method
