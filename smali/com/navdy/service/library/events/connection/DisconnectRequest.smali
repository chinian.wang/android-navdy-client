.class public final Lcom/navdy/service/library/events/connection/DisconnectRequest;
.super Lcom/squareup/wire/Message;
.source "DisconnectRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FORGET:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final forget:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->DEFAULT_FORGET:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;

    .prologue
    .line 33
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;->forget:Ljava/lang/Boolean;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/connection/DisconnectRequest;-><init>(Ljava/lang/Boolean;)V

    .line 34
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/connection/DisconnectRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 35
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;Lcom/navdy/service/library/events/connection/DisconnectRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/connection/DisconnectRequest$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/connection/DisconnectRequest;-><init>(Lcom/navdy/service/library/events/connection/DisconnectRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "forget"    # Ljava/lang/Boolean;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 39
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 41
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 40
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    check-cast p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/connection/DisconnectRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->hashCode:I

    .line 47
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->forget:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/connection/DisconnectRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
