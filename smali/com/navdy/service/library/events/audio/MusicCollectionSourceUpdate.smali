.class public final Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;
.super Lcom/squareup/wire/Message;
.source "MusicCollectionSourceUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 15
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/Long;)V
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p2, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 31
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    .line 32
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;

    .prologue
    .line 35
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;->serial_number:Ljava/lang/Long;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSource;Ljava/lang/Long;)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;-><init>(Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, p0, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v1

    .line 42
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 43
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;

    .line 44
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    .line 45
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 50
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->hashCode:I

    .line 51
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 52
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v0

    .line 53
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->serial_number:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 54
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicCollectionSourceUpdate;->hashCode:I

    .line 56
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 52
    goto :goto_0
.end method
