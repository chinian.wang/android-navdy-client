.class public final Lcom/navdy/service/library/events/audio/MusicEvent;
.super Lcom/squareup/wire/Message;
.source "MusicEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/MusicEvent$Action;,
        Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTION:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public static final DEFAULT_COLLECTIONID:Ljava/lang/String; = ""

.field public static final DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public static final DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public static final DEFAULT_DATASOURCE:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public static final DEFAULT_INDEX:Ljava/lang/Integer;

.field public static final DEFAULT_REPEATMODE:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

.field public static final DEFAULT_SHUFFLEMODE:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final index:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x12
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x11
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->MUSIC_ACTION_PLAY:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_ACTION:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 18
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicDataSource;->MUSIC_SOURCE_NONE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_DATASOURCE:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->COLLECTION_SOURCE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_COLLECTIONSOURCE:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicCollectionType;->COLLECTION_TYPE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_COLLECTIONTYPE:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 22
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_INDEX:Ljava/lang/Integer;

    .line 23
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->MUSIC_SHUFFLE_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_SHUFFLEMODE:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 24
    sget-object v0, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->MUSIC_REPEAT_MODE_UNKNOWN:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    sput-object v0, Lcom/navdy/service/library/events/audio/MusicEvent;->DEFAULT_REPEATMODE:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicEvent$Action;Lcom/navdy/service/library/events/audio/MusicDataSource;Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicShuffleMode;Lcom/navdy/service/library/events/audio/MusicRepeatMode;)V
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;
    .param p2, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;
    .param p3, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;
    .param p4, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;
    .param p5, "collectionId"    # Ljava/lang/String;
    .param p6, "index"    # Ljava/lang/Integer;
    .param p7, "shuffleMode"    # Lcom/navdy/service/library/events/audio/MusicShuffleMode;
    .param p8, "repeatMode"    # Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 78
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 79
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 80
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 81
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    .line 82
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    .line 83
    iput-object p7, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 84
    iput-object p8, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 85
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/MusicEvent$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/MusicEvent$Builder;

    .prologue
    .line 88
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionId:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->index:Ljava/lang/Integer;

    iget-object v7, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iget-object v8, p1, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/audio/MusicEvent;-><init>(Lcom/navdy/service/library/events/audio/MusicEvent$Action;Lcom/navdy/service/library/events/audio/MusicDataSource;Lcom/navdy/service/library/events/audio/MusicCollectionSource;Lcom/navdy/service/library/events/audio/MusicCollectionType;Ljava/lang/String;Ljava/lang/Integer;Lcom/navdy/service/library/events/audio/MusicShuffleMode;Lcom/navdy/service/library/events/audio/MusicRepeatMode;)V

    .line 89
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/MusicEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 90
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/MusicEvent$Builder;Lcom/navdy/service/library/events/audio/MusicEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/MusicEvent$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/MusicEvent;-><init>(Lcom/navdy/service/library/events/audio/MusicEvent$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    if-ne p1, p0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/MusicEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 96
    check-cast v0, Lcom/navdy/service/library/events/audio/MusicEvent;

    .line 97
    .local v0, "o":Lcom/navdy/service/library/events/audio/MusicEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 98
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 99
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 103
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 104
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/MusicEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 109
    iget v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->hashCode:I

    .line 110
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 111
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicEvent$Action;->hashCode()I

    move-result v0

    .line 112
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicDataSource;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 113
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionSource;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicCollectionType;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 116
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 117
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/MusicShuffleMode;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 118
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/audio/MusicRepeatMode;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 119
    iput v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent;->hashCode:I

    .line 121
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 111
    goto :goto_0

    :cond_3
    move v2, v1

    .line 112
    goto :goto_1

    :cond_4
    move v2, v1

    .line 113
    goto :goto_2

    :cond_5
    move v2, v1

    .line 114
    goto :goto_3

    :cond_6
    move v2, v1

    .line 115
    goto :goto_4

    :cond_7
    move v2, v1

    .line 116
    goto :goto_5

    :cond_8
    move v2, v1

    .line 117
    goto :goto_6
.end method
