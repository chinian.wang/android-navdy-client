.class public final Lcom/navdy/service/library/events/destination/Destination;
.super Lcom/squareup/wire/Message;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/destination/Destination$SuggestionType;,
        Lcom/navdy/service/library/events/destination/Destination$FavoriteType;,
        Lcom/navdy/service/library/events/destination/Destination$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CONTACTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESTINATIONDISTANCE:Ljava/lang/String; = ""

.field public static final DEFAULT_DESTINATIONICON:Ljava/lang/Integer;

.field public static final DEFAULT_DESTINATIONICONBKCOLOR:Ljava/lang/Integer;

.field public static final DEFAULT_DESTINATION_SUBTITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_DESTINATION_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_FAVORITE_TYPE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

.field public static final DEFAULT_FULL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_RECOMMENDATION:Ljava/lang/Boolean;

.field public static final DEFAULT_LAST_NAVIGATED_TO:Ljava/lang/Long;

.field public static final DEFAULT_PHONENUMBERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PLACE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACE_TYPE:Lcom/navdy/service/library/events/places/PlaceType;

.field public static final DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

.field private static final serialVersionUID:J


# instance fields
.field public final contacts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/contacts/Contact;
        tag = 0x12
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public final destinationDistance:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destinationIcon:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destinationIconBkColor:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination_subtitle:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final display_position:Lcom/navdy/service/library/events/location/LatLong;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
    .end annotation
.end field

.field public final favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final full_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final is_recommendation:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final last_navigated_to:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final navigation_position:Lcom/navdy/service/library/events/location/LatLong;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
    .end annotation
.end field

.field public final phoneNumbers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/contacts/PhoneNumber;
        tag = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field public final place_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final place_type:Lcom/navdy/service/library/events/places/PlaceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->FAVORITE_NONE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_FAVORITE_TYPE:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 35
    sget-object v0, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->SUGGESTION_NONE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_SUGGESTION_TYPE:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 36
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_IS_RECOMMENDATION:Ljava/lang/Boolean;

    .line 37
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_LAST_NAVIGATED_TO:Ljava/lang/Long;

    .line 38
    sget-object v0, Lcom/navdy/service/library/events/places/PlaceType;->PLACE_TYPE_UNKNOWN:Lcom/navdy/service/library/events/places/PlaceType;

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_PLACE_TYPE:Lcom/navdy/service/library/events/places/PlaceType;

    .line 39
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_DESTINATIONICON:Ljava/lang/Integer;

    .line 40
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_DESTINATIONICONBKCOLOR:Ljava/lang/Integer;

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_PHONENUMBERS:Ljava/util/List;

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/destination/Destination;->DEFAULT_CONTACTS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/destination/Destination$Builder;)V
    .locals 19
    .param p1, "builder"    # Lcom/navdy/service/library/events/destination/Destination$Builder;

    .prologue
    .line 192
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->full_address:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_title:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destination_subtitle:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->identifier:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->is_recommendation:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->last_navigated_to:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIcon:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationIconBkColor:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->place_id:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->destinationDistance:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->phoneNumbers:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/destination/Destination$Builder;->contacts:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v18}, Lcom/navdy/service/library/events/destination/Destination;-><init>(Lcom/navdy/service/library/events/location/LatLong;Lcom/navdy/service/library/events/location/LatLong;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$SuggestionType;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/places/PlaceType;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 193
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/destination/Destination;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 194
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/destination/Destination$Builder;Lcom/navdy/service/library/events/destination/Destination$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/destination/Destination$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/destination/Destination$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/destination/Destination;-><init>(Lcom/navdy/service/library/events/destination/Destination$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/location/LatLong;Lcom/navdy/service/library/events/location/LatLong;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$FavoriteType;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination$SuggestionType;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/places/PlaceType;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1, "navigation_position"    # Lcom/navdy/service/library/events/location/LatLong;
    .param p2, "display_position"    # Lcom/navdy/service/library/events/location/LatLong;
    .param p3, "full_address"    # Ljava/lang/String;
    .param p4, "destination_title"    # Ljava/lang/String;
    .param p5, "destination_subtitle"    # Ljava/lang/String;
    .param p6, "favorite_type"    # Lcom/navdy/service/library/events/destination/Destination$FavoriteType;
    .param p7, "identifier"    # Ljava/lang/String;
    .param p8, "suggestion_type"    # Lcom/navdy/service/library/events/destination/Destination$SuggestionType;
    .param p9, "is_recommendation"    # Ljava/lang/Boolean;
    .param p10, "last_navigated_to"    # Ljava/lang/Long;
    .param p11, "place_type"    # Lcom/navdy/service/library/events/places/PlaceType;
    .param p12, "destinationIcon"    # Ljava/lang/Integer;
    .param p13, "destinationIconBkColor"    # Ljava/lang/Integer;
    .param p14, "place_id"    # Ljava/lang/String;
    .param p15, "destinationDistance"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/location/LatLong;",
            "Lcom/navdy/service/library/events/location/LatLong;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$FavoriteType;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/destination/Destination$SuggestionType;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Lcom/navdy/service/library/events/places/PlaceType;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/PhoneNumber;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p16, "phoneNumbers":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/PhoneNumber;>;"
    .local p17, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 173
    iput-object p2, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 174
    iput-object p3, p0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    .line 175
    iput-object p4, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    .line 176
    iput-object p5, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    .line 177
    iput-object p6, p0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 178
    iput-object p7, p0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    .line 179
    iput-object p8, p0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 180
    iput-object p9, p0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    .line 181
    iput-object p10, p0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    .line 182
    iput-object p11, p0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 183
    iput-object p12, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    .line 184
    iput-object p13, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    .line 185
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    .line 186
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    .line 187
    invoke-static/range {p16 .. p16}, Lcom/navdy/service/library/events/destination/Destination;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    .line 188
    invoke-static/range {p17 .. p17}, Lcom/navdy/service/library/events/destination/Destination;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    .line 189
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/navdy/service/library/events/destination/Destination;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/navdy/service/library/events/destination/Destination;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    if-ne p1, p0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v1

    .line 199
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/destination/Destination;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 200
    check-cast v0, Lcom/navdy/service/library/events/destination/Destination;

    .line 201
    .local v0, "o":Lcom/navdy/service/library/events/destination/Destination;
    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 202
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    .line 203
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    .line 204
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    .line 205
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    .line 206
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    .line 207
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    .line 208
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    .line 209
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    .line 210
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    .line 211
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    .line 212
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    .line 213
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    .line 214
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    .line 215
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    .line 216
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    .line 217
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/destination/Destination;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 222
    iget v0, p0, Lcom/navdy/service/library/events/destination/Destination;->hashCode:I

    .line 223
    .local v0, "result":I
    if-nez v0, :cond_2

    .line 224
    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->navigation_position:Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/LatLong;->hashCode()I

    move-result v0

    .line 225
    :goto_0
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->display_position:Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/LatLong;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 226
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->full_address:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v4, v2

    .line 227
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_title:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 228
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destination_subtitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v4, v2

    .line 229
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->favorite_type:Lcom/navdy/service/library/events/destination/Destination$FavoriteType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/destination/Destination$FavoriteType;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 230
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v4, v2

    .line 231
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->suggestion_type:Lcom/navdy/service/library/events/destination/Destination$SuggestionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/destination/Destination$SuggestionType;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v4, v2

    .line 232
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->is_recommendation:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v4, v2

    .line 233
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->last_navigated_to:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v4, v2

    .line 234
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->place_type:Lcom/navdy/service/library/events/places/PlaceType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/places/PlaceType;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v4, v2

    .line 235
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIcon:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v4, v2

    .line 236
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationIconBkColor:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v4, v2

    .line 237
    mul-int/lit8 v4, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->place_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v4, v2

    .line 238
    mul-int/lit8 v2, v0, 0x25

    iget-object v4, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/destination/Destination;->destinationDistance:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 239
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/navdy/service/library/events/destination/Destination;->phoneNumbers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_e
    add-int v0, v2, v1

    .line 240
    mul-int/lit8 v1, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/service/library/events/destination/Destination;->contacts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v3

    :cond_1
    add-int v0, v1, v3

    .line 241
    iput v0, p0, Lcom/navdy/service/library/events/destination/Destination;->hashCode:I

    .line 243
    :cond_2
    return v0

    :cond_3
    move v0, v1

    .line 224
    goto/16 :goto_0

    :cond_4
    move v2, v1

    .line 225
    goto/16 :goto_1

    :cond_5
    move v2, v1

    .line 226
    goto/16 :goto_2

    :cond_6
    move v2, v1

    .line 227
    goto/16 :goto_3

    :cond_7
    move v2, v1

    .line 228
    goto/16 :goto_4

    :cond_8
    move v2, v1

    .line 229
    goto/16 :goto_5

    :cond_9
    move v2, v1

    .line 230
    goto/16 :goto_6

    :cond_a
    move v2, v1

    .line 231
    goto/16 :goto_7

    :cond_b
    move v2, v1

    .line 232
    goto/16 :goto_8

    :cond_c
    move v2, v1

    .line 233
    goto/16 :goto_9

    :cond_d
    move v2, v1

    .line 234
    goto :goto_a

    :cond_e
    move v2, v1

    .line 235
    goto :goto_b

    :cond_f
    move v2, v1

    .line 236
    goto :goto_c

    :cond_10
    move v2, v1

    .line 237
    goto :goto_d

    :cond_11
    move v1, v3

    .line 239
    goto :goto_e
.end method
