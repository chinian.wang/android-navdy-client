.class public final Lcom/navdy/service/library/events/dial/DialStatusResponse;
.super Lcom/squareup/wire/Message;
.source "DialStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_BATTERYLEVEL:Ljava/lang/Integer;

.field public static final DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

.field public static final DEFAULT_ISPAIRED:Ljava/lang/Boolean;

.field public static final DEFAULT_MACADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final batteryLevel:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isConnected:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isPaired:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final macAddress:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->DEFAULT_ISPAIRED:Ljava/lang/Boolean;

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

    .line 22
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->DEFAULT_BATTERYLEVEL:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;

    .prologue
    .line 63
    iget-object v1, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isPaired:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->isConnected:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->name:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->macAddress:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;->batteryLevel:Ljava/lang/Integer;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/dial/DialStatusResponse;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;Lcom/navdy/service/library/events/dial/DialStatusResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/dial/DialStatusResponse$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/dial/DialStatusResponse;-><init>(Lcom/navdy/service/library/events/dial/DialStatusResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "isPaired"    # Ljava/lang/Boolean;
    .param p2, "isConnected"    # Ljava/lang/Boolean;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "macAddress"    # Ljava/lang/String;
    .param p5, "batteryLevel"    # Ljava/lang/Integer;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    .line 56
    iput-object p2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    .line 57
    iput-object p3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    .line 59
    iput-object p5, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    .line 60
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v1

    .line 70
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/dial/DialStatusResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 71
    check-cast v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;

    .line 72
    .local v0, "o":Lcom/navdy/service/library/events/dial/DialStatusResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    .line 74
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    .line 75
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    .line 76
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/dial/DialStatusResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 81
    iget v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->hashCode:I

    .line 82
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 83
    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isPaired:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 84
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 85
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 86
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->macAddress:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 87
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->batteryLevel:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 88
    iput v0, p0, Lcom/navdy/service/library/events/dial/DialStatusResponse;->hashCode:I

    .line 90
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 83
    goto :goto_0

    :cond_3
    move v2, v1

    .line 84
    goto :goto_1

    :cond_4
    move v2, v1

    .line 85
    goto :goto_2

    :cond_5
    move v2, v1

    .line 86
    goto :goto_3
.end method
