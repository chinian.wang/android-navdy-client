.class public final Lcom/navdy/service/library/events/photo/PhotoUpdate;
.super Lcom/squareup/wire/Message;
.source "PhotoUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHOTO:Lokio/ByteString;

.field public static final DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

.field private static final serialVersionUID:J


# instance fields
.field public final identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photo:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BYTES:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photoType:Lcom/navdy/service/library/events/photo/PhotoType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->DEFAULT_PHOTO:Lokio/ByteString;

    .line 19
    sget-object v0, Lcom/navdy/service/library/events/photo/PhotoType;->PHOTO_ALBUM_ART:Lcom/navdy/service/library/events/photo/PhotoType;

    sput-object v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->DEFAULT_PHOTOTYPE:Lcom/navdy/service/library/events/photo/PhotoType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;

    .prologue
    .line 37
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->identifier:Ljava/lang/String;

    iget-object v1, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photo:Lokio/ByteString;

    iget-object v2, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/photo/PhotoUpdate;-><init>(Ljava/lang/String;Lokio/ByteString;Lcom/navdy/service/library/events/photo/PhotoType;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 39
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/photo/PhotoUpdate$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/photo/PhotoUpdate;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdate$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lokio/ByteString;Lcom/navdy/service/library/events/photo/PhotoType;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "photo"    # Lokio/ByteString;
    .param p3, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    .line 33
    iput-object p3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 34
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v1

    .line 44
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/photo/PhotoUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 45
    check-cast v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;

    .line 46
    .local v0, "o":Lcom/navdy/service/library/events/photo/PhotoUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    .line 47
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iget-object v4, v0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 48
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/photo/PhotoUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53
    iget v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->hashCode:I

    .line 54
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 55
    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->identifier:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 56
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photo:Lokio/ByteString;

    invoke-virtual {v2}, Lokio/ByteString;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 57
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/photo/PhotoType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 58
    iput v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdate;->hashCode:I

    .line 60
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 55
    goto :goto_0

    :cond_3
    move v2, v1

    .line 56
    goto :goto_1
.end method
